#pragma once

#include <descriptors.hpp>

namespace vkpg
{
	class material_post_process;

	class post_process_effect
	{
	public:
		post_process_effect(renderer const& renderer, material_post_process const& material);

		auto update_image_view(renderer const& renderer, vk::ImageView const& image_view) -> void;
		auto record_draw_commands(vk::CommandBuffer const& command_buffer) const -> void;

	private:
		material_post_process const* material_;

		descriptor_post_process descriptor_;
	};
}
