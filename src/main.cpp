#include <iostream>

#include <game.hpp>

auto main() -> int
{
	// Only log exceptions in release mode. Let debugger handle them in debug mode.
#ifdef NDEBUG
	try
	{
#endif
		
		auto app = vkpg::game{};
		app.run();
		
#ifdef NDEBUG
	}
	catch (std::exception const& e)
	{
		std::cerr << e.what() << "\n";
		return EXIT_FAILURE;
	}
#endif
	
	return EXIT_SUCCESS;
}
