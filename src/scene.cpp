#include <fx/gltf.h>
#include <game.hpp>
#include <glm/glm.hpp>
#include <iostream>
#include <light.hpp>
#include <render_component.hpp>
#include <scene.hpp>
#include <skybox.hpp>

using namespace std::string_literals;

vkpg::scene::scene(renderer const& renderer) :
	light_uniform_buffer_{create_uniform_buffer<light_ubo>(renderer.device())},
	light_uniform_buffer_memory_{renderer.bind_buffer_memory(
		*light_uniform_buffer_, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent)},
	lights_descriptor_{renderer, renderer.descriptor_set_layout_lights(), get_frame_arr(*light_uniform_buffer_)}
{
}

auto vkpg::scene::cam() -> camera*
{
	return camera_ ? &camera_.value() : nullptr;
}

auto vkpg::scene::objects() -> std::vector<render_component>&
{
	return objects_;
}

auto vkpg::scene::add_render_component(render_component&& object) -> void
{
	objects_.push_back(std::move(object));
}

auto vkpg::scene::load_gltf(std::filesystem::path const& file, game& game, renderer const& renderer) -> void
{
	auto doc = fx::gltf::LoadFromText(file.string());

	for(auto& mesh : doc.meshes)
	{
		auto indices = get_indices(doc, mesh.primitives[0]);
		auto positions = get_positions(doc, mesh.primitives[0]);
		auto normals = get_normals(doc, mesh.primitives[0]);
		auto texcoords = get_texcoords(doc, mesh.primitives[0]);

		auto vertices = std::vector<vertex>{};
		for(auto i = size_t{0}; i < positions.size(); ++i)
		{
			vertices.push_back(vertex{positions[i], normals[i], texcoords[i]});
		}
		game.add_model(std::make_unique<model>(std::move(vertices), std::move(indices), renderer));
	}
	for(auto& tex : doc.textures)
	{
		auto img = doc.images[tex.source];
		game.add_texture(std::make_unique<texture>(renderer, std::filesystem::path{"scenes/"s + img.uri}));
	}
	for(auto& mat : doc.materials)
	{
		const auto tex_index = mat.pbrMetallicRoughness.baseColorTexture.index;
		const auto alpha_mode = mat.alphaMode == fx::gltf::Material::AlphaMode::Blend ?
			blend_mode::blend :
			(mat.alphaMode == fx::gltf::Material::AlphaMode::Mask ? blend_mode::mask : blend_mode::opaque);
		// TODO: Make it load an actual specular map (implement specular workflow extension?).
		game.add_material(std::make_unique<material_standard>(
			renderer,
			game.get_shader_standard(),
			game.get_texture(tex_index),
			game.get_texture(tex_index),
			32.0f,
			alpha_mode,
			renderer.scene_render_pass()));
	}

	auto scene = doc.scenes[doc.scene];
	for(auto node_index : scene.nodes)
	{
		auto& node = doc.nodes.at(node_index);
		load_node(doc, node, root_node_, game, renderer);
	}

	// TODO: Move this to allow dynamic lights
	auto light_buffer = light_ubo{};
	light_buffer.current_num_directional_lights = dir_lights_.size();
	light_buffer.current_num_point_lights = point_lights_.size();
	light_buffer.current_num_spot_lights = spot_lights_.size();
	for(auto i = size_t{0}; i < dir_lights_.size(); ++i)
	{
		light_buffer.directional[i] = get_light_ubo(dir_lights_[i]);
	}
	for(auto i = size_t{0}; i < point_lights_.size(); ++i)
	{
		light_buffer.point_lights[i] = get_light_ubo(point_lights_[i]);
	}
	for(auto i = size_t{0}; i < spot_lights_.size(); ++i)
	{
		light_buffer.spot_lights[i] = get_light_ubo(spot_lights_[i]);
	}
	renderer.update_ubo(*light_uniform_buffer_memory_, light_buffer);
}

auto vkpg::scene::create_skybox(renderer const& renderer, model const& model, material_skybox const& material) -> void
{
	skybox_ = std::make_unique<skybox>(renderer, model, material);
}

auto vkpg::scene::get_skybox() -> skybox*
{
	return skybox_.get();
}

auto vkpg::scene::get_skybox() const -> skybox const*
{
	return skybox_.get();
}

auto vkpg::scene::lights_descriptor() const -> descriptor_lights const&
{
	return lights_descriptor_;
}

auto vkpg::scene::load_node(
	fx::gltf::Document const& doc, fx::gltf::Node const& node, transform& parent, game& game, renderer const& renderer)
	-> void
{
	const auto pos = glm::vec3{node.translation[0], node.translation[1], node.translation[2]};
	const auto scale = glm::vec3{node.scale[0], node.scale[1], node.scale[2]};
	const auto rot = glm::quat{node.rotation[3], node.rotation[0], node.rotation[1], node.rotation[2]};
	auto trans_ptr = std::make_unique<transform>(pos, scale, rot);
	auto& trans = *trans_ptr;
	parent.add_child(std::move(trans_ptr));

	if(node.mesh != -1)
	{
		auto mesh = doc.meshes.at(node.mesh);
		add_render_component(
			{renderer, game.get_model(node.mesh), game.get_material(mesh.primitives[0].material), trans});
	}

	if(node.camera != -1)
	{
		auto cam = doc.cameras[node.camera];
		camera_ = camera{
			trans,
			cam.perspective.yfov,
			cam.perspective.aspectRatio,
			cam.perspective.znear,
			cam.perspective.zfar,
			renderer};
	}

	if(auto ext = node.extensionsAndExtras.find("extensions"); ext != node.extensionsAndExtras.end())
	{
		if(auto punctual = ext.value().find("KHR_lights_punctual"); punctual != ext->end())
		{
			auto light_index = punctual->at("light").get<int>();
			auto lights = doc.extensionsAndExtras.at("extensions").at("KHR_lights_punctual").at("lights");
			auto light = lights.at(light_index);
			auto type = light.at("type").get<std::string>();
			auto color_iter = light.find("color");
			auto intensity_iter = light.find("intensity");
			auto range_iter = light.find("range");
			auto color = color_iter != light.end() ? color_iter->get<std::array<float, 3>>() :
													 std::array<float, 3>{1.0f, 1.0f, 1.0f};
			auto intensity = intensity_iter != light.end() ? intensity_iter->get<float>() : 1.0f;
			auto color_vec = glm::vec3{color[0], color[1], color[2]} * intensity;
			const auto ambient_multiplier = 0.1f;

			if(type == "directional")
			{
				if(dir_lights_.size() == num_directional_lights)
				{
					std::cout << "Too many directional lights in scene!\n";
				}
				else
				{
					dir_lights_.emplace_back(trans, ambient_multiplier * color_vec, color_vec, color_vec);
				}
			}
			else if(type == "point")
			{
				if(point_lights_.size() == num_point_lights)
				{
					std::cout << "Too many point lights in scene!\n";
				}
				else
				{
					point_lights_.emplace_back(
						trans, ambient_multiplier * color_vec, color_vec, color_vec, 1.0f, 0.022f, 0.0019f);
				}
			}
			else if(type == "spot")
			{
				if(spot_lights_.size() == num_spot_lights)
				{
					std::cout << "Too many spot lights in scene!\n";
				}
				else
				{
					auto spot = light.at("spot");
					auto inner_iter = spot.find("innerConeAngle");
					auto outer_iter = spot.find("outerConeAngle");
					auto inner_angle = inner_iter != spot.end() ? inner_iter->get<float>() : 0.0f;
					auto outer_angle = outer_iter != spot.end() ? outer_iter->get<float>() : glm::pi<float>() / 4.0f;

					spot_lights_.emplace_back(
						trans,
						ambient_multiplier * color_vec,
						color_vec,
						color_vec,
						1.0f,
						0.022f,
						0.0019f,
						glm::cos(inner_angle),
						glm::cos(outer_angle));
				}
			}
		}
	}

	for(auto child : node.children)
	{
		auto& child_node = doc.nodes.at(child);
		load_node(doc, child_node, trans, game, renderer);
	}
}

auto vkpg::get_indices(const fx::gltf::Document& doc, const fx::gltf::Primitive& primitive) -> std::vector<uint32_t>
{
	const auto accessor = doc.accessors[primitive.indices];
	const auto buffer_view = doc.bufferViews[accessor.bufferView];
	const auto buffer = doc.buffers[buffer_view.buffer];
	const auto start = buffer_view.byteOffset + accessor.byteOffset;
	const auto count = accessor.count;
	const auto data = buffer.data;

	// Should avoid buffer overflow prone input data.
	if(data.size() - start < count / sizeof(uint16_t))
	{
		throw std::runtime_error{"Buffer not big enough! Invalid glTF file."};
	}

	auto indices = std::vector<uint32_t>(count);

	// Can't do a simple memcpy as source type is different than destination type.
	// NOTE: The type can differ and should be dependent on componentType of the accessor.
	const auto start_ptr = reinterpret_cast<const uint16_t*>(&data[start]);
	for(auto i = decltype(count){0}; i < count; ++i)
	{
		auto index = static_cast<uint32_t>(start_ptr[i]);
		indices.push_back(index);
	}

	return indices;
}

auto vkpg::get_positions(const fx::gltf::Document& doc, const fx::gltf::Primitive& primitive) -> std::vector<glm::vec3>
{
	const auto accessor = doc.accessors[primitive.attributes.at("POSITION")];
	const auto buffer_view = doc.bufferViews[accessor.bufferView];
	const auto buffer = doc.buffers[buffer_view.buffer];
	const auto start = buffer_view.byteOffset + accessor.byteOffset;
	const auto count = accessor.count;
	const auto data = buffer.data;

	// Should avoid buffer overflow prone input data.
	if(data.size() - start < count / sizeof(glm::vec3))
	{
		throw std::runtime_error{"Buffer not big enough! Invalid glTF file."};
	}

	auto positions = std::vector<glm::vec3>(count);

	std::memcpy(positions.data(), data.data() + start, count * sizeof(glm::vec3));

	return positions;
}

auto vkpg::get_normals(const fx::gltf::Document& doc, const fx::gltf::Primitive& primitive) -> std::vector<glm::vec3>
{
	const auto accessor = doc.accessors[primitive.attributes.at("NORMAL")];
	const auto buffer_view = doc.bufferViews[accessor.bufferView];
	const auto buffer = doc.buffers[buffer_view.buffer];
	const auto start = buffer_view.byteOffset + accessor.byteOffset;
	const auto count = accessor.count;
	const auto data = buffer.data;

	// Should avoid buffer overflow prone input data.
	if(data.size() - start < count / sizeof(glm::vec3))
	{
		throw std::runtime_error{"Buffer not big enough! Invalid glTF file."};
	}

	auto normals = std::vector<glm::vec3>(count);

	std::memcpy(normals.data(), data.data() + start, count * sizeof(glm::vec3));

	return normals;
}

auto vkpg::get_colors(const fx::gltf::Document& doc, const fx::gltf::Primitive& primitive) -> std::vector<glm::vec3>
{
	const auto accessor = doc.accessors[primitive.attributes.at("POSITION")];
	const auto count = accessor.count;

	return std::vector<glm::vec3>(count);
}

auto vkpg::get_texcoords(const fx::gltf::Document& doc, const fx::gltf::Primitive& primitive) -> std::vector<glm::vec2>
{
	const auto accessor = doc.accessors[primitive.attributes.at("TEXCOORD_0")];
	const auto buffer_view = doc.bufferViews[accessor.bufferView];
	const auto buffer = doc.buffers[buffer_view.buffer];
	const auto start = buffer_view.byteOffset + accessor.byteOffset;
	const auto count = accessor.count;
	const auto data = buffer.data;

	// Should avoid buffer overflow prone input data.
	if(data.size() - start < count / sizeof(glm::vec2))
	{
		throw std::runtime_error{"Buffer not big enough! Invalid glTF file."};
	}

	auto texcoords = std::vector<glm::vec2>(count);

	std::memcpy(texcoords.data(), data.data() + start, count * sizeof(glm::vec2));

	return texcoords;
}
