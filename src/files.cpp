#include <fstream>

#include <files.hpp>

auto vkpg::read_file(const std::filesystem::path& file_path) -> std::vector<char>
{
	auto file = std::ifstream{file_path, std::ios::ate | std::ios::binary};

	if(!file.is_open())
	{
		throw std::runtime_error{"failed to open file!"};
	}

	const auto file_size = static_cast<size_t>(file.tellg());
	auto buffer = std::vector<char>(file_size);
	file.seekg(0);
	file.read(buffer.data(), file_size);

	file.close();

	return buffer;
}
