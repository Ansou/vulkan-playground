#pragma once

#include <unordered_set>

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

namespace vkpg
{
	class input_handler
	{
	public:
		explicit input_handler(GLFWwindow* window);

		void reset();
		void update_key(int key, int action);
		void update_mouse();
		void update_mouse(double xpos, double ypos);

		[[nodiscard]] bool key_down(int key) const;
		[[nodiscard]] bool key_pressed(int key) const;
		[[nodiscard]] glm::vec<2, double> mouse_moved() const;
		[[nodiscard]] glm::vec<2, double> mouse_pos() const;
		[[nodiscard]] glm::vec2 left_stick() const;
		[[nodiscard]] glm::vec2 right_stick() const;
		[[nodiscard]] float trigger() const;

		static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos);

	private:
		GLFWwindow* window_;

		std::unordered_set<int> keys_pressed_{};
		glm::vec<2, double> prev_mouse_{mouse_pos()};
		glm::vec<2, double> mouse_moved_{};
	};
}
