#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 fragNormal;
layout(location = 1) in vec2 fragTexCoord;
layout(location = 2) in vec3 fragPosition;

layout(location = 0) out vec4 outColor;

layout(set = 0, binding = 0) uniform ModelObject {
    mat4 model;
} modelUbo;

layout(set = 1, binding = 0) uniform CameraObject {
    mat4 view;
    mat4 proj;
} cameraUbo;

layout(set = 2, binding = 0) uniform Material
{
    float shininess;
    bool masking;
} material;

layout(set = 2, binding = 1) uniform sampler2D texSampler;

layout(set = 2, binding = 2) uniform sampler2D specSampler;

struct DirectionalLight
{
    vec4 direction;

    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
};

struct PointLight
{
    vec4 position;

    float constant;
    float linear;
    float quadratic;

    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
};

struct SpotLight
{
    vec4 position;
    vec4 direction;

    float cutoff;
    float outerCutoff;

    float constant;
    float linear;
    float quadratic;

    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
};

#define NR_DIR_LIGHTS 1
#define NR_POINT_LIGHTS 4
#define NR_SPOT_LIGHTS 1

layout(set = 3, binding = 0) uniform LightBuf
{
    int currentNumDirectionalLights;
    int currentNumPointLights;
    int currentNumSpotLights;
    DirectionalLight directionalLights[NR_DIR_LIGHTS];
    PointLight pointLights[NR_POINT_LIGHTS];
    SpotLight spotLights[NR_SPOT_LIGHTS];
};

vec3 calcDirLight(DirectionalLight light, vec3 normal, vec3 viewDir)
{
    vec3 lightDir = normalize(-vec3(cameraUbo.view * light.direction));
    // Diffuse
    float diff = max(dot(normal, lightDir), 0.0);
    // Specular
    vec3 halfwayDir = normalize(viewDir + lightDir);
    float spec = pow(max(dot(normal, halfwayDir), 0.0), material.shininess);
    // Combine results
    vec3 ambient = vec3(light.ambient) * vec3(texture(texSampler, fragTexCoord));
    vec3 diffuse = vec3(light.diffuse) * diff * vec3(texture(texSampler, fragTexCoord));
    vec3 specular = vec3(light.specular) * spec * vec3(texture(specSampler, fragTexCoord));

    return (ambient + diffuse + specular);
}

vec3 calcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightPosView = vec3(cameraUbo.view * vec4(vec3(light.position), 1.0));
    vec3 lightDir = normalize(lightPosView - fragPos);
    // Diffuse
    float diff = max(dot(normal, lightDir), 0.0);
    // Specular
    vec3 halfwayDir = normalize(viewDir + lightDir);
    float spec = pow(max(dot(normal, halfwayDir), 0.0), material.shininess);
    // Attenuation
    float distance = length(lightPosView - fragPos);
    float attenuation = 1.0 / (distance * distance);
    // Combine results
    vec3 ambient = vec3(light.ambient) * vec3(texture(texSampler, fragTexCoord));
    vec3 diffuse = vec3(light.diffuse) * diff * vec3(texture(texSampler, fragTexCoord));
    vec3 specular = vec3(light.specular) * spec * vec3(texture(specSampler, fragTexCoord));
    ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;

    return (ambient + diffuse + specular);
}

vec3 calcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightPosView = vec3(cameraUbo.view * vec4(vec3(light.position), 1.0));
    vec3 lightDir = normalize(lightPosView - fragPos);
    // Diffuse
    float diff = max(dot(normal, lightDir), 0.0);
    // Specular
    vec3 halfwayDir = normalize(viewDir + lightDir);
    float spec = pow(max(dot(normal, halfwayDir), 0.0), material.shininess);
    // Attenuation
    float distance = length(lightPosView - fragPos);
    float attenuation = 1.0 / (distance * distance);
    // Combine results
    vec3 ambient = vec3(light.ambient) * vec3(texture(texSampler, fragTexCoord));
    vec3 diffuse = vec3(light.diffuse) * diff * vec3(texture(texSampler, fragTexCoord));
    vec3 specular = vec3(light.specular) * spec * vec3(texture(specSampler, fragTexCoord));
    ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;

    // TODO: Check that this works correctly with scaling.
    vec3 phiView = vec3(cameraUbo.view * light.direction);
    float theta = dot(lightDir, normalize(-phiView));
    float epsilon = light.cutoff - light.outerCutoff;
    float intensity = clamp((theta - light.outerCutoff) / epsilon, 0.0, 1.0);

    diffuse *= intensity;
    specular *= intensity;

    return (ambient + diffuse + specular);
}

void main()
{
    vec4 texColor = texture(texSampler, fragTexCoord);
    if(material.masking && texColor.a < 0.1)
    {
        discard;
    }

    // Properties
    vec3 norm = normalize(fragNormal);
    vec3 viewDir = normalize(-fragPosition);

    vec3 result = vec3(0.0, 0.0, 0.0);

    // Directional lights
    for(int i = 0; i < NR_DIR_LIGHTS && i < currentNumDirectionalLights; ++i)
    {
        result += calcDirLight(directionalLights[i], norm, viewDir);
    }
    // Point lights
    for(int i = 0; i < NR_POINT_LIGHTS && i < currentNumPointLights; ++i)
    {
        result += calcPointLight(pointLights[i], norm, fragPosition, viewDir);
    }
    // Spot lights
    for(int i = 0; i < NR_SPOT_LIGHTS && i < currentNumSpotLights; ++i)
    {
        result += calcSpotLight(spotLights[i], norm, fragPosition, viewDir);
    }
    outColor = vec4(result, texColor.a);
}
