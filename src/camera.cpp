#include <camera.hpp>
#include <chrono>
#include <glm/glm.hpp>
#include <input.hpp>
#include <math_utils.hpp>
#include <renderer.hpp>

vkpg::camera::camera(
	transform& trans, float fov, float aspect_ratio, float near_plane, float far_plane, renderer const& renderer) :
	fov{fov},
	aspect_ratio{aspect_ratio},
	near_plane{near_plane},
	far_plane{far_plane},
	transform_{&trans},
	matrix_buffers_{create_uniform_buffers<camera_ubo>(renderer::max_frames_in_flight(), renderer.device())},
	matrix_buffers_memory_{bind_uniform_buffers_memory(matrix_buffers_, renderer)},
	descriptor_{renderer, renderer.descriptor_set_layout_camera(), get_frame_arr(matrix_buffers_)}
{
}

auto vkpg::camera::update(const input_handler& input, const std::chrono::duration<float> delta_time) -> void
{
	update_rotation(input, delta_time);
	update_position(input, delta_time);
}

auto vkpg::camera::update_matrix_buffer(renderer const& renderer, uint32_t current_image) -> void
{
	auto ubo = camera_ubo{};
	ubo.view = view_matrix();
	ubo.proj = projection_matrix();

	renderer.update_ubo(*matrix_buffers_memory_[current_image], ubo);
}

auto vkpg::camera::view_matrix() const -> glm::mat4x4
{
	return glm::inverse(transform_->world_matrix());
}

auto vkpg::camera::projection_matrix() const -> glm::mat4x4
{
	auto proj = glm::perspective(fov, aspect_ratio, near_plane, far_plane);
	proj[1][1] *= -1;
	return proj;
}

auto vkpg::camera::get_transform() const -> const transform&
{
	return *transform_;
}

auto vkpg::camera::descriptor(uint32_t current_frame) const -> vk::DescriptorSet const&
{
	return descriptor_.descriptor_set(current_frame);
}

auto vkpg::camera::update_rotation(const input_handler& input, std::chrono::duration<float> delta_time) -> void
{
	const auto rot_vec = input.mouse_moved();
	const auto right_stick = input.right_stick();
	horizontal_rotation_ -= rot_vec.x;
	vertical_rotation_ -= rot_vec.y;
	if(glm::length(right_stick) > 0.0f)
	{
		horizontal_rotation_ -= right_stick.x * delta_time.count();
		vertical_rotation_ -= right_stick.y * delta_time.count();
	}

	horizontal_rotation_ = normalize_angle(horizontal_rotation_);
	vertical_rotation_ = normalize_angle(
		glm::clamp(normalize_angle_half(vertical_rotation_), -glm::pi<float>() / 2.0f, glm::pi<float>() / 2.0f));

	transform_->set_world_rotation(glm::quat{glm::vec3{vertical_rotation_, horizontal_rotation_, 0.0f}});
}

auto vkpg::camera::update_position(const input_handler& input, std::chrono::duration<float> delta_time) -> void
{
	const auto left_stick = input.left_stick();
	const auto trigger = input.trigger();
	auto move_vec = glm::vec3{0, 0, 0};

	if(glm::length(left_stick) > 0.0f || std::abs(trigger) > 0.0f)
	{
		move_vec.z = left_stick.y;
		move_vec.x = left_stick.x;
		move_vec.y = trigger;
	}
	else
	{
		process_movement_buttons(input, move_vec);
	}
	transform_->position() += transform_->rotation() * move_vec * delta_time.count();
}

auto vkpg::camera::process_movement_buttons(const input_handler& input, glm::vec3& move_vec) const -> void
{
	if(input.key_down(GLFW_KEY_W) && !input.key_down(GLFW_KEY_S))
	{
		move_vec.z = -1.0f;
	}
	else if(input.key_down(GLFW_KEY_S) && !input.key_down(GLFW_KEY_W))
	{
		move_vec.z = 1.0f;
	}
	if(input.key_down(GLFW_KEY_D) && !input.key_down(GLFW_KEY_A))
	{
		move_vec.x = 1.0f;
	}
	else if(input.key_down(GLFW_KEY_A) && !input.key_down(GLFW_KEY_D))
	{
		move_vec.x = -1.0f;
	}
	if(input.key_down(GLFW_KEY_SPACE) && !input.key_down(GLFW_KEY_LEFT_SHIFT))
	{
		move_vec.y = 1.0f;
	}
	else if(input.key_down(GLFW_KEY_LEFT_SHIFT) && !input.key_down(GLFW_KEY_SPACE))
	{
		move_vec.y = -1.0f;
	}
}
