#include <GLFW/glfw3.h>
#include <game.hpp>

using namespace std::string_literals;

vkpg::game::game()
{
	glfwSetWindowUserPointer(window_, this);
	glfwSetFramebufferSizeCallback(window_, framebuffer_resize_callback);
}

vkpg::game::~game()
{
	glfwDestroyWindow(window_);
	glfwTerminate();
}

auto vkpg::game::run() -> void
{
	load_resources();

	scene_.create_skybox(*renderer_, *model_skybox_, *material_skybox_);
	scene_.load_gltf("scenes/shapes.gltf", *this, *renderer_);

	if(auto const cam = scene_.cam(); cam)
	{
		cam->aspect_ratio =
			renderer_->swapchain_extent_.width / static_cast<float>(renderer_->swapchain_extent_.height);
	}

	glfwSetInputMode(window_, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	main_loop();
}

auto vkpg::game::add_model(std::unique_ptr<model> m) -> void
{
	models_.push_back(std::move(m));
}

auto vkpg::game::add_texture(std::unique_ptr<texture> t) -> void
{
	textures_.push_back(std::move(t));
}

auto vkpg::game::add_material(std::unique_ptr<material_standard> m) -> void
{
	materials_standard_.push_back(std::move(m));
}

auto vkpg::game::get_model(const size_t index) const -> model const&
{
	return *models_.at(index);
}

auto vkpg::game::get_texture(const size_t index) const -> texture const&
{
	return *textures_.at(index);
}

auto vkpg::game::get_material(const size_t index) const -> material_standard const&
{
	return *materials_standard_.at(index);
}

auto vkpg::game::get_shader_standard() const -> shader_standard const&
{
	return *shader_standard_;
}

auto vkpg::game::create_window(const int width, const int height) -> GLFWwindow*
{
	glfwInit();
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

	return glfwCreateWindow(width, height, "Vulkan Playground - FPS: 0", nullptr, nullptr);
}

auto vkpg::game::main_loop() -> void
{
	auto frames = 0;
	auto prev = std::chrono::steady_clock::now();
	auto time_passed = std::chrono::steady_clock::duration{};
	while(!glfwWindowShouldClose(window_))
	{
		input_.reset();
		glfwPollEvents();
		input_.update_mouse();

		auto time = std::chrono::steady_clock::now();
		const auto delta_time = time - prev;
		prev = time;
		time_passed += delta_time;

		update(delta_time);
		render();

		++frames;
		if(time_passed >= std::chrono::seconds{1})
		{
			time_passed = {};
			update_fps_counter(frames);
			frames = 0;
		}
	}

	renderer_->wait_for_gpu();
}

auto vkpg::game::update(const std::chrono::steady_clock::duration delta_time) -> void
{
	const auto cursor_visible = glfwGetInputMode(window_, GLFW_CURSOR) == GLFW_CURSOR_NORMAL;
	if(input_.key_pressed(GLFW_KEY_ESCAPE))
	{
		glfwSetInputMode(window_, GLFW_CURSOR, cursor_visible ? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL);
	}
	if(!cursor_visible)
	{
		if(auto const cam = scene_.cam(); cam)
		{
			cam->update(input_, delta_time);
		}
	}
}

auto vkpg::game::render() -> void
{
	auto const cam = scene_.cam();
	if(renderer_->draw_frame(scene_.objects(), cam, scene_.get_skybox(), post_process_, scene_.lights_descriptor()))
	{
		if(cam)
		{
			cam->aspect_ratio =
				renderer_->swapchain_extent_.width / static_cast<float>(renderer_->swapchain_extent_.height);
		}
		std::for_each(
			materials_standard_.begin(), materials_standard_.end(), [&](std::unique_ptr<material_standard>& mat) {
				mat->recreate_pipeline(*renderer_, renderer_->scene_render_pass());
			});
		material_skybox_->recreate_pipeline(*renderer_, renderer_->scene_render_pass());
		post_process_material_.update_image_view(*renderer_->resolve_image_view_);
		post_process_.update_image_view(*renderer_, *renderer_->resolve_image_view_);
		post_process_material_.recreate_pipeline(*renderer_, *renderer_->post_proc_render_pass_);
	}
}

auto vkpg::game::framebuffer_resize_callback(GLFWwindow* window, int width, int height) -> void
{
	// Is this thread-safe?
	const auto app = reinterpret_cast<game*>(glfwGetWindowUserPointer(window));
	app->renderer_->window_resized_ = true;
}

auto vkpg::game::update_fps_counter(const int fps) -> void
{
	glfwSetWindowTitle(window_, ("Vulkan Playground - FPS: "s + std::to_string(fps)).c_str());
}

auto vkpg::game::get_input_handler() -> input_handler&
{
	return input_;
}

auto vkpg::game::load_resources() -> void
{
	shader_standard_ = std::make_unique<shader_standard>(
		*renderer_, std::filesystem::path{"shaders/shader.vert.spv"}, std::filesystem::path{"shaders/shader.frag.spv"});
	shader_skybox_ = std::make_unique<shader_skybox>(
		*renderer_, std::filesystem::path{"shaders/skybox.vert.spv"}, std::filesystem::path{"shaders/skybox.frag.spv"});
	model_skybox_ = std::make_unique<model>(std::filesystem::path{"scenes/skybox/model.obj"}, *renderer_);
	texture_skybox_ = std::make_unique<cubemap>(
		*renderer_,
		std::array<std::filesystem::path, 6>{
			std::filesystem::path{"scenes/skybox/right.png"},
			{"scenes/skybox/left.png"},
			{"scenes/skybox/top.png"},
			{"scenes/skybox/bottom.png"},
			{"scenes/skybox/front.png"},
			{"scenes/skybox/back.png"}});
	material_skybox_ = std::make_unique<material_skybox>(
		*renderer_, *shader_skybox_, *texture_skybox_, renderer_->scene_render_pass());
}
