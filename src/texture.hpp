#pragma once

#include <filesystem>

#include <vulkan/vulkan.hpp>

namespace vkpg
{
	class renderer;

	class texture
	{
	public:
		texture(renderer const& renderer, std::filesystem::path const& file_path);

		[[nodiscard]] auto image_view() const -> vk::ImageView const&;
		[[nodiscard]] auto sampler() const -> vk::Sampler const&;

	private:
		static auto load_image(
			const std::filesystem::path& file_path,
			renderer const& renderer) -> std::tuple<vk::UniqueImage, vk::UniqueDeviceMemory, uint32_t>;
		static auto create_sampler(uint32_t mip_levels, vk::Device const& device) -> vk::UniqueSampler;

		uint32_t mip_levels_;
		vk::UniqueImage image_;
		vk::UniqueDeviceMemory image_memory_;
		vk::UniqueImageView image_view_;
		vk::UniqueSampler sampler_;

		friend class render_component;
	};

	class cubemap
	{
	public:
		cubemap(renderer const& renderer, std::array<std::filesystem::path, 6> const& texture_paths);

		[[nodiscard]] auto image_view() const -> vk::ImageView const&;
		[[nodiscard]] auto sampler() const -> vk::Sampler const&;

	private:
		[[nodiscard]] static auto load_image(
			std::array<std::filesystem::path, 6> const& texture_paths,
			renderer const& renderer) -> std::pair<vk::UniqueImage, vk::UniqueDeviceMemory>;
		[[nodiscard]] static auto create_image_view(
			renderer const& renderer,
			vk::Image& image,
			vk::Format format,
			const vk::ImageAspectFlags&
			aspect_flags) -> vk::UniqueImageView;
		[[nodiscard]] static auto create_sampler(vk::Device const& device) -> vk::UniqueSampler;

		vk::UniqueImage image_;
		vk::UniqueDeviceMemory image_memory_;
		vk::UniqueImageView image_view_;
		vk::UniqueSampler sampler_;
	};
}
