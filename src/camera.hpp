#pragma once

#include <chrono>
#include <descriptors.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <transform.hpp>

namespace vkpg
{
	class input_handler;

	struct camera_ubo
	{
		glm::mat4 view{};
		glm::mat4 proj{};
	};

	class camera
	{
	public:
		camera(
			transform& trans,
			float fov,
			float aspect_ratio,
			float near_plane,
			float far_plane,
			renderer const& renderer);

		// TODO: Move this to another component.
		auto update(const input_handler& input, std::chrono::duration<float> delta_time) -> void;

		auto update_matrix_buffer(renderer const& renderer, uint32_t current_image) -> void;

		[[nodiscard]] auto view_matrix() const -> glm::mat4x4;
		[[nodiscard]] auto projection_matrix() const -> glm::mat4x4;

		[[nodiscard]] auto get_transform() const -> transform const&;
		[[nodiscard]] auto descriptor(uint32_t current_frame) const -> vk::DescriptorSet const&;

		// TODO: Make these private and make accessor functions.
		float fov{90.0f};
		float aspect_ratio{16.0f / 9.0f};
		float near_plane{0.1f};
		float far_plane{1000.0f};

	private:
		auto update_rotation(const input_handler& input, std::chrono::duration<float> delta_time) -> void;
		auto update_position(const input_handler& input, std::chrono::duration<float> delta_time) -> void;
		auto process_movement_buttons(const input_handler& input, glm::vec3& move_vec) const -> void;

		transform* transform_;

		float horizontal_rotation_{glm::eulerAngles(transform_->world_rotation()).y};
		float vertical_rotation_{glm::eulerAngles(transform_->world_rotation()).x};

		std::vector<vk::UniqueBuffer> matrix_buffers_;
		std::vector<vk::UniqueDeviceMemory> matrix_buffers_memory_;

		descriptor_camera descriptor_;
	};
}
