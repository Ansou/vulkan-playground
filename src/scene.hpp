#pragma once

#include <camera.hpp>
#include <filesystem>
#include <fx/gltf.h>
#include <glm/glm.hpp>
#include <light.hpp>
#include <render_component.hpp>
#include <renderer.hpp>
#include <skybox.hpp>
#include <vector>

namespace vkpg
{
	class scene
	{
	public:
		explicit scene(renderer const& renderer);

		[[nodiscard]] auto cam() -> camera*;
		[[nodiscard]] auto objects() -> std::vector<render_component>&;

		// TODO: Make a separate class for glTF loading.
		auto load_gltf(std::filesystem::path const& file, game& game, renderer const& renderer) -> void;
		auto create_skybox(renderer const& renderer, model const& model, material_skybox const& material) -> void;
		[[nodiscard]] auto get_skybox() -> skybox*;
		[[nodiscard]] auto get_skybox() const -> skybox const*;

		auto lights_descriptor() const -> descriptor_lights const&;

	private:
		auto add_render_component(render_component&& object) -> void;
		auto load_node(
			fx::gltf::Document const& doc,
			fx::gltf::Node const& node,
			transform& parent,
			game& game,
			renderer const& renderer) -> void;

		std::vector<directional_light> dir_lights_{};
		std::vector<point_light> point_lights_{};
		std::vector<spot_light> spot_lights_{};
		std::optional<camera> camera_{};
		std::vector<render_component> objects_{};
		std::unique_ptr<skybox> skybox_{};
		transform root_node_{glm::vec3{0.0f}, glm::vec3{1.0f}, glm::identity<glm::quat>()};

		vk::UniqueBuffer light_uniform_buffer_;
		vk::UniqueDeviceMemory light_uniform_buffer_memory_;

		descriptor_lights lights_descriptor_;
	};

	auto get_indices(const fx::gltf::Document& doc, const fx::gltf::Primitive& primitive) -> std::vector<uint32_t>;
	auto get_positions(const fx::gltf::Document& doc, const fx::gltf::Primitive& primitive) -> std::vector<glm::vec3>;
	auto get_normals(const fx::gltf::Document& doc, const fx::gltf::Primitive& primitive) -> std::vector<glm::vec3>;
	auto get_colors(const fx::gltf::Document& doc, const fx::gltf::Primitive& primitive) -> std::vector<glm::vec3>;
	auto get_texcoords(const fx::gltf::Document& doc, const fx::gltf::Primitive& primitive) -> std::vector<glm::vec2>;
}
