#include <algorithm>
#include <array>
#include <filesystem>
#include <iostream>
#include <optional>
#include <set>
#include <vulkan/vulkan.hpp>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#define TINYOBJLOADER_IMPLEMENTATION
#include <GLFW/glfw3.h>
#include <camera.hpp>
#include <files.hpp>
#include <material.hpp>
#include <post_process_effect.hpp>
#include <render_component.hpp>
#include <renderer.hpp>
#include <skybox.hpp>
#include <tiny_obj_loader.h>

#undef max
#undef min

using namespace std::string_literals;

const std::vector<const char*> validation_layers = {"VK_LAYER_LUNARG_standard_validation"};
const std::vector<const char*> device_extensions = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};

#ifdef NDEBUG
constexpr bool enable_validation_layers = false;
#else
constexpr bool enable_validation_layers = true;
#endif

auto vkpg::queue_family_indices::is_complete() const -> bool
{
	return graphics_family.has_value() && present_family.has_value();
}

vkpg::renderer::renderer(GLFWwindow* window) : window_{window}
{
	transition_image_layout(
		*color_msaa_image_,
		swapchain_image_format_,
		vk::ImageLayout::eUndefined,
		vk::ImageLayout::eColorAttachmentOptimal,
		1);
	transition_image_layout(
		*depth_image_,
		find_depth_format(physical_device_),
		vk::ImageLayout::eUndefined,
		vk::ImageLayout::eDepthStencilAttachmentOptimal,
		1);
}

auto vkpg::renderer::record_commands(
	std::vector<vkpg::render_component> const& objects,
	uint32_t current_image,
	vkpg::skybox const* skybox,
	post_process_effect const& post_process,
	camera const* camera,
	descriptor_lights const& lights_descriptor) -> void
{
	const auto i = current_frame_;

	auto begin_info = vk::CommandBufferBeginInfo{};
	begin_info.flags = vk::CommandBufferUsageFlagBits::eSimultaneousUse;
	begin_info.pInheritanceInfo = nullptr;

	auto clear_values = std::array<vk::ClearValue, 2>{};
	clear_values[0].color = vk::ClearColorValue{std::array<float, 4>{0.0f, 0.0f, 0.0f, 1.0f}};
	clear_values[1].depthStencil = vk::ClearDepthStencilValue{1.0f, 0};

	auto scene_render_pass_info = vk::RenderPassBeginInfo{};
	scene_render_pass_info.renderPass = *scene_render_pass_;
	scene_render_pass_info.framebuffer = *scene_framebuffer_;
	scene_render_pass_info.renderArea.offset = vk::Offset2D{0, 0};
	scene_render_pass_info.renderArea.extent = swapchain_extent_;
	scene_render_pass_info.clearValueCount = static_cast<uint32_t>(clear_values.size());
	scene_render_pass_info.pClearValues = clear_values.data();

	auto post_proc_render_pass_info = vk::RenderPassBeginInfo{};
	post_proc_render_pass_info.renderPass = *post_proc_render_pass_;
	post_proc_render_pass_info.framebuffer = *swap_chain_framebuffers_[current_image];
	post_proc_render_pass_info.renderArea.offset = vk::Offset2D{0, 0};
	post_proc_render_pass_info.renderArea.extent = swapchain_extent_;

	command_buffers_[i]->begin(begin_info);

	command_buffers_[i]->beginRenderPass(&scene_render_pass_info, vk::SubpassContents::eInline);
	if(skybox)
	{
		skybox->record_draw_commands(*command_buffers_[i], i);
	}
	for(auto& obj : objects)
	{
		assert(camera);
		obj.record_draw_commands(*command_buffers_[i], *camera, lights_descriptor, i);
	}
	command_buffers_[i]->endRenderPass();

	command_buffers_[i]->beginRenderPass(&post_proc_render_pass_info, vk::SubpassContents::eInline);
	post_process.record_draw_commands(*command_buffers_[i]);
	command_buffers_[i]->endRenderPass();

	command_buffers_[i]->end();
}

auto vkpg::create_post_proc_pipeline(
	vk::ShaderModule const& vert_shader,
	vk::ShaderModule const& frag_shader,
	vk::Device const& device,
	vk::Extent2D swap_chain_extent,
	vk::DescriptorSetLayout const& descriptor_set_layout,
	vk::PipelineLayout const& pipeline_layout,
	vk ::RenderPass const& render_pass) -> vk::UniquePipeline
{
	auto vert_shader_stage_info = vk::PipelineShaderStageCreateInfo{};
	vert_shader_stage_info.stage = vk::ShaderStageFlagBits::eVertex;
	vert_shader_stage_info.module = vert_shader;
	vert_shader_stage_info.pName = "main";

	auto frag_shader_stage_info = vk::PipelineShaderStageCreateInfo{};
	frag_shader_stage_info.stage = vk::ShaderStageFlagBits::eFragment;
	frag_shader_stage_info.module = frag_shader;
	frag_shader_stage_info.pName = "main";

	auto shader_stages =
		std::array<vk::PipelineShaderStageCreateInfo, 2>{vert_shader_stage_info, frag_shader_stage_info};

	auto rasterizer = vk::PipelineRasterizationStateCreateInfo{};
	rasterizer.depthClampEnable = false;
	rasterizer.rasterizerDiscardEnable = false;
	rasterizer.polygonMode = vk::PolygonMode::eFill;
	rasterizer.lineWidth = 1.0f;
	rasterizer.cullMode = vk::CullModeFlagBits::eNone;
	rasterizer.frontFace = vk::FrontFace::eCounterClockwise;
	rasterizer.depthBiasEnable = false;
	rasterizer.depthBiasConstantFactor = 0.0f;
	rasterizer.depthBiasClamp = 0.0f;
	rasterizer.depthBiasSlopeFactor = 0.0f;

	auto viewport = vk::Viewport{};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = static_cast<float>(swap_chain_extent.width);
	viewport.height = static_cast<float>(swap_chain_extent.height);
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	auto scissor = vk::Rect2D{};
	scissor.offset = vk::Offset2D{0, 0};
	scissor.extent = swap_chain_extent;

	auto viewport_state = vk::PipelineViewportStateCreateInfo{};
	viewport_state.viewportCount = 1;
	viewport_state.pViewports = &viewport;
	viewport_state.scissorCount = 1;
	viewport_state.pScissors = &scissor;

	auto multisampling = vk::PipelineMultisampleStateCreateInfo{};
	multisampling.sampleShadingEnable = false;
	multisampling.rasterizationSamples = vk::SampleCountFlagBits::e1;
	multisampling.minSampleShading = 1.0f;
	multisampling.pSampleMask = nullptr;
	multisampling.alphaToCoverageEnable = false;
	multisampling.alphaToOneEnable = false;

	auto input_assembly = vk::PipelineInputAssemblyStateCreateInfo{};
	input_assembly.topology = vk::PrimitiveTopology::eTriangleList;
	input_assembly.primitiveRestartEnable = false;

	auto vertex_input_info = vk::PipelineVertexInputStateCreateInfo{};
	vertex_input_info.vertexBindingDescriptionCount = 0;
	vertex_input_info.pVertexBindingDescriptions = nullptr;
	vertex_input_info.vertexAttributeDescriptionCount = 0;
	vertex_input_info.pVertexAttributeDescriptions = nullptr;

	auto color_blend_attachment = vk::PipelineColorBlendAttachmentState{};
	color_blend_attachment.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG |
		vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
	color_blend_attachment.blendEnable = false;

	auto color_blending = vk::PipelineColorBlendStateCreateInfo{};
	color_blending.logicOpEnable = false;
	color_blending.logicOp = vk::LogicOp::eCopy;
	color_blending.attachmentCount = 1;
	color_blending.pAttachments = &color_blend_attachment;
	color_blending.blendConstants[0] = 0.0f;
	color_blending.blendConstants[1] = 0.0f;
	color_blending.blendConstants[2] = 0.0f;
	color_blending.blendConstants[3] = 0.0f;

	auto depth_stencil = vk::PipelineDepthStencilStateCreateInfo{};
	depth_stencil.depthTestEnable = false;
	depth_stencil.depthWriteEnable = false;
	depth_stencil.depthCompareOp = vk::CompareOp::eLess;
	depth_stencil.depthBoundsTestEnable = false;
	depth_stencil.minDepthBounds = 0.0f;
	depth_stencil.maxDepthBounds = 1.0f;
	depth_stencil.stencilTestEnable = false;
	depth_stencil.front = vk::StencilOpState{};
	depth_stencil.back = vk::StencilOpState{};

	auto pipeline_info = vk::GraphicsPipelineCreateInfo{};
	pipeline_info.stageCount = shader_stages.size();
	pipeline_info.pStages = shader_stages.data();
	pipeline_info.pVertexInputState = &vertex_input_info;
	pipeline_info.pInputAssemblyState = &input_assembly;
	pipeline_info.pViewportState = &viewport_state;
	pipeline_info.pRasterizationState = &rasterizer;
	pipeline_info.pMultisampleState = &multisampling;
	pipeline_info.pDepthStencilState = &depth_stencil;
	pipeline_info.pColorBlendState = &color_blending;
	pipeline_info.pDynamicState = nullptr;
	pipeline_info.layout = pipeline_layout;
	pipeline_info.renderPass = render_pass;
	pipeline_info.subpass = 0;
	pipeline_info.basePipelineHandle = nullptr;
	pipeline_info.basePipelineIndex = -1;

	return device.createGraphicsPipelineUnique(nullptr, pipeline_info);
}

auto vkpg::create_post_proc_descriptor_set(
	vk::DescriptorSetLayout const& descriptor_set_layout,
	vk::DescriptorPool const& descriptor_pool,
	vk::Device const& device) -> vk::UniqueDescriptorSet
{
	auto layouts = std::vector<vk::DescriptorSetLayout>{1, descriptor_set_layout};
	auto alloc_info = vk::DescriptorSetAllocateInfo{};
	alloc_info.descriptorPool = descriptor_pool;
	alloc_info.descriptorSetCount = 1;
	alloc_info.pSetLayouts = layouts.data();

	auto descriptor_sets = device.allocateDescriptorSetsUnique(alloc_info);

	return std::move(descriptor_sets.at(0));
}

auto vkpg::create_post_proc_descriptor_pool(vk::Device const& device) -> vk::UniqueDescriptorPool
{
	auto pool_sizes = std::array<vk::DescriptorPoolSize, 1>{};
	pool_sizes[0].type = vk::DescriptorType::eCombinedImageSampler;
	pool_sizes[0].descriptorCount = 1;

	auto pool_info = vk::DescriptorPoolCreateInfo{};
	pool_info.poolSizeCount = static_cast<uint32_t>(pool_sizes.size());
	pool_info.pPoolSizes = pool_sizes.data();
	pool_info.maxSets = 1;
	pool_info.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;

	return device.createDescriptorPoolUnique(pool_info);
}

auto vkpg::create_post_proc_sampler(vk::Device const& device) -> vk::UniqueSampler
{
	auto sampler_info = vk::SamplerCreateInfo{};
	sampler_info.magFilter = vk::Filter::eLinear;
	sampler_info.minFilter = vk::Filter::eLinear;
	sampler_info.addressModeU = vk::SamplerAddressMode::eClampToEdge;
	sampler_info.addressModeV = vk::SamplerAddressMode::eClampToEdge;
	sampler_info.addressModeW = vk::SamplerAddressMode::eClampToEdge;
	sampler_info.anisotropyEnable = false;
	sampler_info.maxAnisotropy = 1.0f;
	sampler_info.borderColor = vk::BorderColor::eIntOpaqueBlack;
	sampler_info.unnormalizedCoordinates = false;
	sampler_info.compareEnable = false;
	sampler_info.compareOp = vk::CompareOp::eAlways;
	sampler_info.mipmapMode = vk::SamplerMipmapMode::eLinear;
	sampler_info.mipLodBias = 0.0f;
	sampler_info.minLod = 0.0f;
	sampler_info.maxLod = 1.0f;

	return device.createSamplerUnique(sampler_info);
}

auto vkpg::check_validation_layer_support() -> bool
{
	auto available_layers = vk::enumerateInstanceLayerProperties();

	for(const auto layer_name : validation_layers)
	{
		auto layer_found = false;

		for(const auto& layer_properties : available_layers)
		{
			if(strcmp(layer_name, layer_properties.layerName) == 0)
			{
				layer_found = true;
				break;
			}
		}
		if(!layer_found)
		{
			return false;
		}
	}

	return true;
}

auto vkpg::check_available_extensions() -> std::vector<vk::ExtensionProperties>
{
	auto extensions = vk::enumerateInstanceExtensionProperties();

	std::cout << "available extensions:\n";

	for(const auto& extension : extensions)
	{
		std::cout << "\t" << extension.extensionName << "\n";
	}

	return extensions;
}

auto vkpg::query_swapchain_support(vk::PhysicalDevice const& device, vk::SurfaceKHR const& surface)
	-> vkpg::swap_chain_support_details
{
	auto details = swap_chain_support_details{};

	details.capabilities = device.getSurfaceCapabilitiesKHR(surface);
	details.formats = device.getSurfaceFormatsKHR(surface);
	details.present_modes = device.getSurfacePresentModesKHR(surface);

	return details;
}

auto vkpg::get_max_usable_sample_count(vk::PhysicalDevice const& physical_device) -> vk::SampleCountFlagBits
{
	auto physical_device_properties = vk::PhysicalDeviceProperties{};
	physical_device.getProperties(&physical_device_properties);

	const auto color_count = physical_device_properties.limits.framebufferColorSampleCounts;
	const auto depth_count = physical_device_properties.limits.framebufferDepthSampleCounts;
	if(color_count & depth_count & vk::SampleCountFlagBits::e64)
	{
		return vk::SampleCountFlagBits::e64;
	}
	if(color_count & depth_count & vk::SampleCountFlagBits::e32)
	{
		return vk::SampleCountFlagBits::e32;
	}
	if(color_count & depth_count & vk::SampleCountFlagBits::e16)
	{
		return vk::SampleCountFlagBits::e16;
	}
	if(color_count & depth_count & vk::SampleCountFlagBits::e8)
	{
		return vk::SampleCountFlagBits::e8;
	}
	if(color_count & depth_count & vk::SampleCountFlagBits::e4)
	{
		return vk::SampleCountFlagBits::e4;
	}
	if(color_count & depth_count & vk::SampleCountFlagBits::e2)
	{
		return vk::SampleCountFlagBits::e2;
	}

	return vk::SampleCountFlagBits::e1;
}

auto vkpg::find_queue_families(vk::PhysicalDevice const& device, vk::SurfaceKHR const& surface)
	-> vkpg::queue_family_indices
{
	auto indices = queue_family_indices{};

	auto queue_families = device.getQueueFamilyProperties();

	auto i = 0;
	for(const auto& queue_family : queue_families)
	{
		if(queue_family.queueCount > 0 && queue_family.queueFlags & vk::QueueFlagBits::eGraphics)
		{
			indices.graphics_family = i;
		}

		auto present_support = vk::Bool32{false};
		device.getSurfaceSupportKHR(i, surface, &present_support);
		if(queue_family.queueCount > 0 && present_support)
		{
			indices.present_family = i;
		}

		if(indices.is_complete())
		{
			break;
		}

		++i;
	}

	return indices;
}

auto vkpg::get_required_extensions() -> std::vector<const char*>
{
	auto glfw_extension_count = uint32_t{0};
	const auto glfw_extensions = glfwGetRequiredInstanceExtensions(&glfw_extension_count);
	auto extensions = std::vector<const char*>{glfw_extensions, glfw_extensions + glfw_extension_count};

	if constexpr(enable_validation_layers)
	{
		extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
	}

	std::cout << "required extensions:\n";
	for(auto& extension : extensions)
	{
		std::cout << "\t" << extension << "\n";
	}

	return extensions;
}

auto vkpg::check_required_extensions_present(
	std::vector<vk::ExtensionProperties> const& available_extensions,
	std::vector<const char*> const& required_extensions) -> void
{
	for(auto& required_extension : required_extensions)
	{
		auto found = false;
		for(const auto& extension : available_extensions)
		{
			if(std::strcmp(required_extension, extension.extensionName) != 0)
			{
				found = true;
			}
		}
		if(!found)
		{
			throw std::runtime_error("missing vulkan extension!");
		}
	}
	std::cout << "extension requirement fulfilled\n";
}

VKAPI_ATTR VkBool32 VKAPI_CALL vkpg::debug_callback(
	VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
	VkDebugUtilsMessageTypeFlagsEXT message_type,
	const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
	void* user_data)
{
	std::cerr << "validation layer: " << callback_data->pMessage << "\n";

	return false;
}

auto vkpg::renderer::recreate_swapchain() -> void
{
	auto width = 0;
	auto height = 0;
	while(width == 0 || height == 0)
	{
		glfwGetFramebufferSize(window_, &width, &height);
		glfwWaitEvents();
	}

	device_->waitIdle();

	std::cout << "Recreating swap chain\n";
	scene_framebuffer_.reset();
	std::for_each(swap_chain_framebuffers_.begin(), swap_chain_framebuffers_.end(), [](vk::UniqueFramebuffer& ptr) {
		ptr.reset();
	});
	resolve_image_view_.reset();
	resolve_image_memory_.reset();
	resolve_image_.reset();
	depth_image_view_.reset();
	depth_image_memory_.reset();
	depth_image_.reset();
	color_msaa_image_view_.reset();
	color_msaa_image_memory_.reset();
	color_msaa_image_.reset();
	post_proc_render_pass_.reset();
	scene_render_pass_.reset();
	std::for_each(
		swapchain_image_views_.begin(), swapchain_image_views_.end(), [](vk::UniqueImageView& ptr) { ptr.reset(); });
	swapchain_.reset();

	swapchain_ = create_swapchain(physical_device_, *surface_, *device_, window_);
	swapchain_images_ = device_->getSwapchainImagesKHR(*swapchain_);
	swapchain_image_format_ =
		choose_swapchain_surface_format(query_swapchain_support(physical_device_, *surface_).formats).format;
	swapchain_extent_ =
		choose_swapchain_extent(query_swapchain_support(physical_device_, *surface_).capabilities, window_);
	swapchain_image_views_ = create_image_views(swapchain_images_, swapchain_image_format_, *device_);
	scene_render_pass_ = create_scene_render_pass(swapchain_image_format_, msaa_samples_, *device_, physical_device_);
	post_proc_render_pass_ = create_post_proc_render_pass(swapchain_image_format_, *device_, physical_device_);
	color_msaa_image_ = create_image(
		*device_,
		swapchain_extent_.width,
		swapchain_extent_.height,
		1,
		msaa_samples_,
		swapchain_image_format_,
		vk::ImageTiling::eOptimal,
		vk::ImageUsageFlagBits::eTransientAttachment | vk::ImageUsageFlagBits::eColorAttachment);
	color_msaa_image_memory_ = bind_image_memory(*color_msaa_image_, vk::MemoryPropertyFlagBits::eDeviceLocal);
	color_msaa_image_view_ =
		create_image_view(*color_msaa_image_, swapchain_image_format_, vk::ImageAspectFlagBits::eColor, 1, *device_);
	depth_image_ = create_image(
		*device_,
		swapchain_extent_.width,
		swapchain_extent_.height,
		1,
		msaa_samples_,
		find_depth_format(physical_device_),
		vk::ImageTiling::eOptimal,
		vk::ImageUsageFlagBits::eDepthStencilAttachment);
	depth_image_memory_ = bind_image_memory(*depth_image_, vk::MemoryPropertyFlagBits::eDeviceLocal);
	depth_image_view_ = create_image_view(
		*depth_image_, find_depth_format(physical_device_), vk::ImageAspectFlagBits::eDepth, 1, *device_);
	resolve_image_ = create_image(
		*device_,
		swapchain_extent_.width,
		swapchain_extent_.height,
		1,
		vk::SampleCountFlagBits::e1,
		swapchain_image_format_,
		vk::ImageTiling::eOptimal,
		vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eSampled);
	resolve_image_memory_ = bind_image_memory(*resolve_image_, vk::MemoryPropertyFlagBits::eDeviceLocal);
	resolve_image_view_ =
		create_image_view(*resolve_image_, swapchain_image_format_, vk::ImageAspectFlagBits::eColor, 1, *device_);
	swap_chain_framebuffers_ =
		create_swapchain_framebuffers(swapchain_image_views_, *post_proc_render_pass_, swapchain_extent_, *device_);
	scene_framebuffer_ = create_scene_framebuffer(
		*color_msaa_image_view_,
		*depth_image_view_,
		*resolve_image_view_,
		*scene_render_pass_,
		swapchain_extent_,
		*device_);
}

auto vkpg::create_instance() -> vk::UniqueInstance
{
	if constexpr(enable_validation_layers)
	{
		if(!check_validation_layer_support())
		{
			throw std::runtime_error{"validation layers requested, but not available!"};
		}
	}

	auto app_info = vk::ApplicationInfo{};
	app_info.pApplicationName = "Hello Triangle";
	app_info.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	app_info.pEngineName = "No Engine";
	app_info.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	app_info.apiVersion = VK_API_VERSION_1_1;

	auto create_info = vk::InstanceCreateInfo{};
	create_info.pApplicationInfo = &app_info;

	auto extensions = get_required_extensions();
	check_required_extensions_present(check_available_extensions(), extensions);

	create_info.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
	create_info.ppEnabledExtensionNames = extensions.data();

	if constexpr(enable_validation_layers)
	{
		create_info.enabledLayerCount = static_cast<uint32_t>(validation_layers.size());
		create_info.ppEnabledLayerNames = validation_layers.data();
	}
	else
	{
		create_info.enabledLayerCount = 0;
	}

	return vk::createInstanceUnique(create_info);
}

auto vkpg::find_supported_format(
	std::vector<vk::Format> const& candidates,
	const vk::ImageTiling tiling,
	vk::FormatFeatureFlags const& features,
	vk::PhysicalDevice const& physical_device) -> vk::Format
{
	for(auto format : candidates)
	{
		auto props = vk::FormatProperties{};
		physical_device.getFormatProperties(format, &props);

		if(tiling == vk::ImageTiling::eLinear && (props.linearTilingFeatures & features) == features)
		{
			return format;
		}
		if(tiling == vk::ImageTiling::eOptimal && (props.optimalTilingFeatures & features) == features)
		{
			return format;
		}
	}

	throw std::runtime_error{"failed to find supported format!"};
}

auto vkpg::find_depth_format(vk::PhysicalDevice const& physical_device) -> vk::Format
{
	return find_supported_format(
		{vk::Format::eD32Sfloat, vk::Format::eD32SfloatS8Uint, vk::Format::eD24UnormS8Uint},
		vk::ImageTiling::eOptimal,
		vk::FormatFeatureFlagBits::eDepthStencilAttachment,
		physical_device);
}

auto vkpg::has_stencil_component(vk::Format const format) -> bool
{
	return format == vk::Format::eD32SfloatS8Uint || format == vk::Format::eD24UnormS8Uint;
}

auto vkpg::create_image_view(
	vk::Image const& image,
	vk::Format const format,
	vk::ImageAspectFlags const& aspect_flags,
	uint32_t const mip_levels,
	vk::Device const& device) -> vk::UniqueImageView
{
	auto view_info = vk::ImageViewCreateInfo{};
	view_info.image = image;
	view_info.viewType = vk::ImageViewType::e2D;
	view_info.format = format;
	view_info.subresourceRange.aspectMask = aspect_flags;
	view_info.subresourceRange.baseMipLevel = 0;
	view_info.subresourceRange.levelCount = mip_levels;
	view_info.subresourceRange.baseArrayLayer = 0;
	view_info.subresourceRange.layerCount = 1;

	return device.createImageViewUnique(view_info);
}

auto vkpg::renderer::generate_mipmaps(
	vk::Image const& image,
	vk::Format const& image_format,
	const int32_t tex_width,
	const int32_t tex_height,
	const uint32_t mip_levels) const -> void
{
	auto format_properties = vk::FormatProperties{};
	physical_device_.getFormatProperties(image_format, &format_properties);
	if(!(format_properties.optimalTilingFeatures & vk::FormatFeatureFlagBits::eSampledImageFilterLinear))
	{
		throw std::runtime_error{"texture image format does not support linear blitting!"};
	}

	auto command_buffer = begin_single_time_commands();

	auto barrier = vk::ImageMemoryBarrier{};
	barrier.image = image;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.layerCount = 1;
	barrier.subresourceRange.levelCount = 1;

	auto mip_width = tex_width;
	auto mip_height = tex_height;

	for(uint32_t i = 1; i < mip_levels; ++i)
	{
		barrier.subresourceRange.baseMipLevel = i - 1;
		barrier.oldLayout = vk::ImageLayout::eTransferDstOptimal;
		barrier.newLayout = vk::ImageLayout::eTransferSrcOptimal;
		barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
		barrier.dstAccessMask = vk::AccessFlagBits::eTransferRead;

		command_buffer->pipelineBarrier(
			vk::PipelineStageFlagBits::eTransfer,
			vk::PipelineStageFlagBits::eTransfer,
			vk::DependencyFlags{},
			0,
			nullptr,
			0,
			nullptr,
			1,
			&barrier);

		auto blit = vk::ImageBlit{};
		blit.srcOffsets[0] = vk::Offset3D{0, 0, 0};
		blit.srcOffsets[1] = vk::Offset3D{mip_width, mip_height, 1};
		blit.srcSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
		blit.srcSubresource.mipLevel = i - 1;
		blit.srcSubresource.baseArrayLayer = 0;
		blit.srcSubresource.layerCount = 1;
		blit.dstOffsets[0] = vk::Offset3D{0, 0, 0};
		blit.dstOffsets[1] = vk::Offset3D{mip_width > 1 ? mip_width / 2 : 1, mip_height > 1 ? mip_height / 2 : 1, 1};
		blit.dstSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
		blit.dstSubresource.mipLevel = i;
		blit.dstSubresource.baseArrayLayer = 0;
		blit.dstSubresource.layerCount = 1;

		command_buffer->blitImage(
			image,
			vk::ImageLayout::eTransferSrcOptimal,
			image,
			vk::ImageLayout::eTransferDstOptimal,
			1,
			&blit,
			vk::Filter::eLinear);

		barrier.oldLayout = vk::ImageLayout::eTransferSrcOptimal;
		barrier.newLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
		barrier.srcAccessMask = vk::AccessFlagBits::eTransferRead;
		barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

		command_buffer->pipelineBarrier(
			vk::PipelineStageFlagBits::eTransfer,
			vk::PipelineStageFlagBits::eFragmentShader,
			vk::DependencyFlags{},
			0,
			nullptr,
			0,
			nullptr,
			1,
			&barrier);

		if(mip_width > 1)
		{
			mip_width /= 2;
		}
		if(mip_height > 1)
		{
			mip_height /= 2;
		}
	}

	barrier.subresourceRange.baseMipLevel = mip_levels - 1;
	barrier.oldLayout = vk::ImageLayout::eTransferDstOptimal;
	barrier.newLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
	barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
	barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

	command_buffer->pipelineBarrier(
		vk::PipelineStageFlagBits::eTransfer,
		vk::PipelineStageFlagBits::eFragmentShader,
		vk::DependencyFlags{},
		0,
		nullptr,
		0,
		nullptr,
		1,
		&barrier);

	end_single_time_commands(*command_buffer);
}

auto vkpg::renderer::wait_for_gpu() -> void
{
	device_->waitIdle();
}

auto vkpg::renderer::current_resolution() const -> vk::Extent2D
{
	return swapchain_extent_;
}

auto vkpg::renderer::load_shader(std::filesystem::path const& shader_path) const -> vk::UniqueShaderModule
{
	return create_shader_module(read_file(shader_path));
}

auto vkpg::create_post_proc_descriptor_set_layout(vk::Device const& device) -> vk::UniqueDescriptorSetLayout
{
	auto sampler_layout_binding = vk::DescriptorSetLayoutBinding{};
	sampler_layout_binding.binding = 0;
	sampler_layout_binding.descriptorCount = 1;
	sampler_layout_binding.descriptorType = vk::DescriptorType::eCombinedImageSampler;
	sampler_layout_binding.pImmutableSamplers = nullptr;
	sampler_layout_binding.stageFlags = vk::ShaderStageFlagBits::eFragment;

	auto bindings = std::array<vk::DescriptorSetLayoutBinding, 1>{sampler_layout_binding};
	auto layout_info = vk::DescriptorSetLayoutCreateInfo{};
	layout_info.bindingCount = static_cast<uint32_t>(bindings.size());
	layout_info.pBindings = bindings.data();

	return device.createDescriptorSetLayoutUnique(layout_info);
}

auto vkpg::create_post_proc_pipeline_layout(
	vk::Device const& device, vk::DescriptorSetLayout const& descriptor_set_layout) -> vk::UniquePipelineLayout
{
	auto pipeline_layout_info = vk::PipelineLayoutCreateInfo{};
	pipeline_layout_info.setLayoutCount = 1;
	pipeline_layout_info.pSetLayouts = &descriptor_set_layout;
	pipeline_layout_info.pushConstantRangeCount = 0;
	pipeline_layout_info.pPushConstantRanges = nullptr;

	return device.createPipelineLayoutUnique(pipeline_layout_info);
}

auto vkpg::create_image_available_semaphores(vk::Device const& device, const int max_frames_in_flight)
	-> std::vector<vk::UniqueSemaphore>
{
	auto semaphores = std::vector<vk::UniqueSemaphore>{};
	for(auto i = 0; i < max_frames_in_flight; ++i)
	{
		semaphores.push_back(device.createSemaphoreUnique({}));
	}
	return semaphores;
}

auto vkpg::create_render_finished_semaphores(vk::Device const& device, const int max_frames_in_flight)
	-> std::vector<vk::UniqueSemaphore>
{
	auto semaphores = std::vector<vk::UniqueSemaphore>{};
	for(auto i = 0; i < max_frames_in_flight; ++i)
	{
		semaphores.push_back(device.createSemaphoreUnique({}));
	}
	return semaphores;
}

auto vkpg::create_in_flight_fences(vk::Device const& device, const int max_frames_in_flight)
	-> std::vector<vk::UniqueFence>
{
	auto fence_info = vk::FenceCreateInfo{};
	fence_info.flags = vk::FenceCreateFlagBits::eSignaled;
	auto fences = std::vector<vk::UniqueFence>{};
	for(auto i = 0; i < max_frames_in_flight; ++i)
	{
		fences.push_back(device.createFenceUnique(fence_info));
	}
	return fences;
}

auto vkpg::create_buffer(const vk::DeviceSize size, vk::BufferUsageFlags const& usage, vk::Device const& device)
	-> vk::UniqueBuffer
{
	vk::BufferCreateInfo buffer_info = {};
	buffer_info.size = size;
	buffer_info.usage = usage;
	buffer_info.sharingMode = vk::SharingMode::eExclusive;

	return device.createBufferUnique(buffer_info);
}

auto vkpg::bind_uniform_buffers_memory(std::vector<vk::UniqueBuffer> const& buffers, renderer const& renderer)
	-> std::vector<vk::UniqueDeviceMemory>
{
	auto memories = std::vector<vk::UniqueDeviceMemory>{};
	for(auto& buffer : buffers)
	{
		memories.push_back(renderer.bind_buffer_memory(
			*buffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits ::eHostCoherent));
	}
	return memories;
}

auto vkpg::renderer::bind_buffer_memory(vk::Buffer const& buffer, vk::MemoryPropertyFlags const& properties) const
	-> vk::UniqueDeviceMemory
{
	const auto memory_requirements = device().getBufferMemoryRequirements(buffer);

	vk::MemoryAllocateInfo alloc_info = {};
	alloc_info.allocationSize = memory_requirements.size;
	alloc_info.memoryTypeIndex = find_memory_type(memory_requirements.memoryTypeBits, properties, physical_device());

	auto memory = device().allocateMemoryUnique(alloc_info);

	device().bindBufferMemory(buffer, *memory, 0);

	return memory;
}

auto vkpg::renderer::create_descriptor_set(
	vk::DescriptorSetLayout const& descriptor_set_layout, vk::DescriptorPool const& descriptor_pool) const
	-> vk::UniqueDescriptorSet
{
	auto descriptor_sets = create_descriptor_sets(descriptor_set_layout, descriptor_pool, 1);
	return std::move(descriptor_sets.at(0));
}

auto vkpg::renderer::create_descriptor_sets(
	vk::DescriptorSetLayout const& descriptor_set_layout,
	vk::DescriptorPool const& descriptor_pool,
	const int num_sets) const -> std::vector<vk::UniqueDescriptorSet>
{
	auto layouts = std::vector<vk::DescriptorSetLayout>{static_cast<size_t>(num_sets), descriptor_set_layout};
	auto alloc_info = vk::DescriptorSetAllocateInfo{};
	alloc_info.descriptorPool = descriptor_pool;
	alloc_info.descriptorSetCount = static_cast<uint32_t>(num_sets);
	alloc_info.pSetLayouts = layouts.data();

	return device().allocateDescriptorSetsUnique(alloc_info);
}

void vkpg::renderer::copy_buffer(
	vk::Buffer const& src_buffer, vk::Buffer const& dst_buffer, const vk::DeviceSize size) const
{
	auto command_buffer = begin_single_time_commands();

	auto copy_region = vk::BufferCopy{};
	copy_region.size = size;
	command_buffer->copyBuffer(src_buffer, dst_buffer, 1, &copy_region);

	end_single_time_commands(*command_buffer);
}

auto vkpg::renderer::begin_single_time_commands() const -> vk::UniqueCommandBuffer
{
	auto alloc_info = vk::CommandBufferAllocateInfo{};
	alloc_info.level = vk::CommandBufferLevel::ePrimary;
	alloc_info.commandPool = *command_pool_;
	alloc_info.commandBufferCount = 1;

	auto command_buffer = device_->allocateCommandBuffersUnique(alloc_info);

	auto begin_info = vk::CommandBufferBeginInfo{};
	begin_info.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit;

	command_buffer[0]->begin(begin_info);

	return std::move(command_buffer[0]);
}

auto vkpg::renderer::end_single_time_commands(vk::CommandBuffer const& command_buffer) const -> void
{
	command_buffer.end();

	auto submit_info = std::array<vk::SubmitInfo, 1>{};
	submit_info[0].commandBufferCount = 1;
	submit_info[0].pCommandBuffers = &command_buffer;

	graphics_queue_.submit(submit_info, nullptr);
	graphics_queue_.waitIdle();
}

// Stages and access masks may not be very necessary for current use cases.
auto vkpg::renderer::transition_image_layout(
	vk::Image const& image,
	const vk::Format format,
	const vk::ImageLayout old_layout,
	const vk::ImageLayout new_layout,
	vk::ImageSubresourceRange const& subresource_range) const -> void
{
	auto command_buffer = begin_single_time_commands();

	auto barrier = vk::ImageMemoryBarrier{};
	barrier.oldLayout = old_layout;
	barrier.newLayout = new_layout;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.image = image;
	barrier.subresourceRange = subresource_range;

	if(new_layout == vk::ImageLayout::eDepthStencilAttachmentOptimal)
	{
		barrier.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eDepth;
		if(has_stencil_component(format))
		{
			barrier.subresourceRange.aspectMask |= vk::ImageAspectFlagBits::eStencil;
		}
	}
	else
	{
		barrier.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
	}

	auto source_stage = vk::PipelineStageFlags{};
	auto destination_stage = vk::PipelineStageFlags{};

	if(old_layout == vk::ImageLayout::eUndefined && new_layout == vk::ImageLayout::eTransferDstOptimal)
	{
		barrier.srcAccessMask = vk::AccessFlags{};
		barrier.dstAccessMask = vk::AccessFlagBits::eTransferWrite;

		source_stage = vk::PipelineStageFlagBits::eTopOfPipe;
		destination_stage = vk::PipelineStageFlagBits::eTransfer;
	}
	else if(old_layout == vk::ImageLayout::eTransferDstOptimal && new_layout == vk::ImageLayout::eShaderReadOnlyOptimal)
	{
		barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
		barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

		source_stage = vk::PipelineStageFlagBits::eTransfer;
		destination_stage = vk::PipelineStageFlagBits::eFragmentShader;
	}
	else if(old_layout == vk::ImageLayout::eUndefined && new_layout == vk::ImageLayout::eDepthStencilAttachmentOptimal)
	{
		barrier.srcAccessMask = vk::AccessFlags{};
		barrier.dstAccessMask =
			vk::AccessFlagBits::eDepthStencilAttachmentRead | vk::AccessFlagBits::eDepthStencilAttachmentWrite;

		source_stage = vk::PipelineStageFlagBits::eTopOfPipe;
		destination_stage = vk::PipelineStageFlagBits::eEarlyFragmentTests;
	}
	else if(old_layout == vk::ImageLayout::eUndefined && new_layout == vk::ImageLayout::eColorAttachmentOptimal)
	{
		barrier.srcAccessMask = vk::AccessFlags{};
		barrier.dstAccessMask = vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite;

		source_stage = vk::PipelineStageFlagBits::eTopOfPipe;
		destination_stage = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	}
	else
	{
		throw std::invalid_argument{"unsupported layout transition!"};
	}

	command_buffer->pipelineBarrier(
		source_stage, destination_stage, vk::DependencyFlags{}, 0, nullptr, 0, nullptr, 1, &barrier);

	end_single_time_commands(*command_buffer);
}

auto vkpg::renderer::transition_image_layout(
	vk::Image const& image,
	vk::Format format,
	vk::ImageLayout old_layout,
	vk::ImageLayout new_layout,
	uint32_t mip_levels) const -> void
{
	auto subresource_range = vk::ImageSubresourceRange{};
	subresource_range.baseMipLevel = 0;
	subresource_range.levelCount = mip_levels;
	subresource_range.baseArrayLayer = 0;
	subresource_range.layerCount = 1;
	transition_image_layout(image, format, old_layout, new_layout, subresource_range);
}

auto vkpg::renderer::copy_buffer_to_image(
	vk::Buffer const& buffer, vk::Image const& image, const uint32_t width, const uint32_t height) const -> void
{
	auto command_buffer = begin_single_time_commands();

	auto region = vk::BufferImageCopy{};
	region.bufferOffset = 0;
	region.bufferRowLength = 0;
	region.bufferImageHeight = 0;

	region.imageSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
	region.imageSubresource.mipLevel = 0;
	region.imageSubresource.baseArrayLayer = 0;
	region.imageSubresource.layerCount = 1;

	region.imageOffset = vk::Offset3D{0, 0, 0};
	region.imageExtent = vk::Extent3D{width, height, 1};

	command_buffer->copyBufferToImage(buffer, image, vk::ImageLayout::eTransferDstOptimal, 1, &region);

	end_single_time_commands(*command_buffer);
}

auto vkpg::renderer::copy_buffer_to_image(
	vk::Buffer const& buffer, vk::Image const& image, std::vector<vk::BufferImageCopy> const& copies) const -> void
{
	auto command_buffer = begin_single_time_commands();

	command_buffer->copyBufferToImage(
		buffer, image, vk::ImageLayout::eTransferDstOptimal, copies.size(), copies.data());

	end_single_time_commands(*command_buffer);
}

auto vkpg::find_memory_type(
	const uint32_t type_filter, vk::MemoryPropertyFlags const& properties, vk::PhysicalDevice const& physical_device)
	-> uint32_t
{
	auto mem_properties = vk::PhysicalDeviceMemoryProperties{};
	physical_device.getMemoryProperties(&mem_properties);

	for(uint32_t i = 0; i < mem_properties.memoryTypeCount; ++i)
	{
		if(type_filter & (1 << i) && (mem_properties.memoryTypes[i].propertyFlags & properties) == properties)
		{
			return i;
		}
	}

	throw std::runtime_error{"failed to find suitable memory type!"};
}

auto vkpg::create_command_buffers(
	vk::CommandPool const& command_pool, vk::Device const& device, const uint32_t num_buffers)
	-> std::vector<vk::UniqueCommandBuffer>
{
	auto alloc_info = vk::CommandBufferAllocateInfo{};
	alloc_info.commandPool = command_pool;
	alloc_info.level = vk::CommandBufferLevel::ePrimary;
	alloc_info.commandBufferCount = num_buffers;

	return device.allocateCommandBuffersUnique(alloc_info);
}

auto vkpg::create_descriptor_set_layout_transform(vk::Device const& device) -> vk::UniqueDescriptorSetLayout
{
	auto ubo_layout_binding = vk::DescriptorSetLayoutBinding{};
	ubo_layout_binding.binding = 0;
	ubo_layout_binding.descriptorType = vk::DescriptorType::eUniformBuffer;
	ubo_layout_binding.descriptorCount = 1;
	ubo_layout_binding.stageFlags = vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment;
	ubo_layout_binding.pImmutableSamplers = nullptr;

	auto bindings = std::array<vk::DescriptorSetLayoutBinding, 1>{ubo_layout_binding};
	auto layout_info = vk::DescriptorSetLayoutCreateInfo{};
	layout_info.bindingCount = static_cast<uint32_t>(bindings.size());
	layout_info.pBindings = bindings.data();

	return device.createDescriptorSetLayoutUnique(layout_info);
}

auto vkpg::create_descriptor_set_layout_camera(vk::Device const& device) -> vk::UniqueDescriptorSetLayout
{
	auto ubo_layout_binding = vk::DescriptorSetLayoutBinding{};
	ubo_layout_binding.binding = 0;
	ubo_layout_binding.descriptorType = vk::DescriptorType::eUniformBuffer;
	ubo_layout_binding.descriptorCount = 1;
	ubo_layout_binding.stageFlags = vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment;
	ubo_layout_binding.pImmutableSamplers = nullptr;

	auto bindings = std::array<vk::DescriptorSetLayoutBinding, 1>{ubo_layout_binding};
	auto layout_info = vk::DescriptorSetLayoutCreateInfo{};
	layout_info.bindingCount = static_cast<uint32_t>(bindings.size());
	layout_info.pBindings = bindings.data();

	return device.createDescriptorSetLayoutUnique(layout_info);
}

auto vkpg::create_descriptor_set_layout_lights(vk::Device const& device) -> vk::UniqueDescriptorSetLayout
{
	auto ubo_layout_binding = vk::DescriptorSetLayoutBinding{};
	ubo_layout_binding.binding = 0;
	ubo_layout_binding.descriptorType = vk::DescriptorType::eUniformBuffer;
	ubo_layout_binding.descriptorCount = 1;
	ubo_layout_binding.stageFlags = vk::ShaderStageFlagBits::eFragment;
	ubo_layout_binding.pImmutableSamplers = nullptr;

	auto bindings = std::array<vk::DescriptorSetLayoutBinding, 1>{ubo_layout_binding};
	auto layout_info = vk::DescriptorSetLayoutCreateInfo{};
	layout_info.bindingCount = static_cast<uint32_t>(bindings.size());
	layout_info.pBindings = bindings.data();

	return device.createDescriptorSetLayoutUnique(layout_info);
}

// This function doesn't feel very useful anymore, maybe remove it?
auto vkpg::create_image(
	vk::Device const& device,
	const uint32_t width,
	const uint32_t height,
	const uint32_t mip_levels,
	const vk::SampleCountFlagBits num_samples,
	const vk::Format format,
	const vk::ImageTiling tiling,
	vk::ImageUsageFlags const& usage) -> vk::UniqueImage
{
	auto image_info = vk::ImageCreateInfo{};
	image_info.imageType = vk::ImageType::e2D;
	image_info.extent.width = static_cast<uint32_t>(width);
	image_info.extent.height = static_cast<uint32_t>(height);
	image_info.extent.depth = 1;
	image_info.mipLevels = mip_levels;
	image_info.arrayLayers = 1;
	image_info.format = format;
	image_info.tiling = tiling;
	image_info.initialLayout = vk::ImageLayout::eUndefined;
	image_info.usage = usage;
	image_info.sharingMode = vk::SharingMode::eExclusive;
	image_info.samples = num_samples;

	return device.createImageUnique(image_info);
}

auto vkpg::renderer::bind_image_memory(vk::Image const& image, vk::MemoryPropertyFlags const& properties) const
	-> vk::UniqueDeviceMemory
{
	const auto mem_requirements = device().getImageMemoryRequirements(image);

	auto alloc_info = vk::MemoryAllocateInfo{};
	alloc_info.allocationSize = mem_requirements.size;
	alloc_info.memoryTypeIndex = find_memory_type(mem_requirements.memoryTypeBits, properties, physical_device());

	auto image_memory = device().allocateMemoryUnique(alloc_info);

	device().bindImageMemory(image, *image_memory, 0);

	return image_memory;
}

auto vkpg::create_command_pool(
	vk::PhysicalDevice const& physical_device, vk::Device const& device, vk::SurfaceKHR const& surface)
	-> vk::UniqueCommandPool
{
	auto queue_family_indices = find_queue_families(physical_device, surface);

	auto pool_info = vk::CommandPoolCreateInfo{};
	pool_info.queueFamilyIndex = queue_family_indices.graphics_family.value();
	pool_info.flags = vk::CommandPoolCreateFlagBits::eResetCommandBuffer;

	return device.createCommandPoolUnique(pool_info);
}

auto vkpg::create_swapchain_framebuffers(
	std::vector<vk::UniqueImageView> const& swap_chain_image_views,
	vk::RenderPass const& render_pass,
	vk::Extent2D swap_chain_extent,
	vk::Device const& device) -> std::vector<vk::UniqueFramebuffer>
{
	auto frame_buffers = std::vector<vk::UniqueFramebuffer>{};
	for(const auto& swap_chain_image_view : swap_chain_image_views)
	{
		auto attachments = std::array<vk::ImageView, 1>{*swap_chain_image_view};

		auto framebuffer_info = vk::FramebufferCreateInfo{};
		framebuffer_info.renderPass = render_pass;
		framebuffer_info.attachmentCount = static_cast<uint32_t>(attachments.size());
		framebuffer_info.pAttachments = attachments.data();
		framebuffer_info.width = swap_chain_extent.width;
		framebuffer_info.height = swap_chain_extent.height;
		framebuffer_info.layers = 1;

		frame_buffers.push_back(device.createFramebufferUnique(framebuffer_info));
	}
	return frame_buffers;
}

auto vkpg::create_scene_framebuffer(
	vk::ImageView const& color_image,
	vk::ImageView const& depth_image,
	vk::ImageView const& color_resolve_image,
	vk::RenderPass const& render_pass,
	vk::Extent2D extent,
	vk::Device const& device) -> vk::UniqueFramebuffer
{
	auto attachments = std::array<vk::ImageView, 3>{color_image, depth_image, color_resolve_image};

	auto framebuffer_info = vk::FramebufferCreateInfo{};
	framebuffer_info.renderPass = render_pass;
	framebuffer_info.attachmentCount = static_cast<uint32_t>(attachments.size());
	framebuffer_info.pAttachments = attachments.data();
	framebuffer_info.width = extent.width;
	framebuffer_info.height = extent.height;
	framebuffer_info.layers = 1;

	return device.createFramebufferUnique(framebuffer_info);
}

auto vkpg::create_scene_render_pass(
	vk::Format swap_chain_image_format,
	vk::SampleCountFlagBits msaa_samples,
	vk::Device const& device,
	vk::PhysicalDevice const& physical_device) -> vk::UniqueRenderPass
{
	auto color_attachment = vk::AttachmentDescription{};
	color_attachment.format = swap_chain_image_format;
	color_attachment.samples = msaa_samples;
	color_attachment.loadOp = vk::AttachmentLoadOp::eClear;
	color_attachment.storeOp = vk::AttachmentStoreOp::eStore;
	color_attachment.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
	color_attachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
	color_attachment.initialLayout = vk::ImageLayout::eUndefined;
	color_attachment.finalLayout = vk::ImageLayout::eColorAttachmentOptimal;

	auto depth_attachment = vk::AttachmentDescription{};
	depth_attachment.format = find_depth_format(physical_device);
	depth_attachment.samples = msaa_samples;
	depth_attachment.loadOp = vk::AttachmentLoadOp::eClear;
	depth_attachment.storeOp = vk::AttachmentStoreOp::eDontCare;
	depth_attachment.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
	depth_attachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
	depth_attachment.initialLayout = vk::ImageLayout::eUndefined;
	depth_attachment.finalLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal;

	auto color_attachment_resolve = vk::AttachmentDescription{};
	color_attachment_resolve.format = swap_chain_image_format;
	color_attachment_resolve.samples = vk::SampleCountFlagBits::e1;
	color_attachment_resolve.loadOp = vk::AttachmentLoadOp::eDontCare;
	color_attachment_resolve.storeOp = vk::AttachmentStoreOp::eStore;
	color_attachment_resolve.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
	color_attachment_resolve.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
	color_attachment_resolve.initialLayout = vk::ImageLayout::eUndefined;
	color_attachment_resolve.finalLayout = vk::ImageLayout::eShaderReadOnlyOptimal;

	auto color_attachment_ref = vk::AttachmentReference{};
	color_attachment_ref.attachment = 0;
	color_attachment_ref.layout = vk::ImageLayout::eColorAttachmentOptimal;

	auto depth_attachment_ref = vk::AttachmentReference{};
	depth_attachment_ref.attachment = 1;
	depth_attachment_ref.layout = vk::ImageLayout::eDepthStencilAttachmentOptimal;

	auto color_attachment_resolve_ref = vk::AttachmentReference{};
	color_attachment_resolve_ref.attachment = 2;
	color_attachment_resolve_ref.layout = vk::ImageLayout::eColorAttachmentOptimal;

	auto subpass = vk::SubpassDescription{};
	subpass.pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &color_attachment_ref;
	subpass.pDepthStencilAttachment = &depth_attachment_ref;
	subpass.pResolveAttachments = &color_attachment_resolve_ref;

	auto dependency = vk::SubpassDependency{};
	dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependency.dstSubpass = 0;
	dependency.srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	dependency.srcAccessMask = vk::AccessFlags{};
	dependency.dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	dependency.dstAccessMask = vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite;

	auto attachments =
		std::array<vk::AttachmentDescription, 3>{color_attachment, depth_attachment, color_attachment_resolve};
	auto render_pass_info = vk::RenderPassCreateInfo{};
	render_pass_info.attachmentCount = static_cast<uint32_t>(attachments.size());
	render_pass_info.pAttachments = attachments.data();
	render_pass_info.subpassCount = 1;
	render_pass_info.pSubpasses = &subpass;
	render_pass_info.dependencyCount = 1;
	render_pass_info.pDependencies = &dependency;

	return device.createRenderPassUnique(render_pass_info);
}

auto vkpg::create_post_proc_render_pass(
	vk::Format swap_chain_image_format, vk::Device const& device, vk::PhysicalDevice const& physical_device)
	-> vk::UniqueRenderPass
{
	auto color_attachment = vk::AttachmentDescription{};
	color_attachment.format = swap_chain_image_format;
	color_attachment.samples = vk::SampleCountFlagBits::e1;
	color_attachment.loadOp = vk::AttachmentLoadOp::eDontCare;
	color_attachment.storeOp = vk::AttachmentStoreOp::eStore;
	color_attachment.initialLayout = vk::ImageLayout::eUndefined;
	color_attachment.finalLayout = vk::ImageLayout::ePresentSrcKHR;

	auto color_attachment_ref = vk::AttachmentReference{};
	color_attachment_ref.attachment = 0;
	color_attachment_ref.layout = vk::ImageLayout::eColorAttachmentOptimal;

	auto subpass = vk::SubpassDescription{};
	subpass.pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &color_attachment_ref;

	auto dependencies = std::array<vk::SubpassDependency, 2>{};

	dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
	dependencies[0].dstSubpass = 0;
	dependencies[0].srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	dependencies[0].dstStageMask = vk::PipelineStageFlagBits::eFragmentShader;
	dependencies[0].srcAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;
	dependencies[0].dstAccessMask = vk::AccessFlagBits::eShaderRead;

	dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
	dependencies[0].dstSubpass = 0;
	dependencies[0].srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	dependencies[0].srcAccessMask = vk::AccessFlags{};
	dependencies[0].dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	dependencies[0].dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;

	auto render_pass_info = vk::RenderPassCreateInfo{};
	render_pass_info.attachmentCount = 1;
	render_pass_info.pAttachments = &color_attachment;
	render_pass_info.subpassCount = 1;
	render_pass_info.pSubpasses = &subpass;
	render_pass_info.dependencyCount = 1;
	render_pass_info.pDependencies = dependencies.data();

	return device.createRenderPassUnique(render_pass_info);
}

auto vkpg::renderer::create_shader_module(std::vector<char> const& code) const -> vk::UniqueShaderModule
{
	auto create_info = vk::ShaderModuleCreateInfo{};
	create_info.codeSize = code.size();
	create_info.pCode = reinterpret_cast<const uint32_t*>(code.data());

	return device_->createShaderModuleUnique(create_info);
}

auto vkpg::create_image_views(
	std::vector<vk::Image> const& images, const vk::Format swap_chain_image_format, vk::Device const& device)
	-> std::vector<vk::UniqueImageView>
{
	auto image_views = std::vector<vk::UniqueImageView>{};
	for(auto const image : images)
	{
		image_views.push_back(
			create_image_view(image, swap_chain_image_format, vk::ImageAspectFlagBits::eColor, 1, device));
	}
	return image_views;
}

auto vkpg::create_swapchain(
	vk::PhysicalDevice const& physical_device,
	vk::SurfaceKHR const& surface,
	vk::Device const& device,
	GLFWwindow* window) -> vk::UniqueSwapchainKHR
{
	const auto swap_chain_support = query_swapchain_support(physical_device, surface);

	const auto surface_format = choose_swapchain_surface_format(swap_chain_support.formats);
	const auto present_mode = choose_swapchain_present_mode(swap_chain_support.present_modes);
	std::cout << "Present mode used: ";
	switch(present_mode)
	{
		case vk::PresentModeKHR::eImmediate:
			std::cout << "Immediate\n";
			break;
		case vk::PresentModeKHR::eMailbox:
			std::cout << "Mailbox\n";
			break;
		case vk::PresentModeKHR::eFifo:
			std::cout << "Fifo\n";
			break;
		case vk::PresentModeKHR::eFifoRelaxed:
			std::cout << "Fifo relaxed";
			break;
		case vk::PresentModeKHR::eSharedDemandRefresh:
			std::cout << "Shared demand refresh";
			break;
		case vk::PresentModeKHR::eSharedContinuousRefresh:
			std::cout << "Shared continuous refresh";
			break;
	}
	const auto extent = choose_swapchain_extent(swap_chain_support.capabilities, window);

	auto image_count = uint32_t{swap_chain_support.capabilities.minImageCount + 1};
	if(swap_chain_support.capabilities.maxImageCount > 0 && image_count > swap_chain_support.capabilities.maxImageCount)
	{
		image_count = swap_chain_support.capabilities.maxImageCount;
	}

	auto create_info = vk::SwapchainCreateInfoKHR{};
	create_info.surface = surface;
	create_info.minImageCount = image_count;
	create_info.imageFormat = surface_format.format;
	create_info.imageColorSpace = surface_format.colorSpace;
	create_info.imageExtent = extent;
	create_info.imageArrayLayers = 1;
	create_info.imageUsage = vk::ImageUsageFlagBits::eColorAttachment;

	auto indices = find_queue_families(physical_device, surface);
	auto queue_family_indices =
		std::array<uint32_t, 2>{indices.graphics_family.value(), indices.present_family.value()};

	if(indices.graphics_family != indices.present_family)
	{
		create_info.imageSharingMode = vk::SharingMode::eConcurrent;
		create_info.queueFamilyIndexCount = 2;
		create_info.pQueueFamilyIndices = queue_family_indices.data();
	}
	else
	{
		create_info.imageSharingMode = vk::SharingMode::eExclusive;
		create_info.queueFamilyIndexCount = 0;
		create_info.pQueueFamilyIndices = nullptr;
	}

	create_info.preTransform = swap_chain_support.capabilities.currentTransform;
	create_info.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
	create_info.presentMode = present_mode;
	create_info.clipped = true;
	create_info.oldSwapchain = nullptr;

	return device.createSwapchainKHRUnique(create_info);
}

auto vkpg::create_window_surface(GLFWwindow* window, vk::Instance const& instance) -> vk::UniqueSurfaceKHR
{
	auto surface = VkSurfaceKHR{};

	if(glfwCreateWindowSurface(instance, window, nullptr, &surface) != VK_SUCCESS)
	{
		throw std::runtime_error{"failed to create window surface!"};
	}

	return vk::UniqueSurfaceKHR{surface, instance};
}

auto vkpg::create_logical_device(vk::PhysicalDevice const& physical_device, vk::SurfaceKHR const& surface)
	-> vk::UniqueDevice
{
	auto indices = find_queue_families(physical_device, surface);

	auto queue_create_infos = std::vector<vk::DeviceQueueCreateInfo>{};
	const auto unique_queue_families =
		std::set<uint32_t>{indices.graphics_family.value(), indices.present_family.value()};

	const auto queue_priority = 1.0f;
	for(auto queue_family : unique_queue_families)
	{
		auto queue_create_info = vk::DeviceQueueCreateInfo{};
		queue_create_info.queueFamilyIndex = queue_family;
		queue_create_info.queueCount = 1;
		queue_create_info.pQueuePriorities = &queue_priority;
		queue_create_infos.push_back(queue_create_info);
	}

	auto device_features = vk::PhysicalDeviceFeatures{};
	device_features.samplerAnisotropy = true;
	device_features.sampleRateShading = false;

	auto create_info = vk::DeviceCreateInfo{};
	create_info.queueCreateInfoCount = static_cast<uint32_t>(queue_create_infos.size());
	create_info.pQueueCreateInfos = queue_create_infos.data();

	create_info.pEnabledFeatures = &device_features;

	create_info.enabledExtensionCount = static_cast<uint32_t>(device_extensions.size());
	create_info.ppEnabledExtensionNames = device_extensions.data();

	if constexpr(enable_validation_layers)
	{
		create_info.enabledLayerCount = static_cast<uint32_t>(validation_layers.size());
		create_info.ppEnabledLayerNames = validation_layers.data();
	}
	else
	{
		create_info.enabledLayerCount = 0;
	}

	return physical_device.createDeviceUnique(create_info);
}

auto vkpg::is_device_suitable(vk::PhysicalDevice const& device, vk::SurfaceKHR const& surface) -> bool
{
	auto indices = find_queue_families(device, surface);

	const auto extensions_supported = check_device_extension_support(device);

	auto swap_chain_adequate = false;
	if(extensions_supported)
	{
		auto swap_chain_support = query_swapchain_support(device, surface);
		swap_chain_adequate = !swap_chain_support.formats.empty() && !swap_chain_support.present_modes.empty();
	}

	auto supported_features = vk::PhysicalDeviceFeatures{};
	device.getFeatures(&supported_features);

	return indices.is_complete() && extensions_supported && swap_chain_adequate && supported_features.samplerAnisotropy;
}

auto vkpg::choose_swapchain_surface_format(std::vector<vk::SurfaceFormatKHR> const& available_formats)
	-> vk::SurfaceFormatKHR
{
	if(available_formats.size() == 1 && available_formats[0].format == vk::Format::eUndefined)
	{
		return {vk::Format::eB8G8R8A8Srgb, vk::ColorSpaceKHR::eSrgbNonlinear};
	}

	for(const auto& available_format : available_formats)
	{
		if(available_format.format == vk::Format::eB8G8R8A8Srgb &&
		   available_format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
		{
			return available_format;
		}
	}

	return available_formats[0];
}

auto vkpg::choose_swapchain_present_mode(std::vector<vk::PresentModeKHR> const& available_present_modes)
	-> vk::PresentModeKHR
{
	auto best_mode = vk::PresentModeKHR::eFifo;

	for(const auto& available_present_mode : available_present_modes)
	{
		if(available_present_mode == vk::PresentModeKHR::eMailbox)
		{
			return available_present_mode;
		}
		if(available_present_mode == vk::PresentModeKHR::eImmediate)
		{
			// best_mode = available_present_mode; // Prefer v-sync, ignore immediate mode.
		}
	}

	return best_mode;
}

auto vkpg::choose_swapchain_extent(vk::SurfaceCapabilitiesKHR const& capabilities, GLFWwindow* window) -> vk::Extent2D
{
	if(capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max())
	{
		return capabilities.currentExtent;
	}
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);
	auto actual_extent = vk::Extent2D{static_cast<uint32_t>(width), static_cast<uint32_t>(height)};

	actual_extent.width =
		std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actual_extent.width));
	actual_extent.height = std::max(
		capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actual_extent.height));

	return actual_extent;
}

auto vkpg::check_device_extension_support(vk::PhysicalDevice const& device) -> bool
{
	auto available_extensions = device.enumerateDeviceExtensionProperties();
	auto required_extensions = std::set<std::string>{device_extensions.begin(), device_extensions.end()};

	for(const auto& extension : available_extensions)
	{
		required_extensions.erase(extension.extensionName);
	}

	return required_extensions.empty();
}

auto vkpg::pick_physical_device(vk::Instance const& instance, vk::SurfaceKHR const& surface) -> vk::PhysicalDevice
{
	auto devices = instance.enumeratePhysicalDevices();

	for(auto& device : devices)
	{
		if(is_device_suitable(device, surface))
		{
			return device;
		}
	}

	throw std::runtime_error{"failed to find GPUs with Vulkan support!"};
}

auto vkpg::create_debug_callback(vk::Instance& instance, vk::DispatchLoaderDynamic& loader)
	-> vk::UniqueHandle<vk::DebugUtilsMessengerEXT, vk::DispatchLoaderDynamic>
{
	if constexpr(!enable_validation_layers)
	{
		return vk::UniqueHandle<vk::DebugUtilsMessengerEXT, vk::DispatchLoaderDynamic>{};
	}

	auto create_info = vk::DebugUtilsMessengerCreateInfoEXT{};
	create_info.messageSeverity = vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose |
		vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning | vk::DebugUtilsMessageSeverityFlagBitsEXT::eError;
	create_info.messageType = vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral |
		vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation | vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance;
	create_info.pfnUserCallback = debug_callback;
	create_info.pUserData = nullptr;

	return instance.createDebugUtilsMessengerEXTUnique(create_info, nullptr, loader);
}

auto vkpg::renderer::draw_frame(
	std::vector<render_component>& objects,
	vkpg::camera* cam,
	vkpg::skybox* skybox,
	vkpg::post_process_effect const& post_process,
	descriptor_lights const& lights_descriptor) -> bool
{
	// TODO: Handle this result?
	device_->waitForFences(*in_flight_fences_[current_frame_], true, std::numeric_limits<uint64_t>::max());

	const auto acquire_result = device_->acquireNextImageKHR(
		*swapchain_, std::numeric_limits<uint64_t>::max(), *image_available_semaphores_[current_frame_], nullptr);

	if(acquire_result.result == vk::Result::eErrorOutOfDateKHR)
	{
		recreate_swapchain();
		return true;
	}
	if(acquire_result.result != vk::Result::eSuccess && acquire_result.result != vk::Result::eSuboptimalKHR)
	{
		throw std::runtime_error{"failed to acquire swap chain image!"};
	}

	if(cam)
	{
		update_uniform_buffers(objects, *cam, skybox);
		std::sort(
			objects.begin(), objects.end(), [&cam](const render_component& first, const render_component& second) {
				if(second.get_material().get_blend_mode() != blend_mode::blend)
				{
					return false;
				}
				if(first.get_material().get_blend_mode() != blend_mode::blend)
				{
					return true;
				}
				return glm::length(first.get_transform().world_position() - cam->get_transform().world_position()) >
					glm::length(second.get_transform().world_position() - cam->get_transform().world_position());
			});
		record_commands(objects, acquire_result.value, skybox, post_process, cam, lights_descriptor);
	}
	else
	{
		record_commands({}, acquire_result.value, nullptr, post_process, cam, lights_descriptor);
	}

	auto submit_info = vk::SubmitInfo{};

	auto wait_stage = static_cast<vk::PipelineStageFlags>(vk::PipelineStageFlagBits::eColorAttachmentOutput);
	submit_info.waitSemaphoreCount = 1;
	submit_info.pWaitSemaphores = &*image_available_semaphores_[current_frame_];
	submit_info.pWaitDstStageMask = &wait_stage;
	submit_info.commandBufferCount = 1;
	submit_info.pCommandBuffers = &*command_buffers_[current_frame_];
	submit_info.signalSemaphoreCount = 1;
	submit_info.pSignalSemaphores = &*render_finished_semaphores_[current_frame_];

	device_->resetFences(*in_flight_fences_[current_frame_]);

	graphics_queue_.submit(submit_info, *in_flight_fences_[current_frame_]);

	auto present_info = vk::PresentInfoKHR{};

	present_info.waitSemaphoreCount = 1;
	present_info.pWaitSemaphores = &*render_finished_semaphores_[current_frame_];
	present_info.swapchainCount = 1;
	present_info.pSwapchains = &*swapchain_;
	present_info.pImageIndices = &acquire_result.value;
	present_info.pResults = nullptr;

	auto recreated_swapchain = false;

	try
	{
		const auto present_result = present_queue_.presentKHR(present_info);
		if(present_result == vk::Result::eErrorOutOfDateKHR || present_result == vk::Result::eSuboptimalKHR ||
		   window_resized_)
		{
			window_resized_ = false;
			recreate_swapchain();
			recreated_swapchain = true;
		}
		else if(present_result != vk::Result::eSuccess)
		{
			throw std::runtime_error{"failed to present swap chain image!"};
		}
	}
	catch(vk::OutOfDateKHRError& e)
	{
		window_resized_ = false;
		recreate_swapchain();
		recreated_swapchain = true;
	}

	current_frame_ = (current_frame_ + 1) % max_frames_in_flight();

	return recreated_swapchain;
}

auto vkpg::renderer::update_uniform_buffers(
	std::vector<render_component>& objects, vkpg::camera& cam, vkpg::skybox* skybox) const -> void
{
	cam.update_matrix_buffer(*this, current_frame_);
	for(auto& object : objects)
	{
		object.update_transform_buffer(*this, static_cast<uint32_t>(current_frame_));
	}
	if(skybox)
	{
		skybox->update_transform_buffer(*this, cam, static_cast<uint32_t>(current_frame_));
	}
}

auto vkpg::renderer::device() -> vk::Device&
{
	return *device_;
}

auto vkpg::renderer::device() const -> vk::Device const&
{
	return *device_;
}

auto vkpg::renderer::physical_device() -> vk::PhysicalDevice&
{
	return physical_device_;
}

auto vkpg::renderer::physical_device() const -> vk::PhysicalDevice const&
{
	return physical_device_;
}

auto vkpg::renderer::msaa_samples() const -> vk::SampleCountFlagBits
{
	return msaa_samples_;
}

auto vkpg::renderer::scene_render_pass() const -> vk::RenderPass const&
{
	return *scene_render_pass_;
}

auto vkpg::renderer::descriptor_set_layout_transform() const -> vk::DescriptorSetLayout const&
{
	return *descriptor_set_layout_transform_;
}

auto vkpg::renderer::descriptor_set_layout_camera() const -> vk::DescriptorSetLayout const&
{
	return *descriptor_set_layout_camera_;
}

auto vkpg::renderer::descriptor_set_layout_lights() const -> vk::DescriptorSetLayout const&
{
	return *descriptor_set_layout_lights_;
}
