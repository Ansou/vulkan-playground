#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

layout(binding = 0) uniform sampler2D texSampler;


vec3 edge_detection(float offset)
{
    vec2 offsets[9] = vec2[](
        vec2(-offset, offset), vec2(0.0f, offset), vec2(offset, offset),
        vec2(-offset, 0.0f), vec2(0.0f, 0.0f), vec2(offset, 0.0f),
        vec2(-offset, -offset), vec2(0.0f, -offset), vec2(offset, -offset)
    );

    float kernel[9] = float[](
        1, 1, 1,
        1, -8, 1,
        1, 1, 1
    );

    vec3 sampleTex[9];
    for(int i = 0; i < 9; ++i)
    {
        sampleTex[i] = vec3(texture(texSampler, fragTexCoord + offsets[i]));
    }
    vec3 col = vec3(0.0);
    for(int i = 0; i < 9; ++i)
    {
        col += sampleTex[i] * kernel[i];
    }

    return col;
}

vec3 fill(float offset)
{
    vec2 offsets[9] = vec2[](
        vec2(-offset, offset), vec2(0.0f, offset), vec2(offset, offset),
        vec2(-offset, 0.0f), vec2(0.0f, 0.0f), vec2(offset, 0.0f),
        vec2(-offset, -offset), vec2(0.0f, -offset), vec2(offset, -offset)
    );

    float kernel[9] = float[](
        1, 1, 1,
        1, -8, 1,
        1, 1, 1
    );

    vec3 sampleTex[9];
    for(int i = 0; i < 9; ++i)
    {
        sampleTex[i] = vec3(texture(texSampler, fragTexCoord + offsets[i]));
    }
    vec3 col = vec3(0.0);
    for(int i = 0; i < 9; ++i)
    {
        col += sampleTex[i] * kernel[i];
    }

    col = 1.0 - col;

    return col;
}

void main()
{
    vec3 edgeCol = edge_detection(1.0 / 300.0);
    vec3 fillCol = fill(1.0 / 50.0);

    vec3 finalCol = edgeCol * 0.5 + fillCol * 0.5;

    outColor = vec4(finalCol, 1.0);
}
