[requires]
glfw/3.3.2
nlohmann_json/3.9.1
cmake/3.18.2

[generators]
cmake_find_package
cmake_paths
