#include <camera.hpp>
#include <descriptors.hpp>
#include <light.hpp>
#include <render_component.hpp>
#include <renderer.hpp>

//
// descriptor_transform
//

vkpg::descriptor_transform::descriptor_transform(
	renderer const& renderer, vk::DescriptorSetLayout const& layout, frame_arr<vk::Buffer> transform_buffers) :
	descriptor_pool_{create_pool(renderer)},
	descriptor_sets_{renderer.create_descriptor_sets(layout, *descriptor_pool_)}
{
	update_descriptor_sets(renderer, transform_buffers);
}

auto vkpg::descriptor_transform::descriptor_set(uint32_t current_frame) const -> vk::DescriptorSet const&
{
	assert(current_frame >= 0);
	assert(current_frame < renderer::max_frames_in_flight());
	return *descriptor_sets_[current_frame];
}

auto vkpg::descriptor_transform::create_pool(renderer const& renderer) -> vk::UniqueDescriptorPool
{
	auto constexpr num_sets = renderer::max_frames_in_flight();

	auto pool_sizes = std::array<vk::DescriptorPoolSize, 1>{};
	pool_sizes[0].type = vk::DescriptorType::eUniformBuffer;
	pool_sizes[0].descriptorCount = num_sets;

	auto pool_info = vk::DescriptorPoolCreateInfo{};
	pool_info.poolSizeCount = static_cast<uint32_t>(pool_sizes.size());
	pool_info.pPoolSizes = pool_sizes.data();
	pool_info.maxSets = num_sets;
	pool_info.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;

	return renderer.device().createDescriptorPoolUnique(pool_info);
}

auto vkpg::descriptor_transform::update_descriptor_sets(
	renderer const& renderer, frame_arr<vk::Buffer> transform_buffers) -> void
{
	for(size_t i = 0; i < descriptor_sets_.size(); ++i)
	{
		auto buffer_info = vk::DescriptorBufferInfo{};
		buffer_info.buffer = *transform_buffers[i];
		buffer_info.offset = 0;
		buffer_info.range = sizeof(transform_ubo);

		// TODO: Write some helper function that can make this code less verbose.
		auto descriptor_writes = std::array<vk::WriteDescriptorSet, 1>{};

		descriptor_writes[0].dstSet = *descriptor_sets_[i];
		descriptor_writes[0].dstBinding = 0;
		descriptor_writes[0].dstArrayElement = 0;
		descriptor_writes[0].descriptorType = vk::DescriptorType::eUniformBuffer;
		descriptor_writes[0].descriptorCount = 1;
		descriptor_writes[0].pBufferInfo = &buffer_info;

		renderer.device().updateDescriptorSets(
			static_cast<uint32_t>(descriptor_writes.size()), descriptor_writes.data(), 0, nullptr);
	}
}

//
// descriptor_camera
//

vkpg::descriptor_camera::descriptor_camera(
	renderer const& renderer, vk::DescriptorSetLayout const& layout, frame_arr<vk::Buffer> camera_buffers) :
	descriptor_pool_{create_pool(renderer)},
	descriptor_sets_{renderer.create_descriptor_sets(layout, *descriptor_pool_)}
{
	update_descriptor_sets(renderer, camera_buffers);
}

auto vkpg::descriptor_camera::descriptor_set(uint32_t current_frame) const -> vk::DescriptorSet const&
{
	assert(current_frame >= 0);
	assert(current_frame < renderer::max_frames_in_flight());
	return *descriptor_sets_[current_frame];
}

auto vkpg::descriptor_camera::create_pool(renderer const& renderer) -> vk::UniqueDescriptorPool
{
	auto constexpr num_sets = renderer::max_frames_in_flight();

	auto pool_sizes = std::array<vk::DescriptorPoolSize, 1>{};
	pool_sizes[0].type = vk::DescriptorType::eUniformBuffer;
	pool_sizes[0].descriptorCount = num_sets;

	auto pool_info = vk::DescriptorPoolCreateInfo{};
	pool_info.poolSizeCount = static_cast<uint32_t>(pool_sizes.size());
	pool_info.pPoolSizes = pool_sizes.data();
	pool_info.maxSets = num_sets;
	pool_info.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;

	return renderer.device().createDescriptorPoolUnique(pool_info);
}

auto vkpg::descriptor_camera::update_descriptor_sets(renderer const& renderer, frame_arr<vk::Buffer> camera_buffers)
	-> void
{
	for(size_t i = 0; i < descriptor_sets_.size(); ++i)
	{
		auto buffer_info = vk::DescriptorBufferInfo{};
		buffer_info.buffer = *camera_buffers[i];
		buffer_info.offset = 0;
		buffer_info.range = sizeof(camera_ubo);

		// TODO: Write some helper function that can make this code less verbose.
		auto descriptor_writes = std::array<vk::WriteDescriptorSet, 1>{};

		descriptor_writes[0].dstSet = *descriptor_sets_[i];
		descriptor_writes[0].dstBinding = 0;
		descriptor_writes[0].dstArrayElement = 0;
		descriptor_writes[0].descriptorType = vk::DescriptorType::eUniformBuffer;
		descriptor_writes[0].descriptorCount = 1;
		descriptor_writes[0].pBufferInfo = &buffer_info;

		renderer.device().updateDescriptorSets(
			static_cast<uint32_t>(descriptor_writes.size()), descriptor_writes.data(), 0, nullptr);
	}
}

//
// descriptor_lights
//

vkpg::descriptor_lights::descriptor_lights(
	renderer const& renderer, vk::DescriptorSetLayout const& layout, frame_arr<vk::Buffer> lights_buffers) :
	descriptor_pool_{create_pool(renderer)},
	descriptor_sets_{renderer.create_descriptor_sets(layout, *descriptor_pool_)}
{
	update_descriptor_sets(renderer, lights_buffers);
}

auto vkpg::descriptor_lights::descriptor_set(uint32_t current_frame) const -> vk::DescriptorSet const&
{
	assert(current_frame >= 0);
	assert(current_frame < renderer::max_frames_in_flight());
	return *descriptor_sets_[current_frame];
}

auto vkpg::descriptor_lights::create_pool(renderer const& renderer) -> vk::UniqueDescriptorPool
{
	auto constexpr num_sets = renderer::max_frames_in_flight();

	auto pool_sizes = std::array<vk::DescriptorPoolSize, 1>{};
	pool_sizes[0].type = vk::DescriptorType::eUniformBuffer;
	pool_sizes[0].descriptorCount = num_sets;

	auto pool_info = vk::DescriptorPoolCreateInfo{};
	pool_info.poolSizeCount = static_cast<uint32_t>(pool_sizes.size());
	pool_info.pPoolSizes = pool_sizes.data();
	pool_info.maxSets = num_sets;
	pool_info.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;

	return renderer.device().createDescriptorPoolUnique(pool_info);
}

auto vkpg::descriptor_lights::update_descriptor_sets(renderer const& renderer, frame_arr<vk::Buffer> lights_buffers)
	-> void
{
	for(size_t i = 0; i < descriptor_sets_.size(); ++i)
	{
		auto buffer_info = vk::DescriptorBufferInfo{};
		buffer_info.buffer = *lights_buffers[i];
		buffer_info.offset = 0;
		buffer_info.range = sizeof(light_ubo);

		auto descriptor_writes = std::array<vk::WriteDescriptorSet, 1>{};

		descriptor_writes[0].dstSet = *descriptor_sets_[i];
		descriptor_writes[0].dstBinding = 0;
		descriptor_writes[0].dstArrayElement = 0;
		descriptor_writes[0].descriptorType = vk::DescriptorType::eUniformBuffer;
		descriptor_writes[0].descriptorCount = 1;
		descriptor_writes[0].pBufferInfo = &buffer_info;

		renderer.device().updateDescriptorSets(
			static_cast<uint32_t>(descriptor_writes.size()), descriptor_writes.data(), 0, nullptr);
	}
}

//
// descriptor_standard
//

vkpg::descriptor_standard::descriptor_standard(
	renderer const& renderer,
	vk::DescriptorSetLayout const& layout,
	frame_arr<vk::Buffer> const material_buffers,
	frame_arr<texture> const albedo_textures,
	frame_arr<texture> const specular_textures) :
	descriptor_pool_{create_pool(renderer)},
	descriptor_sets_{renderer.create_descriptor_sets(layout, *descriptor_pool_, renderer::max_frames_in_flight())}
{
	update_descriptor_sets(renderer, material_buffers, albedo_textures, specular_textures);
}

auto vkpg::get_frame_arr(std::vector<vk::UniqueBuffer> const& vec) -> frame_arr<vk::Buffer>
{
	auto arr = frame_arr<vk::Buffer>{};
	for(auto i = size_t{0}; i < arr.size(); ++i)
	{
		arr[i] = &*vec.at(i);
	}

	return arr;
}

auto vkpg::descriptor_standard::descriptor_set(uint32_t const current_frame) const -> vk::DescriptorSet const&
{
	assert(current_frame >= 0);
	assert(current_frame < renderer::max_frames_in_flight());
	return *descriptor_sets_[current_frame];
}

auto vkpg::descriptor_standard::create_pool(renderer const& renderer) -> vk::UniqueDescriptorPool
{
	auto const num_sets = renderer::max_frames_in_flight();

	auto pool_sizes = std::array<vk::DescriptorPoolSize, 3>{};
	pool_sizes[0].type = vk::DescriptorType::eUniformBuffer;
	pool_sizes[0].descriptorCount = num_sets;
	pool_sizes[1].type = vk::DescriptorType::eCombinedImageSampler;
	pool_sizes[1].descriptorCount = num_sets;
	pool_sizes[2].type = vk::DescriptorType::eCombinedImageSampler;
	pool_sizes[2].descriptorCount = num_sets;

	auto pool_info = vk::DescriptorPoolCreateInfo{};
	pool_info.poolSizeCount = static_cast<uint32_t>(pool_sizes.size());
	pool_info.pPoolSizes = pool_sizes.data();
	pool_info.maxSets = num_sets;
	pool_info.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;

	return renderer.device().createDescriptorPoolUnique(pool_info);
}

auto vkpg::descriptor_standard::update_descriptor_sets(
	renderer const& renderer,
	frame_arr<vk::Buffer> const material_buffers,
	frame_arr<texture> const albedo_textures,
	frame_arr<texture> const specular_textures) -> void
{
	for(size_t i = 0; i < descriptor_sets_.size(); ++i)
	{
		auto material_buffer_info = vk::DescriptorBufferInfo{};
		material_buffer_info.buffer = *material_buffers[i];
		material_buffer_info.offset = 0;
		material_buffer_info.range = sizeof(ubo_material_standard);

		auto image_info = vk::DescriptorImageInfo{};
		image_info.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
		image_info.imageView = albedo_textures[i]->image_view();
		image_info.sampler = albedo_textures[i]->sampler();

		auto specular_info = vk::DescriptorImageInfo{};
		specular_info.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
		specular_info.imageView = specular_textures[i]->image_view();
		specular_info.sampler = specular_textures[i]->sampler();

		// TODO: Write some helper function that can make this code less verbose.
		auto descriptor_writes = std::array<vk::WriteDescriptorSet, 3>{};

		descriptor_writes[0].dstSet = *descriptor_sets_[i];
		descriptor_writes[0].dstBinding = 0;
		descriptor_writes[0].dstArrayElement = 0;
		descriptor_writes[0].descriptorType = vk::DescriptorType::eUniformBuffer;
		descriptor_writes[0].descriptorCount = 1;
		descriptor_writes[0].pBufferInfo = &material_buffer_info;

		descriptor_writes[1].dstSet = *descriptor_sets_[i];
		descriptor_writes[1].dstBinding = 1;
		descriptor_writes[1].dstArrayElement = 0;
		descriptor_writes[1].descriptorType = vk::DescriptorType::eCombinedImageSampler;
		descriptor_writes[1].descriptorCount = 1;
		descriptor_writes[1].pImageInfo = &image_info;

		descriptor_writes[2].dstSet = *descriptor_sets_[i];
		descriptor_writes[2].dstBinding = 2;
		descriptor_writes[2].dstArrayElement = 0;
		descriptor_writes[2].descriptorType = vk::DescriptorType::eCombinedImageSampler;
		descriptor_writes[2].descriptorCount = 1;
		descriptor_writes[2].pImageInfo = &specular_info;

		renderer.device().updateDescriptorSets(
			static_cast<uint32_t>(descriptor_writes.size()), descriptor_writes.data(), 0, nullptr);
	}
}

//
// descriptor_skybox
//

vkpg::descriptor_skybox::descriptor_skybox(
	renderer const& renderer,
	vk::DescriptorSetLayout const& layout,
	frame_arr<vk::Buffer> transform_buffers,
	frame_arr<cubemap> textures) :
	descriptor_pool_{create_pool(renderer)},
	descriptor_sets_{renderer.create_descriptor_sets(layout, *descriptor_pool_, renderer::max_frames_in_flight())}
{
	update_descriptor_sets(renderer, transform_buffers, textures);
}

auto vkpg::descriptor_skybox::descriptor_set(uint32_t const current_frame) const -> vk::DescriptorSet const&
{
	assert(current_frame >= 0);
	assert(current_frame < renderer::max_frames_in_flight());
	return *descriptor_sets_[current_frame];
}

auto vkpg::descriptor_skybox::create_pool(renderer const& renderer) -> vk::UniqueDescriptorPool
{
	auto pool_sizes = std::array<vk::DescriptorPoolSize, 2>{};
	pool_sizes[0].type = vk::DescriptorType::eUniformBuffer;
	pool_sizes[0].descriptorCount = renderer::max_frames_in_flight();
	pool_sizes[1].type = vk::DescriptorType::eCombinedImageSampler;
	pool_sizes[1].descriptorCount = renderer::max_frames_in_flight();

	auto pool_info = vk::DescriptorPoolCreateInfo{};
	pool_info.poolSizeCount = static_cast<uint32_t>(pool_sizes.size());
	pool_info.pPoolSizes = pool_sizes.data();
	pool_info.maxSets = renderer::max_frames_in_flight();
	pool_info.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;

	return renderer.device().createDescriptorPoolUnique(pool_info);
}

auto vkpg::descriptor_skybox::update_descriptor_sets(
	renderer const& renderer, frame_arr<vk::Buffer> transform_buffers, frame_arr<cubemap> textures) -> void
{
	for(size_t i = 0; i < descriptor_sets_.size(); ++i)
	{
		auto buffer_info = vk::DescriptorBufferInfo{};
		buffer_info.buffer = *transform_buffers[i];
		buffer_info.offset = 0;
		buffer_info.range = sizeof(mvp_ubo);

		auto image_info = vk::DescriptorImageInfo{};
		image_info.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
		image_info.imageView = textures[i]->image_view();
		image_info.sampler = textures[i]->sampler();

		auto descriptor_writes = std::array<vk::WriteDescriptorSet, 2>{};

		descriptor_writes[0].dstSet = *descriptor_sets_[i];
		descriptor_writes[0].dstBinding = 0;
		descriptor_writes[0].dstArrayElement = 0;
		descriptor_writes[0].descriptorType = vk::DescriptorType::eUniformBuffer;
		descriptor_writes[0].descriptorCount = 1;
		descriptor_writes[0].pBufferInfo = &buffer_info;

		descriptor_writes[1].dstSet = *descriptor_sets_[i];
		descriptor_writes[1].dstBinding = 1;
		descriptor_writes[1].dstArrayElement = 0;
		descriptor_writes[1].descriptorType = vk::DescriptorType::eCombinedImageSampler;
		descriptor_writes[1].descriptorCount = 1;
		descriptor_writes[1].pImageInfo = &image_info;

		renderer.device().updateDescriptorSets(
			static_cast<uint32_t>(descriptor_writes.size()), descriptor_writes.data(), 0, nullptr);
	}
}

//
// descriptor_post_process
//

vkpg::descriptor_post_process::descriptor_post_process(
	renderer const& renderer,
	vk::DescriptorSetLayout const& layout,
	vk::ImageView const& image_view,
	vk::Sampler const& sampler) :
	descriptor_pool_{create_pool(renderer)}, descriptor_set_{renderer.create_descriptor_set(layout, *descriptor_pool_)}
{
	update_descriptor_set(renderer, image_view, sampler);
}

auto vkpg::descriptor_post_process::descriptor_set() const -> vk::DescriptorSet const&
{
	return *descriptor_set_;
}

auto vkpg::descriptor_post_process::create_pool(renderer const& renderer) -> vk::UniqueDescriptorPool
{
	auto pool_sizes = std::array<vk::DescriptorPoolSize, 1>{};
	pool_sizes[0].type = vk::DescriptorType::eCombinedImageSampler;
	pool_sizes[0].descriptorCount = 1;

	auto pool_info = vk::DescriptorPoolCreateInfo{};
	pool_info.poolSizeCount = static_cast<uint32_t>(pool_sizes.size());
	pool_info.pPoolSizes = pool_sizes.data();
	pool_info.maxSets = 1;
	pool_info.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;

	return renderer.device().createDescriptorPoolUnique(pool_info);
}

auto vkpg::descriptor_post_process::update_descriptor_set(
	renderer const& renderer, vk::ImageView const& image_view, vk::Sampler const& sampler) -> void
{
	auto image_info = vk::DescriptorImageInfo{};
	image_info.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
	image_info.imageView = image_view;
	image_info.sampler = sampler;

	auto descriptor_writes = std::array<vk::WriteDescriptorSet, 1>{};
	descriptor_writes[0].dstSet = *descriptor_set_;
	descriptor_writes[0].dstBinding = 0;
	descriptor_writes[0].dstArrayElement = 0;
	descriptor_writes[0].descriptorType = vk::DescriptorType::eCombinedImageSampler;
	descriptor_writes[0].descriptorCount = 1;
	descriptor_writes[0].pImageInfo = &image_info;

	renderer.device().updateDescriptorSets(
		static_cast<uint32_t>(descriptor_writes.size()), descriptor_writes.data(), 0, nullptr);
}
