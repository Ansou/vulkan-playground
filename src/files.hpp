#pragma once

#include <vector>
#include <filesystem>

namespace vkpg
{
	auto read_file(const std::filesystem::path& file_path) -> std::vector<char>;
}
