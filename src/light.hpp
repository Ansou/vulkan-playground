#pragma once

#include <glm/glm.hpp>

namespace vkpg
{
	class transform;

	struct directional_light_ubo
	{
		glm::vec3 direction;
		float padding_1;
		glm::vec3 ambient;
		float padding_2;
		glm::vec3 diffuse;
		float padding_3;
		glm::vec3 specular;
		float padding_4;
	};

	struct point_light_ubo
	{
		glm::vec3 position;
		float padding_1;
		float constant;
		float linear;
		float quadratic;
		float padding_2;
		glm::vec3 ambient;
		float padding_3;
		glm::vec3 diffuse;
		float padding_4;
		glm::vec3 specular;
		float padding_5;
	};

	struct spot_light_ubo
	{
		glm::vec3 position;
		float padding_1;
		glm::vec3 direction;
		float padding_2;
		float cutoff;
		float outer_cutoff;
		float constant;
		float linear;
		float quadratic;
		float padding_3[3];
		glm::vec3 ambient;
		float padding_4;
		glm::vec3 diffuse;
		float padding_5;
		glm::vec3 specular;
		float padding_6;
	};

	constexpr int num_directional_lights = 1;
	constexpr int num_point_lights = 4;
	constexpr int num_spot_lights = 1;

	struct light_ubo
	{
		int32_t current_num_directional_lights;
		int32_t current_num_point_lights;
		int32_t current_num_spot_lights;
		int32_t padding_1;
		directional_light_ubo directional[num_directional_lights];
		point_light_ubo point_lights[num_point_lights];
		spot_light_ubo spot_lights[num_spot_lights];
	};

	class directional_light
	{
	public:
		directional_light(
			transform const& trans,
			glm::vec3 ambient,
			glm::vec3 diffuse,
			glm::vec3 specular);

		transform const* transform;

		glm::vec3 ambient;
		glm::vec3 diffuse;
		glm::vec3 specular;
	};

	class point_light
	{
	public:
		point_light(
			transform const& trans,
			glm::vec3 ambient,
			glm::vec3 diffuse,
			glm::vec3 specular,
			float constant,
			float linear,
			float quadratic);

		transform const* transform;

		glm::vec3 ambient;
		glm::vec3 diffuse;
		glm::vec3 specular;

		float constant;
		float linear;
		float quadratic;
	};

	class spot_light
	{
	public:
		spot_light(
			transform const& trans,
			glm::vec3 ambient,
			glm::vec3 diffuse,
			glm::vec3 specular,
			float constant,
			float linear,
			float quadratic,
			float cutoff,
			float outer_cutoff);

		transform const* transform;

		glm::vec3 ambient;
		glm::vec3 diffuse;
		glm::vec3 specular;

		float constant;
		float linear;
		float quadratic;

		float cutoff;
		float outer_cutoff;
	};

	auto get_light_ubo(directional_light light) -> directional_light_ubo;
	auto get_light_ubo(point_light light) -> point_light_ubo;
	auto get_light_ubo(spot_light light) -> spot_light_ubo;
}
