#pragma once

#include <cmath>

#include <glm/glm.hpp>

namespace vkpg
{
	template<typename T>
	auto normalize_angle(T rad) -> T;

	template<typename T>
	auto normalize_angle_half(T rad) -> T;
}

template<typename T>
auto vkpg::normalize_angle(T rad) -> T
{
	const auto tau = glm::pi<T>() * T{2};
	auto normalized = std::fmod(rad, tau);

	if(normalized < T{})
	{
		normalized += tau;
	}
	else if(normalized >= tau)
	{
		normalized -= tau;
	}

	return normalized;
}

template<typename T>
auto vkpg::normalize_angle_half(T rad) -> T
{
	const auto tau = glm::pi<T>() * T{2};
	auto normalized = std::fmod(rad, tau);

	if(normalized < -glm::pi<T>())
	{
		normalized += tau;
	}
	else if(normalized >= glm::pi<T>())
	{
		normalized -= tau;
	}

	return normalized;
}
