#include <glm/glm.hpp>
#include <input.hpp>
#include <game.hpp>

vkpg::input_handler::input_handler(GLFWwindow* window) :
	window_{window}
{
	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window_, cursor_position_callback);
}

auto vkpg::input_handler::reset() -> void
{
	keys_pressed_.clear();
	mouse_moved_ = glm::vec<2, double>{0.0, 0.0};
}

auto vkpg::input_handler::update_key(const int key, const int action) -> void
{
	if(action == GLFW_PRESS && keys_pressed_.find(key) == keys_pressed_.end())
	{
		keys_pressed_.insert(key);
	}
}

auto vkpg::input_handler::update_mouse() -> void
{
	double xpos, ypos;
	glfwGetCursorPos(window_, &xpos, &ypos);
	update_mouse(xpos, ypos);
}

auto vkpg::input_handler::update_mouse(double xpos, double ypos) -> void
{
	auto const pos = glm::vec<2, double>{xpos, ypos};
	auto const delta = pos - prev_mouse_;
	prev_mouse_ = pos;
	mouse_moved_ = delta;
}

auto vkpg::input_handler::key_down(const int key) const -> bool
{
	return glfwGetKey(window_, key) == GLFW_PRESS;
}

auto vkpg::input_handler::key_pressed(const int key) const -> bool
{
	return keys_pressed_.find(key) != keys_pressed_.end();
}

auto vkpg::input_handler::mouse_moved() const -> glm::vec<2, double>
{
	constexpr auto sensitivity = 0.003;
	return mouse_moved_ * sensitivity;
}

auto vkpg::input_handler::mouse_pos() const -> glm::vec<2, double>
{
	auto pos = glm::vec<2, double>{};
	glfwGetCursorPos(window_, &pos.x, &pos.y);
	return pos;
}

auto vkpg::input_handler::left_stick() const -> glm::vec2
{
	GLFWgamepadstate state;
	if(glfwJoystickIsGamepad(GLFW_JOYSTICK_1) && glfwGetGamepadState(GLFW_JOYSTICK_1, &state))
	{
		const auto vector_deadzone = 0.1f;

		auto v = glm::vec2{state.axes[GLFW_GAMEPAD_AXIS_LEFT_X], state.axes[GLFW_GAMEPAD_AXIS_LEFT_Y]};
		if(glm::length(v) >= vector_deadzone)
		{
			if(glm::length(v) > 1.0f)
			{
				v = glm::normalize(v);
			}
			v = glm::normalize(v) * (glm::length(v) * ((glm::length(v) - vector_deadzone) / (1.0f - vector_deadzone)));
		}
		else
		{
			v = {0.0f, 0.0f};
		}
		return v;
	}

	return glm::vec2{0.0, 0.0};
}

auto vkpg::input_handler::right_stick() const -> glm::vec2
{
	GLFWgamepadstate state;
	if(glfwJoystickIsGamepad(GLFW_JOYSTICK_1) && glfwGetGamepadState(GLFW_JOYSTICK_1, &state))
	{
		const auto vector_deadzone = 0.1f;

		auto v = glm::vec2{state.axes[GLFW_GAMEPAD_AXIS_RIGHT_X], state.axes[GLFW_GAMEPAD_AXIS_RIGHT_Y]};
		if(glm::length(v) >= vector_deadzone)
		{
			if(glm::length(v) > 1.0f)
			{
				v = glm::normalize(v);
			}
			v = glm::normalize(v) * (glm::length(v) * ((glm::length(v) - vector_deadzone) / (1.0f - vector_deadzone)));
		}
		else
		{
			v = {0.0f, 0.0f};
		}
		return v;
	}

	return glm::vec2{0.0, 0.0};
}

auto vkpg::input_handler::trigger() const -> float
{
	GLFWgamepadstate state;
	if(glfwJoystickIsGamepad(GLFW_JOYSTICK_1) && glfwGetGamepadState(GLFW_JOYSTICK_1, &state))
	{
		return state.axes[GLFW_GAMEPAD_AXIS_RIGHT_TRIGGER] - state.axes[GLFW_GAMEPAD_AXIS_LEFT_TRIGGER];
	}

	return 0.0f;
}

auto vkpg::input_handler::key_callback(
	GLFWwindow* window,
	const int key,
	int scancode,
	const int action,
	int mods) -> void
{
	const auto app = reinterpret_cast<game*>(glfwGetWindowUserPointer(window));
	app->get_input_handler().update_key(key, action);
}

auto vkpg::input_handler::cursor_position_callback(GLFWwindow* window, const double xpos, const double ypos) -> void
{
	/*const auto app = reinterpret_cast<game*>(glfwGetWindowUserPointer(window));
	app->get_input_handler().update_mouse(xpos, ypos);*/
}
