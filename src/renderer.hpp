#pragma once

#include <GLFW/glfw3.h>
#include <filesystem>
#include <optional>
#include <vector>
#include <vulkan/vulkan.hpp>

namespace vkpg
{
	class post_process_effect;
	class camera;
	class render_component;
	class skybox;
	class renderer;
	class descriptor_lights;

	struct queue_family_indices
	{
		std::optional<uint32_t> graphics_family{};
		std::optional<uint32_t> present_family{};

		[[nodiscard]] auto is_complete() const -> bool;
	};

	struct swap_chain_support_details
	{
		vk::SurfaceCapabilitiesKHR capabilities{};
		std::vector<vk::SurfaceFormatKHR> formats{};
		std::vector<vk::PresentModeKHR> present_modes{};
	};

	// Extensions
	auto check_available_extensions() -> std::vector<vk::ExtensionProperties>;
	auto get_required_extensions() -> std::vector<const char*>;
	auto check_required_extensions_present(
		std::vector<vk::ExtensionProperties> const& available_extensions,
		std::vector<const char*> const& required_extensions) -> void;

	// Validation layers
	auto check_validation_layer_support() -> bool;
	auto create_debug_callback(vk::Instance& instance, vk::DispatchLoaderDynamic& loader)
		-> vk::UniqueHandle<vk::DebugUtilsMessengerEXT, vk::DispatchLoaderDynamic>;
	VKAPI_ATTR VkBool32 VKAPI_CALL debug_callback(
		VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
		VkDebugUtilsMessageTypeFlagsEXT message_type,
		const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
		void* user_data);

	// Device queries
	auto is_device_suitable(vk::PhysicalDevice const& device, vk::SurfaceKHR const& surface) -> bool;
	auto find_queue_families(vk::PhysicalDevice const& device, vk::SurfaceKHR const& surface) -> queue_family_indices;
	auto check_device_extension_support(vk::PhysicalDevice const& device) -> bool;
	auto query_swapchain_support(vk::PhysicalDevice const& device, vk::SurfaceKHR const& surface)
		-> swap_chain_support_details;
	auto get_max_usable_sample_count(vk::PhysicalDevice const& physical_device) -> vk::SampleCountFlagBits;
	auto find_depth_format(vk::PhysicalDevice const& physical_device) -> vk::Format;
	auto has_stencil_component(vk::Format format) -> bool;
	auto find_supported_format(
		std::vector<vk::Format> const& candidates,
		vk::ImageTiling tiling,
		vk::FormatFeatureFlags const& features,
		vk::PhysicalDevice const& physical_device) -> vk::Format;
	auto find_memory_type(
		uint32_t type_filter, vk::MemoryPropertyFlags const& properties, vk::PhysicalDevice const& physical_device)
		-> uint32_t;

	// Initialization
	auto create_instance() -> vk::UniqueInstance;
	auto create_window_surface(GLFWwindow* window, vk::Instance const& instance) -> vk::UniqueSurfaceKHR;
	auto pick_physical_device(vk::Instance const& instance, vk::SurfaceKHR const& surface) -> vk::PhysicalDevice;
	auto create_logical_device(vk::PhysicalDevice const& physical_device, vk::SurfaceKHR const& surface)
		-> vk::UniqueDevice;

	// Swapchain
	auto choose_swapchain_surface_format(std::vector<vk::SurfaceFormatKHR> const& available_formats)
		-> vk::SurfaceFormatKHR;
	auto choose_swapchain_present_mode(std::vector<vk::PresentModeKHR> const& available_present_modes)
		-> vk::PresentModeKHR;
	auto choose_swapchain_extent(vk::SurfaceCapabilitiesKHR const& capabilities, GLFWwindow* window) -> vk::Extent2D;
	auto create_swapchain(
		vk::PhysicalDevice const& physical_device,
		vk::SurfaceKHR const& surface,
		vk::Device const& device,
		GLFWwindow* window) -> vk::UniqueSwapchainKHR;

	// Resource creation
	auto create_image(
		vk::Device const& device,
		uint32_t width,
		uint32_t height,
		uint32_t mip_levels,
		vk::SampleCountFlagBits num_samples,
		vk::Format format,
		vk::ImageTiling tiling,
		vk::ImageUsageFlags const& usage) -> vk::UniqueImage;
	auto create_buffer(vk::DeviceSize size, vk::BufferUsageFlags const& usage, vk::Device const& device)
		-> vk::UniqueBuffer;
	auto create_image_view(
		vk::Image const& image,
		vk::Format format,
		vk::ImageAspectFlags const& aspect_flags,
		uint32_t mip_levels,
		vk::Device const& device) -> vk::UniqueImageView;
	auto create_image_views(
		std::vector<vk::Image> const& images, vk::Format swap_chain_image_format, vk::Device const& device)
		-> std::vector<vk::UniqueImageView>;
	template<typename BufferType>
	auto create_uniform_buffer(vk::Device const& device) -> vk::UniqueBuffer;
	template<typename BufferType>
	auto create_uniform_buffers(int num_buffers, vk::Device const& device) -> std::vector<vk::UniqueBuffer>;
	auto bind_uniform_buffers_memory(std::vector<vk::UniqueBuffer> const& buffers, renderer const& renderer)
		-> std::vector<vk::UniqueDeviceMemory>;

	// Render resources
	auto create_scene_render_pass(
		vk::Format swap_chain_image_format,
		vk::SampleCountFlagBits msaa_samples,
		vk::Device const& device,
		vk::PhysicalDevice const& physical_device) -> vk::UniqueRenderPass;
	auto create_post_proc_render_pass(
		vk::Format swap_chain_image_format, vk::Device const& device, vk::PhysicalDevice const& physical_device)
		-> vk::UniqueRenderPass;
	auto create_scene_framebuffer(
		vk::ImageView const& color_image,
		vk::ImageView const& depth_image,
		vk::ImageView const& color_resolve_image,
		vk::RenderPass const& render_pass,
		vk::Extent2D extent,
		vk::Device const& device) -> vk::UniqueFramebuffer;
	auto create_swapchain_framebuffers(
		std::vector<vk::UniqueImageView> const& swap_chain_image_views,
		vk::RenderPass const& render_pass,
		vk::Extent2D swap_chain_extent,
		vk::Device const& device) -> std::vector<vk::UniqueFramebuffer>;
	auto create_command_pool(
		vk::PhysicalDevice const& physical_device, vk::Device const& device, vk::SurfaceKHR const& surface)
		-> vk::UniqueCommandPool;
	auto create_command_buffers(vk::CommandPool const& command_pool, vk::Device const& device, uint32_t num_buffers)
		-> std::vector<vk::UniqueCommandBuffer>;

	// Descriptor set layout creation
	auto create_descriptor_set_layout_transform(vk::Device const& device) -> vk::UniqueDescriptorSetLayout;
	auto create_descriptor_set_layout_camera(vk::Device const& device) -> vk::UniqueDescriptorSetLayout;
	auto create_descriptor_set_layout_lights(vk::Device const& device) -> vk::UniqueDescriptorSetLayout;

	// Synchronization
	auto create_image_available_semaphores(vk::Device const& device, int max_frames_in_flight)
		-> std::vector<vk::UniqueSemaphore>;
	auto create_render_finished_semaphores(vk::Device const& device, int max_frames_in_flight)
		-> std::vector<vk::UniqueSemaphore>;
	auto create_in_flight_fences(vk::Device const& device, int max_frames_in_flight) -> std::vector<vk::UniqueFence>;

	// Post processing
	// TODO: Move these.
	auto
		create_post_proc_pipeline_layout(vk::Device const& device, vk::DescriptorSetLayout const& descriptor_set_layout)
			-> vk::UniquePipelineLayout;
	auto create_post_proc_descriptor_set_layout(vk::Device const& device) -> vk::UniqueDescriptorSetLayout;
	auto create_post_proc_sampler(vk::Device const& device) -> vk::UniqueSampler;
	auto create_post_proc_descriptor_pool(vk::Device const& device) -> vk::UniqueDescriptorPool;
	auto create_post_proc_descriptor_set(
		vk::DescriptorSetLayout const& descriptor_set_layout,
		vk::DescriptorPool const& descriptor_pool,
		vk::Device const& device) -> vk::UniqueDescriptorSet;
	auto create_post_proc_pipeline(
		vk::ShaderModule const& vert_shader,
		vk::ShaderModule const& frag_shader,
		vk::Device const& device,
		vk::Extent2D swap_chain_extent,
		vk::DescriptorSetLayout const& descriptor_set_layout,
		vk::PipelineLayout const& pipeline_layout,
		vk::RenderPass const& render_pass) -> vk::UniquePipeline;


	class renderer
	{
	public:
		explicit renderer(GLFWwindow* window);

		auto draw_frame(
			std::vector<render_component>& objects,
			camera* cam,
			vkpg::skybox* skybox,
			post_process_effect const& post_process,
			descriptor_lights const& lights_descriptor) -> bool;
		auto wait_for_gpu() -> void;

		[[nodiscard]] auto load_shader(std::filesystem::path const& shader_path) const -> vk::UniqueShaderModule;
		auto transition_image_layout(
			vk::Image const& image,
			vk::Format format,
			vk::ImageLayout old_layout,
			vk::ImageLayout new_layout,
			vk::ImageSubresourceRange const& subresource_range) const -> void;
		auto transition_image_layout(
			vk::Image const& image,
			vk::Format format,
			vk::ImageLayout old_layout,
			vk::ImageLayout new_layout,
			uint32_t mip_levels) const -> void;
		auto copy_buffer(vk::Buffer const& src_buffer, vk::Buffer const& dst_buffer, vk::DeviceSize size) const -> void;
		auto copy_buffer_to_image(
			vk::Buffer const& buffer, vk::Image const& image, uint32_t width, uint32_t height) const -> void;
		auto copy_buffer_to_image(
			vk::Buffer const& buffer, vk::Image const& image, std::vector<vk::BufferImageCopy> const& copies) const
			-> void;
		auto generate_mipmaps(
			vk::Image const& image,
			vk::Format const& image_format,
			int32_t tex_width,
			int32_t tex_height,
			uint32_t mip_levels) const -> void;
		[[nodiscard]] auto bind_image_memory(vk::Image const& image, vk::MemoryPropertyFlags const& properties) const
			-> vk::UniqueDeviceMemory;
		[[nodiscard]] auto bind_buffer_memory(vk::Buffer const& buffer, vk::MemoryPropertyFlags const& properties) const
			-> vk::UniqueDeviceMemory;
		[[nodiscard]] auto create_descriptor_set(
			vk::DescriptorSetLayout const& descriptor_set_layout, vk::DescriptorPool const& descriptor_pool) const
			-> vk::UniqueDescriptorSet;
		[[nodiscard]] auto create_descriptor_sets(
			vk::DescriptorSetLayout const& descriptor_set_layout,
			vk::DescriptorPool const& descriptor_pool,
			int num_sets = max_frames_in_flight()) const -> std::vector<vk::UniqueDescriptorSet>;

		template<typename UniformBuffer>
		auto update_ubo(vk::DeviceMemory const& buffer_memory, UniformBuffer const& ubo) const;

		[[nodiscard]] auto current_resolution() const -> vk::Extent2D;
		[[nodiscard]] auto device() -> vk::Device&;
		[[nodiscard]] auto device() const -> vk::Device const&;
		[[nodiscard]] auto physical_device() -> vk::PhysicalDevice&;
		[[nodiscard]] auto physical_device() const -> vk::PhysicalDevice const&;
		[[nodiscard]] auto msaa_samples() const -> vk::SampleCountFlagBits;
		[[nodiscard]] auto scene_render_pass() const -> vk::RenderPass const&;

		[[nodiscard]] auto descriptor_set_layout_transform() const -> vk::DescriptorSetLayout const&;
		[[nodiscard]] auto descriptor_set_layout_camera() const -> vk::DescriptorSetLayout const&;
		[[nodiscard]] auto descriptor_set_layout_lights() const -> vk::DescriptorSetLayout const&;

		static constexpr auto max_frames_in_flight() -> int;

	private:
		auto recreate_swapchain() -> void;

		[[nodiscard]] auto create_shader_module(std::vector<char> const& code) const -> vk::UniqueShaderModule;

		auto update_uniform_buffers(std::vector<render_component>& objects, camera& cam, vkpg::skybox* skybox) const
			-> void;
		auto record_commands(
			std::vector<render_component> const& objects,
			uint32_t current_image,
			skybox const* skybox,
			post_process_effect const& post_process,
			camera const* camera,
			descriptor_lights const& lights_descriptor) -> void;
		[[nodiscard]] auto begin_single_time_commands() const -> vk::UniqueCommandBuffer;
		auto end_single_time_commands(vk::CommandBuffer const& command_buffer) const -> void;

		GLFWwindow* window_;
		vk::UniqueInstance instance_{create_instance()};
		vk::DispatchLoaderDynamic loader_{*instance_, vkGetInstanceProcAddr};
		vk::UniqueHandle<vk::DebugUtilsMessengerEXT, vk::DispatchLoaderDynamic> callback_{
			create_debug_callback(*instance_, loader_)};
		vk::UniqueSurfaceKHR surface_{create_window_surface(window_, *instance_)};
		bool window_resized_{};
		vk::PhysicalDevice physical_device_{pick_physical_device(*instance_, *surface_)};
		vk::UniqueDevice device_{create_logical_device(physical_device_, *surface_)};

		vk::Queue graphics_queue_{
			device_->getQueue(find_queue_families(physical_device_, *surface_).graphics_family.value(), 0)};
		vk::Queue present_queue_{
			device_->getQueue(find_queue_families(physical_device_, *surface_).present_family.value(), 0)};
		vk::UniqueCommandPool command_pool_{create_command_pool(physical_device_, *device_, *surface_)};
		std::vector<vk::UniqueCommandBuffer> command_buffers_{
			create_command_buffers(*command_pool_, *device_, max_frames_in_flight())};

		vk::Extent2D swapchain_extent_{
			choose_swapchain_extent(query_swapchain_support(physical_device_, *surface_).capabilities, window_)};
		vk::UniqueSwapchainKHR swapchain_{create_swapchain(physical_device_, *surface_, *device_, window_)};
		std::vector<vk::Image> swapchain_images_{device_->getSwapchainImagesKHR(*swapchain_)};
		vk::Format swapchain_image_format_{
			choose_swapchain_surface_format(query_swapchain_support(physical_device_, *surface_).formats).format};
		std::vector<vk::UniqueImageView> swapchain_image_views_{
			create_image_views(swapchain_images_, swapchain_image_format_, *device_)};

		vk::SampleCountFlagBits msaa_samples_{get_max_usable_sample_count(physical_device_)};
		vk::UniqueImage color_msaa_image_{create_image(
			*device_,
			swapchain_extent_.width,
			swapchain_extent_.height,
			1,
			msaa_samples_,
			swapchain_image_format_,
			vk::ImageTiling::eOptimal,
			vk::ImageUsageFlagBits::eTransientAttachment | vk::ImageUsageFlagBits::eColorAttachment)};
		vk::UniqueDeviceMemory color_msaa_image_memory_{
			bind_image_memory(*color_msaa_image_, vk::MemoryPropertyFlagBits::eDeviceLocal)};
		vk::UniqueImageView color_msaa_image_view_{create_image_view(
			*color_msaa_image_, swapchain_image_format_, vk::ImageAspectFlagBits::eColor, 1, *device_)};
		vk::UniqueImage depth_image_{create_image(
			*device_,
			swapchain_extent_.width,
			swapchain_extent_.height,
			1,
			msaa_samples_,
			find_depth_format(physical_device_),
			vk::ImageTiling::eOptimal,
			vk::ImageUsageFlagBits::eDepthStencilAttachment)};
		vk::UniqueDeviceMemory depth_image_memory_{
			bind_image_memory(*depth_image_, vk::MemoryPropertyFlagBits::eDeviceLocal)};
		vk::UniqueImageView depth_image_view_{create_image_view(
			*depth_image_, find_depth_format(physical_device_), vk::ImageAspectFlagBits::eDepth, 1, *device_)};
		vk::UniqueImage resolve_image_{create_image(
			*device_,
			swapchain_extent_.width,
			swapchain_extent_.height,
			1,
			vk::SampleCountFlagBits::e1,
			swapchain_image_format_,
			vk::ImageTiling::eOptimal,
			vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eSampled)};
		vk::UniqueDeviceMemory resolve_image_memory_{
			bind_image_memory(*resolve_image_, vk::MemoryPropertyFlagBits::eDeviceLocal)};
		vk::UniqueImageView resolve_image_view_{
			create_image_view(*resolve_image_, swapchain_image_format_, vk::ImageAspectFlagBits::eColor, 1, *device_)};

		vk::UniqueRenderPass scene_render_pass_{
			create_scene_render_pass(swapchain_image_format_, msaa_samples_, *device_, physical_device_)};
		vk::UniqueRenderPass post_proc_render_pass_{
			create_post_proc_render_pass(swapchain_image_format_, *device_, physical_device_)};
		vk::UniqueFramebuffer scene_framebuffer_{create_scene_framebuffer(
			*color_msaa_image_view_,
			*depth_image_view_,
			*resolve_image_view_,
			*scene_render_pass_,
			swapchain_extent_,
			*device_)};
		std::vector<vk::UniqueFramebuffer> swap_chain_framebuffers_{create_swapchain_framebuffers(
			swapchain_image_views_, *post_proc_render_pass_, swapchain_extent_, *device_)};

		vk::UniqueDescriptorSetLayout descriptor_set_layout_transform_{
			create_descriptor_set_layout_transform(*device_)};
		vk::UniqueDescriptorSetLayout descriptor_set_layout_camera_{create_descriptor_set_layout_camera(*device_)};
		vk::UniqueDescriptorSetLayout descriptor_set_layout_lights_{create_descriptor_set_layout_lights(*device_)};

		std::vector<vk::UniqueSemaphore> image_available_semaphores_{
			create_image_available_semaphores(*device_, max_frames_in_flight())};
		std::vector<vk::UniqueSemaphore> render_finished_semaphores_{
			create_render_finished_semaphores(*device_, max_frames_in_flight())};
		std::vector<vk::UniqueFence> in_flight_fences_{create_in_flight_fences(*device_, max_frames_in_flight())};
		size_t current_frame_{};

		friend class game;
	};
}

constexpr auto vkpg::renderer::max_frames_in_flight() -> int
{
	return 2;
}

template<typename BufferType>
auto vkpg::create_uniform_buffer(vk::Device const& device) -> vk::UniqueBuffer
{
	const auto buffer_size = sizeof(BufferType);
	return create_buffer(buffer_size, vk::BufferUsageFlagBits::eUniformBuffer, device);
}

template<typename BufferType>
auto vkpg::create_uniform_buffers(const int num_buffers, vk::Device const& device) -> std::vector<vk::UniqueBuffer>
{
	// TODO: See if any STL algorithm can be used to improve this code.
	auto buffers = std::vector<vk::UniqueBuffer>{};
	for(auto i = 0; i < num_buffers; ++i)
	{
		buffers.push_back(create_uniform_buffer<BufferType>(device));
	}
	return buffers;
}

template<typename UniformBuffer>
auto vkpg::renderer::update_ubo(vk::DeviceMemory const& buffer_memory, UniformBuffer const& ubo) const
{
	auto const data = device().mapMemory(buffer_memory, 0, sizeof ubo);
	std::memcpy(data, &ubo, sizeof ubo);
	device().unmapMemory(buffer_memory);
}
