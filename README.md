# Vulkan Playground

This is a program that reads a glTF file and renders a scene contained in the file.
The camera can be controlled with standard first-person controls, either with mouse and keyboard, or with a gamepad.

The project has mostly just been a way for me to learn graphics programming and is still a work in progress.
The renderer works fine as it is now, but there are still many features that I would like to add.
There are also many optimization opportunities possible, particularly when it comes to memory allocation, draw call batching and concurrency.

Current features include:
*  Rendering of static meshes
*  Lighting from directional-, point- and spotlights with the Blinn-Phong lighting model
*  Blending of transparent objects
*  Rendering of a skybox
*  Post-processing via sampling of the color buffer
*  Anti-aliasing with MSAA

Some of the features that I have yet to implement include:
*  Shadow mapping
*  Normal mapping
*  Deferred rendering
*  Physically based rendering

In the future, I hope to make the renderer as close as possible to be compliant with the full glTF specification, including a few of the spec extensions.

I also want to implement some raytracing features, or possibly even a full path-tracer, using the Vulkan extension for raytracing.

My process for working on this has so far mostly been based on following [learnopengl.com](https://learnopengl.com/), but doing everything in Vulkan instead of OpenGL.
The shaders are still written in GLSL, but compiled to SPIR-V.