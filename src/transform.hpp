#pragma once

#include <vector>
#include <memory>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

namespace vkpg
{
	struct transform_ubo
	{
		glm::mat4 model{};
	};

	class transform
	{
	public:
		transform() = default;
		transform(glm::vec3 position, glm::vec3 scale, glm::quat rotation);

		auto position() -> glm::vec3&;
		auto scale() -> glm::vec3&;
		auto rotation() -> glm::quat&;

		[[nodiscard]] auto world_rotation() const -> glm::quat;
		auto set_world_rotation(glm::quat rot) -> void;
		[[nodiscard]] auto world_position() const -> glm::vec3;
		[[nodiscard]] auto world_scale() const -> glm::vec3;

		// May be error-prone as previous children and parents are not transferred.
		auto set_parent(transform* parent) -> void;
		auto add_child(std::unique_ptr<transform> child) -> void;

		[[nodiscard]] auto local_matrix() const -> glm::mat4x4;
		[[nodiscard]] auto world_matrix() const -> glm::mat4x4;

		// TODO: Look over these transformations and implement reversed versions.
		[[nodiscard]] auto transform_point(glm::vec3 point) const -> glm::vec3;
		[[nodiscard]] auto transform_vector(glm::vec3 vector) const -> glm::vec3;
		[[nodiscard]] auto transform_direction(glm::vec3 direction) const -> glm::vec3;

	private:
		glm::vec3 position_{0.0f};
		glm::vec3 scale_{1.0f, 1.0f, 1.0f};
		glm::quat rotation_{glm::identity<glm::quat>()};

		transform* parent_{nullptr};
		std::vector<std::unique_ptr<transform>> children_{};
	};
}
