#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_decompose.hpp>

#include <transform.hpp>
#include <camera.hpp>

vkpg::transform::transform(glm::vec3 position, glm::vec3 scale, glm::quat rotation) :
	position_{position},
	scale_{scale},
	rotation_{rotation}
{
}

auto vkpg::transform::position() -> glm::vec3&
{
	return position_;
}

auto vkpg::transform::scale() -> glm::vec3&
{
	return scale_;
}

auto vkpg::transform::rotation() -> glm::quat&
{
	return rotation_;
}

auto vkpg::transform::world_rotation() const -> glm::quat
{
	const auto mat = world_matrix();
	auto scale = glm::vec3{};
	auto rotation = glm::quat{};
	auto translation = glm::vec3{};
	auto skew = glm::vec3{};
	auto perspective = glm::vec4{};
	glm::decompose(mat, scale, rotation, translation, skew, perspective);

	return rotation;
}

auto vkpg::transform::set_world_rotation(const glm::quat rot) -> void
{
	const auto parent_rot = parent_ ? parent_->world_rotation() : glm::identity<glm::quat>();
	rotation_ = glm::inverse(parent_rot) * rot;
}

auto vkpg::transform::world_position() const -> glm::vec3
{
	const auto mat = world_matrix();
	auto scale = glm::vec3{};
	auto rotation = glm::quat{};
	auto translation = glm::vec3{};
	auto skew = glm::vec3{};
	auto perspective = glm::vec4{};
	glm::decompose(mat, scale, rotation, translation, skew, perspective);

	return translation;
}

auto vkpg::transform::world_scale() const -> glm::vec3
{
	const auto mat = world_matrix();
	auto scale = glm::vec3{};
	auto rotation = glm::quat{};
	auto translation = glm::vec3{};
	auto skew = glm::vec3{};
	auto perspective = glm::vec4{};
	glm::decompose(mat, scale, rotation, translation, skew, perspective);

	return scale;
}

auto vkpg::transform::set_parent(transform* parent) -> void
{
	parent_ = parent;
}

auto vkpg::transform::add_child(std::unique_ptr<transform> child) -> void
{
	child->set_parent(this);
	children_.push_back(std::move(child));
}

auto vkpg::transform::local_matrix() const -> glm::mat4x4
{
	return glm::translate(glm::identity<glm::mat4>(), position_) * glm::mat4_cast(rotation_) * glm::scale(
		glm::identity<glm::mat4>(),
		scale_);
}

auto vkpg::transform::world_matrix() const -> glm::mat4x4
{
	return parent_ ? parent_->world_matrix() * local_matrix() : local_matrix();
}

auto vkpg::transform::transform_point(glm::vec3 point) const -> glm::vec3
{
	const auto mat = world_matrix();
	return mat * glm::vec4{point, 1.0f};
}

auto vkpg::transform::transform_vector(glm::vec3 vector) const -> glm::vec3
{
	const auto mat = world_matrix();
	return mat * glm::vec4{vector, 0.0f};
}

auto vkpg::transform::transform_direction(glm::vec3 direction) const -> glm::vec3
{
	return world_rotation() * direction;
}
