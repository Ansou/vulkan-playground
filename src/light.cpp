#include <light.hpp>
#include <game.hpp>

vkpg::directional_light::directional_light(
	vkpg::transform const& trans,
	glm::vec3 ambient,
	glm::vec3 diffuse,
	glm::vec3 specular) :
	transform{&trans},
	ambient{ambient},
	diffuse{diffuse},
	specular{specular}
{
}

vkpg::point_light::point_light(
	vkpg::transform const& trans,
	glm::vec3 ambient,
	glm::vec3 diffuse,
	glm::vec3 specular,
	float constant,
	float linear,
	float quadratic) :
	transform{&trans},
	ambient{ambient},
	diffuse{diffuse},
	specular{specular},
	constant{constant},
	linear{linear},
	quadratic{quadratic}
{
}

vkpg::spot_light::spot_light(
	vkpg::transform const& trans,
	glm::vec3 ambient,
	glm::vec3 diffuse,
	glm::vec3 specular,
	float constant,
	float linear,
	float quadratic,
	float cutoff,
	float outer_cutoff) :
	transform{&trans},
	ambient{ambient},
	diffuse{diffuse},
	specular{specular},
	constant{constant},
	linear{linear},
	quadratic{quadratic},
	cutoff{cutoff},
	outer_cutoff{outer_cutoff}
{
}

auto vkpg::get_light_ubo(directional_light light) -> directional_light_ubo
{
	auto ubo = directional_light_ubo{};
	ubo.direction = light.transform->transform_direction(glm::vec3{0.0f, 0.0f, -1.0f});
	ubo.ambient = light.ambient;
	ubo.diffuse = light.diffuse;
	ubo.specular = light.specular;

	return ubo;
}

auto vkpg::get_light_ubo(point_light light) -> point_light_ubo
{
	auto ubo = point_light_ubo{};
	ubo.position = light.transform->world_position();
	ubo.ambient = light.ambient;
	ubo.diffuse = light.diffuse;
	ubo.specular = light.specular;
	ubo.constant = light.constant;
	ubo.linear = light.linear;
	ubo.quadratic = light.quadratic;

	return ubo;
}

auto vkpg::get_light_ubo(spot_light light) -> spot_light_ubo
{
	auto ubo = spot_light_ubo{};
	ubo.position = light.transform->world_position();
	ubo.direction = light.transform->transform_direction(glm::vec3{0.0f, 0.0f, -1.0f});
	ubo.cutoff = light.cutoff;
	ubo.outer_cutoff = light.outer_cutoff;
	ubo.constant = light.constant;
	ubo.linear = light.linear;
	ubo.quadratic = light.quadratic;
	ubo.ambient = light.ambient;
	ubo.diffuse = light.diffuse;
	ubo.specular = light.specular;

	return ubo;
}
