#include <skybox.hpp>
#include <renderer.hpp>
#include <material.hpp>
#include <camera.hpp>

vkpg::skybox::skybox(renderer const& renderer, model const& model, material_skybox const& material) :
	model_{&model},
	material_{&material},
	transform_buffers_{
		create_uniform_buffers<skybox_mvp_ubo>(
			renderer::max_frames_in_flight(),
			renderer.device())
	},
	transform_buffers_memory_{bind_uniform_buffers_memory(transform_buffers_, renderer)},
	descriptor_{
		renderer,
		material_->shader().descriptor_set_layout(),
		get_frame_arr(transform_buffers_),
		get_frame_arr(material_->texture())
	}
{
}

auto vkpg::skybox::record_draw_commands(vk::CommandBuffer& command_buffer, uint32_t const current_frame) const -> void
{
	command_buffer.bindPipeline(vk::PipelineBindPoint::eGraphics, material_->pipeline());
	command_buffer.bindVertexBuffers(0, model_->vertex_buffer(), vk::DeviceSize{0});
	command_buffer.bindIndexBuffer(model_->index_buffer(), 0, vk::IndexType::eUint32);
	command_buffer.bindDescriptorSets(
		vk::PipelineBindPoint::eGraphics,
		material_->shader().pipeline_layout(),
		0,
		1,
		&descriptor_.descriptor_set(current_frame),
		0,
		nullptr);
	command_buffer.drawIndexed(static_cast<uint32_t>(model_->indices().size()), 1, 0, 0, 0);
}

auto vkpg::skybox::update_transform_buffer(
	vkpg::renderer const& renderer,
	camera const& cam,
	const uint32_t current_frame) -> void
{
	auto ubo = skybox_mvp_ubo{};
	ubo.proj = cam.projection_matrix();
	// Only use rotation of view matrix.
	ubo.view = glm::inverse(glm::mat4_cast(cam.get_transform().world_rotation()));

	renderer.update_ubo(*transform_buffers_memory_[current_frame], ubo);
}
