#pragma once

#include <renderer.hpp>
#include <vulkan/vulkan.hpp>

namespace vkpg
{
	class texture;
	class cubemap;

	template<typename T>
	using frame_arr = std::array<T const*, renderer::max_frames_in_flight()>;

	template<typename T>
	auto get_frame_arr(T const& obj) -> frame_arr<T>;

	auto get_frame_arr(std::vector<vk::UniqueBuffer> const& vec) -> frame_arr<vk::Buffer>;

	class descriptor_transform
	{
	public:
		descriptor_transform(
			renderer const& renderer, vk::DescriptorSetLayout const& layout, frame_arr<vk::Buffer> transform_buffers);

		[[nodiscard]] auto descriptor_set(uint32_t current_frame) const -> vk::DescriptorSet const&;

	private:
		static auto create_pool(renderer const& renderer) -> vk::UniqueDescriptorPool;
		auto update_descriptor_sets(renderer const& renderer, frame_arr<vk::Buffer> transform_buffers) -> void;

		vk::UniqueDescriptorPool descriptor_pool_;
		std::vector<vk::UniqueDescriptorSet> descriptor_sets_;
	};

	class descriptor_camera
	{
	public:
		descriptor_camera(
			renderer const& renderer, vk::DescriptorSetLayout const& layout, frame_arr<vk::Buffer> camera_buffers);

		[[nodiscard]] auto descriptor_set(uint32_t current_frame) const -> vk::DescriptorSet const&;

	private:
		static auto create_pool(renderer const& renderer) -> vk::UniqueDescriptorPool;
		auto update_descriptor_sets(renderer const& renderer, frame_arr<vk::Buffer> camera_buffers) -> void;

		vk::UniqueDescriptorPool descriptor_pool_;
		std::vector<vk::UniqueDescriptorSet> descriptor_sets_;
	};

	class descriptor_lights
	{
	public:
		descriptor_lights(
			renderer const& renderer, vk::DescriptorSetLayout const& layout, frame_arr<vk::Buffer> lights_buffers);

		[[nodiscard]] auto descriptor_set(uint32_t current_frame) const -> vk::DescriptorSet const&;

	private:
		static auto create_pool(renderer const& renderer) -> vk::UniqueDescriptorPool;
		auto update_descriptor_sets(renderer const& renderer, frame_arr<vk::Buffer> lights_buffers) -> void;

		vk::UniqueDescriptorPool descriptor_pool_;
		std::vector<vk::UniqueDescriptorSet> descriptor_sets_;
	};

	class descriptor_standard
	{
	public:
		descriptor_standard(
			renderer const& renderer,
			vk::DescriptorSetLayout const& layout,
			frame_arr<vk::Buffer> material_buffers,
			frame_arr<texture> albedo_textures,
			frame_arr<texture> specular_textures);

		[[nodiscard]] auto descriptor_set(uint32_t current_frame) const -> vk::DescriptorSet const&;

	private:
		static auto create_pool(renderer const& renderer) -> vk::UniqueDescriptorPool;
		auto update_descriptor_sets(
			renderer const& renderer,
			frame_arr<vk::Buffer> material_buffers,
			frame_arr<texture> albedo_textures,
			frame_arr<texture> specular_textures) -> void;

		vk::UniqueDescriptorPool descriptor_pool_;
		std::vector<vk::UniqueDescriptorSet> descriptor_sets_;
	};

	class descriptor_skybox
	{
	public:
		descriptor_skybox(
			renderer const& renderer,
			vk::DescriptorSetLayout const& layout,
			frame_arr<vk::Buffer> transform_buffers,
			frame_arr<cubemap> textures);

		[[nodiscard]] auto descriptor_set(uint32_t current_frame) const -> vk::DescriptorSet const&;

	private:
		static auto create_pool(renderer const& renderer) -> vk::UniqueDescriptorPool;
		auto update_descriptor_sets(
			renderer const& renderer, frame_arr<vk::Buffer> transform_buffers, frame_arr<cubemap> textures) -> void;

		vk::UniqueDescriptorPool descriptor_pool_;
		std::vector<vk::UniqueDescriptorSet> descriptor_sets_;
	};

	class descriptor_post_process
	{
	public:
		descriptor_post_process(
			renderer const& renderer,
			vk::DescriptorSetLayout const& layout,
			vk::ImageView const& image_view,
			vk::Sampler const& sampler);

		auto
			update_descriptor_set(renderer const& renderer, vk::ImageView const& image_view, vk::Sampler const& sampler)
				-> void;
		[[nodiscard]] auto descriptor_set() const -> vk::DescriptorSet const&;

	private:
		static auto create_pool(renderer const& renderer) -> vk::UniqueDescriptorPool;

		vk::UniqueDescriptorPool descriptor_pool_;
		vk::UniqueDescriptorSet descriptor_set_;
	};
}

template<typename T>
auto vkpg::get_frame_arr(T const& obj) -> frame_arr<T>
{
	auto arr = frame_arr<T>{};
	for(auto i = 0; i < arr.size(); ++i)
	{
		arr[i] = &obj;
	}

	return arr;
}
