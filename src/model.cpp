#include <unordered_map>
#include <stdexcept>

#include <tiny_obj_loader.h>

#include <model.hpp>
#include <vertex.hpp>
#include <renderer.hpp>

vkpg::model::model(std::filesystem::path const& file_path, renderer const& renderer)
{
	std::tie(vertices_, indices_) = load_model(file_path);
	std::tie(vertex_buffer_, vertex_buffer_memory_) = create_vertex_buffer(vertices_, renderer);
	std::tie(index_buffer_, index_buffer_memory_) = create_index_buffer(indices_, renderer);
}

vkpg::model::
model(std::vector<vertex> vertices, std::vector<uint32_t> indices, renderer const& renderer) :
	vertices_{
		std::move(vertices)
	},
	indices_{
		std::move(indices)
	}
{
	std::tie(vertex_buffer_, vertex_buffer_memory_) = create_vertex_buffer(vertices_, renderer);
	std::tie(index_buffer_, index_buffer_memory_) = create_index_buffer(indices_, renderer);
}

auto vkpg::model::indices() const -> std::vector<uint32_t> const&
{
	return indices_;
}

auto vkpg::model::vertex_buffer() const -> vk::Buffer const&
{
	return *vertex_buffer_;
}

auto vkpg::model::index_buffer() const -> vk::Buffer const&
{
	return *index_buffer_;
}

auto vkpg::load_model(const std::filesystem::path& file_path) -> std::pair<std::vector<vertex>, std::vector<uint32_t>>
{
	auto attrib = tinyobj::attrib_t{};
	auto shapes = std::vector<tinyobj::shape_t>{};
	auto materials = std::vector<tinyobj::material_t>{};
	auto warn = std::string{};
	auto err = std::string{};

	if(!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, file_path.string().c_str()))
	{
		throw std::runtime_error{warn + err};
	}

	auto unique_vertices = std::unordered_map<vertex, uint32_t>{};

	auto vertices = std::vector<vertex>{};
	auto indices = std::vector<uint32_t>{};
	for(const auto& shape : shapes)
	{
		for(const auto& index : shape.mesh.indices)
		{
			auto vert = vertex{};

			vert.pos = {
				attrib.vertices[3 * index.vertex_index + 0],
				attrib.vertices[3 * index.vertex_index + 1],
				attrib.vertices[3 * index.vertex_index + 2]
			};

			vert.tex_coord = {
				attrib.texcoords[2 * index.texcoord_index + 0],
				1.0f - attrib.texcoords[2 * index.texcoord_index + 1]
			};

			vert.normal = {1.0f, 1.0f, 1.0f};

			if(unique_vertices.count(vert) == 0)
			{
				unique_vertices[vert] = static_cast<uint32_t>(vertices.size());
				vertices.push_back(vert);
			}

			indices.push_back(unique_vertices[vert]);
		}
	}

	return std::make_pair(std::move(vertices), std::move(indices));
}

auto vkpg::create_vertex_buffer(
	std::vector<vertex> const& vertices,
	renderer const& ren) -> std::pair<vk::UniqueBuffer, vk::UniqueDeviceMemory>
{
	const auto buffer_size = sizeof vertices[0] * vertices.size();

	auto staging_buffer = create_buffer(buffer_size, vk::BufferUsageFlagBits::eTransferSrc, ren.device());
	auto staging_buffer_memory = ren.bind_buffer_memory(
		*staging_buffer,
		vk::MemoryPropertyFlagBits::eHostVisible | vk::
		MemoryPropertyFlagBits::eHostCoherent);

	auto const data = ren.device().mapMemory(*staging_buffer_memory, 0, buffer_size, vk::MemoryMapFlags{});
	memcpy(data, vertices.data(), static_cast<size_t>(buffer_size));
	ren.device().unmapMemory(*staging_buffer_memory);

	auto vertex_buffer = create_buffer(
		buffer_size,
		vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::
		eVertexBuffer,
		ren.device());
	auto vertex_buffer_memory = ren.bind_buffer_memory(*vertex_buffer, vk::MemoryPropertyFlagBits::eDeviceLocal);

	ren.copy_buffer(*staging_buffer, *vertex_buffer, buffer_size);

	return std::make_pair(std::move(vertex_buffer), std::move(vertex_buffer_memory));
}

auto vkpg::create_index_buffer(
	std::vector<uint32_t> const& indices,
	renderer const& ren) -> std::pair<vk::UniqueBuffer, vk::UniqueDeviceMemory>
{
	const auto buffer_size = sizeof(indices[0]) * indices.size();

	auto staging_buffer = create_buffer(buffer_size, vk::BufferUsageFlagBits::eTransferSrc, ren.device());
	auto staging_buffer_memory = ren.bind_buffer_memory(
		*staging_buffer,
		vk::MemoryPropertyFlagBits::eHostVisible | vk::
		MemoryPropertyFlagBits::eHostCoherent);

	auto const data = ren.device().mapMemory(*staging_buffer_memory, 0, buffer_size, vk::MemoryMapFlags{});
	memcpy(data, indices.data(), static_cast<size_t>(buffer_size));
	ren.device().unmapMemory(*staging_buffer_memory);

	auto index_buffer = create_buffer(
		buffer_size,
		vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eIndexBuffer,
		ren.device());
	auto index_buffer_memory = ren.bind_buffer_memory(*index_buffer, vk::MemoryPropertyFlagBits::eDeviceLocal);

	ren.copy_buffer(*staging_buffer, *index_buffer, buffer_size);

	return std::make_pair(std::move(index_buffer), std::move(index_buffer_memory));
}
