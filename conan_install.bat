@echo off
mkdir build
cd build
mkdir Debug
cd Debug
conan install ../.. -s build_type=Debug --build missing
cd ..
mkdir Release
cd Release
conan install ../.. -s build_type=Release --build missing
pause