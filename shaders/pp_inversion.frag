#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

layout(binding = 0) uniform sampler2D texSampler;


void main()
{
    vec4 texColor = texture(texSampler, fragTexCoord);
    outColor = vec4(vec3(1.0 - texColor), 1.0);
}
