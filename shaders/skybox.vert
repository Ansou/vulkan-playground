#version 450

layout(location = 0) in vec3 inPos;

layout(location = 0) out vec3 outUVW;

layout(binding = 0) uniform MVPObject {
    mat4 model;
    mat4 view;
    mat4 proj;
} mvp;

void main()
{
    outUVW = inPos;
    outUVW.x *= -1.0;
    gl_Position = mvp.proj * mvp.view * vec4(inPos.xyz, 1.0);
}
