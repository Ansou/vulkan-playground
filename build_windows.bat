@echo off
mkdir build
cd build
conan install .. -s build_type=Debug --build missing
cmake .. -DCMAKE_TOOLCHAIN_FILE=conan_paths.cmake -G "Visual Studio 16 2019"
pause