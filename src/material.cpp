#include <files.hpp>
#include <material.hpp>
#include <scene.hpp>
#include <vertex.hpp>

vkpg::material_standard::material_standard(
	renderer const& renderer,
	shader_standard const& shader,
	texture const& tex,
	texture const& specular_map,
	float const shininess,
	blend_mode const blend_mode,
	vk::RenderPass const& render_pass) :
	shader_{&shader},
	texture_{&tex},
	specular_map_{&specular_map},
	shininess_{shininess},
	blend_mode_{blend_mode},
	uniform_buffer_{create_uniform_buffer<ubo_material_standard>(renderer.device())},
	uniform_buffer_memory_{renderer.bind_buffer_memory(
		*uniform_buffer_, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent)},
	pipeline_{create_pipeline(renderer, render_pass)},
	descriptor_{
		renderer,
		shader.descriptor_set_layout(),
		get_frame_arr(*uniform_buffer_),
		get_frame_arr(tex),
		get_frame_arr(specular_map)}
{
	update_uniform_buffer(renderer);
}

auto vkpg::material_standard::recreate_pipeline(renderer const& renderer, vk::RenderPass const& render_pass) -> void
{
	pipeline_ = create_pipeline(renderer, render_pass);
}

auto vkpg::material_standard::shader() const -> shader_standard const&
{
	return *shader_;
}

auto vkpg::material_standard::albedo() const -> texture const&
{
	return *texture_;
}

auto vkpg::material_standard::specular() const -> texture const&
{
	return *specular_map_;
}

auto vkpg::material_standard::get_blend_mode() const -> blend_mode
{
	return blend_mode_;
}

auto vkpg::material_standard::pipeline() const -> vk::Pipeline const&
{
	return *pipeline_;
}

auto vkpg::material_standard::uniform_buffer() const -> vk::Buffer const&
{
	return *uniform_buffer_;
}

auto vkpg::material_standard::descriptor(uint32_t current_frame) const -> vk::DescriptorSet const&
{
	return descriptor_.descriptor_set(current_frame);
}

auto vkpg::material_standard::create_pipeline(renderer const& renderer, vk::RenderPass const& render_pass) const
	-> vk::UniquePipeline
{
	auto vert_shader_stage_info = vk::PipelineShaderStageCreateInfo{};
	vert_shader_stage_info.stage = vk::ShaderStageFlagBits::eVertex;
	vert_shader_stage_info.module = shader_->vertex_shader();
	vert_shader_stage_info.pName = "main";

	auto frag_shader_stage_info = vk::PipelineShaderStageCreateInfo{};
	frag_shader_stage_info.stage = vk::ShaderStageFlagBits::eFragment;
	frag_shader_stage_info.module = shader_->fragment_shader();
	frag_shader_stage_info.pName = "main";

	auto shader_stages =
		std::array<vk::PipelineShaderStageCreateInfo, 2>{vert_shader_stage_info, frag_shader_stage_info};

	auto binding_description = vertex::get_binding_description();
	auto attribute_descriptions = vertex::get_attribute_descriptions();

	auto vertex_input_info = vk::PipelineVertexInputStateCreateInfo{};
	vertex_input_info.vertexBindingDescriptionCount = 1;
	vertex_input_info.pVertexBindingDescriptions = &binding_description;
	vertex_input_info.vertexAttributeDescriptionCount = static_cast<uint32_t>(attribute_descriptions.size());
	vertex_input_info.pVertexAttributeDescriptions = attribute_descriptions.data();

	auto input_assembly = vk::PipelineInputAssemblyStateCreateInfo{};
	input_assembly.topology = vk::PrimitiveTopology::eTriangleList;
	input_assembly.primitiveRestartEnable = false;

	auto draw_resolution = renderer.current_resolution();

	auto viewport = vk::Viewport{};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = static_cast<float>(draw_resolution.width);
	viewport.height = static_cast<float>(draw_resolution.height);
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	auto scissor = vk::Rect2D{};
	scissor.offset = vk::Offset2D{0, 0};
	scissor.extent = draw_resolution;

	auto viewport_state = vk::PipelineViewportStateCreateInfo{};
	viewport_state.viewportCount = 1;
	viewport_state.pViewports = &viewport;
	viewport_state.scissorCount = 1;
	viewport_state.pScissors = &scissor;

	auto rasterizer = vk::PipelineRasterizationStateCreateInfo{};
	rasterizer.depthClampEnable = false;
	rasterizer.rasterizerDiscardEnable = false;
	rasterizer.polygonMode = vk::PolygonMode::eFill;
	rasterizer.lineWidth = 1.0f;
	rasterizer.cullMode = vk::CullModeFlagBits::eBack;
	rasterizer.frontFace = vk::FrontFace::eCounterClockwise;
	rasterizer.depthBiasEnable = false;
	rasterizer.depthBiasConstantFactor = 0.0f;
	rasterizer.depthBiasClamp = 0.0f;
	rasterizer.depthBiasSlopeFactor = 0.0f;

	auto multisampling = vk::PipelineMultisampleStateCreateInfo{};
	multisampling.sampleShadingEnable = false;
	multisampling.rasterizationSamples = renderer.msaa_samples();
	multisampling.minSampleShading = 1.0f;
	multisampling.pSampleMask = nullptr;
	multisampling.alphaToCoverageEnable = false;
	multisampling.alphaToOneEnable = false;

	auto color_blend_attachment = vk::PipelineColorBlendAttachmentState{};
	color_blend_attachment.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG |
		vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
	color_blend_attachment.blendEnable = blend_mode_ == blend_mode::blend;
	color_blend_attachment.srcColorBlendFactor = vk::BlendFactor::eSrcAlpha;
	color_blend_attachment.dstColorBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha;
	color_blend_attachment.colorBlendOp = vk::BlendOp::eAdd;
	color_blend_attachment.srcAlphaBlendFactor = vk::BlendFactor::eOne;
	color_blend_attachment.dstAlphaBlendFactor = vk::BlendFactor::eZero;
	color_blend_attachment.alphaBlendOp = vk::BlendOp::eAdd;

	auto color_blending = vk::PipelineColorBlendStateCreateInfo{};
	color_blending.logicOpEnable = false;
	color_blending.logicOp = vk::LogicOp::eCopy;
	color_blending.attachmentCount = 1;
	color_blending.pAttachments = &color_blend_attachment;
	color_blending.blendConstants[0] = 0.0f;
	color_blending.blendConstants[1] = 0.0f;
	color_blending.blendConstants[2] = 0.0f;
	color_blending.blendConstants[3] = 0.0f;

	auto depth_stencil = vk::PipelineDepthStencilStateCreateInfo{};
	depth_stencil.depthTestEnable = true;
	depth_stencil.depthWriteEnable = blend_mode_ != blend_mode::blend;
	depth_stencil.depthCompareOp = vk::CompareOp::eLess;
	depth_stencil.depthBoundsTestEnable = false;
	depth_stencil.minDepthBounds = 0.0f;
	depth_stencil.maxDepthBounds = 1.0f;
	depth_stencil.stencilTestEnable = false;
	depth_stencil.front = vk::StencilOpState{};
	depth_stencil.back = vk::StencilOpState{};

	auto pipeline_layout_info = vk::PipelineLayoutCreateInfo{};
	pipeline_layout_info.setLayoutCount = 1;
	pipeline_layout_info.pSetLayouts = &shader_->descriptor_set_layout();
	pipeline_layout_info.pushConstantRangeCount = 0;
	pipeline_layout_info.pPushConstantRanges = nullptr;

	auto pipeline_info = vk::GraphicsPipelineCreateInfo{};
	pipeline_info.stageCount = shader_stages.size();
	pipeline_info.pStages = shader_stages.data();
	pipeline_info.pVertexInputState = &vertex_input_info;
	pipeline_info.pInputAssemblyState = &input_assembly;
	pipeline_info.pViewportState = &viewport_state;
	pipeline_info.pRasterizationState = &rasterizer;
	pipeline_info.pMultisampleState = &multisampling;
	pipeline_info.pDepthStencilState = &depth_stencil;
	pipeline_info.pColorBlendState = &color_blending;
	pipeline_info.pDynamicState = nullptr;
	pipeline_info.layout = shader_->pipeline_layout();
	pipeline_info.renderPass = render_pass;
	pipeline_info.subpass = 0;
	pipeline_info.basePipelineHandle = nullptr;
	pipeline_info.basePipelineIndex = -1;

	return renderer.device().createGraphicsPipelineUnique(nullptr, pipeline_info);
}

auto vkpg::material_standard::update_uniform_buffer(renderer const& renderer) -> void
{
	auto ubo = ubo_material_standard{};
	ubo.shininess = shininess_;
	ubo.masking = blend_mode_ == blend_mode::mask;

	void* data;
	renderer.device().mapMemory(*uniform_buffer_memory_, 0, sizeof ubo, vk::MemoryMapFlags{}, &data);
	memcpy(data, &ubo, sizeof ubo);
	renderer.device().unmapMemory(*uniform_buffer_memory_);
}

vkpg::material_skybox::material_skybox(
	renderer const& renderer, shader_skybox const& shader, cubemap const& tex, vk::RenderPass const& render_pass) :
	shader_{&shader}, texture_{&tex}, pipeline_{create_pipeline(renderer, render_pass)}
{
}

auto vkpg::material_skybox::recreate_pipeline(renderer const& renderer, vk::RenderPass const& render_pass) -> void
{
	pipeline_ = create_pipeline(renderer, render_pass);
}

auto vkpg::material_skybox::shader() const -> shader_skybox const&
{
	return *shader_;
}

auto vkpg::material_skybox::texture() const -> cubemap const&
{
	return *texture_;
}

auto vkpg::material_skybox::pipeline() const -> vk::Pipeline const&
{
	return *pipeline_;
}

auto vkpg::material_skybox::create_pipeline(renderer const& renderer, vk::RenderPass const& render_pass) const
	-> vk::UniquePipeline
{
	auto vert_shader_stage_info = vk::PipelineShaderStageCreateInfo{};
	vert_shader_stage_info.stage = vk::ShaderStageFlagBits::eVertex;
	vert_shader_stage_info.module = shader().vertex_shader();
	vert_shader_stage_info.pName = "main";

	auto frag_shader_stage_info = vk::PipelineShaderStageCreateInfo{};
	frag_shader_stage_info.stage = vk::ShaderStageFlagBits::eFragment;
	frag_shader_stage_info.module = shader().fragment_shader();
	frag_shader_stage_info.pName = "main";

	auto shader_stages =
		std::array<vk::PipelineShaderStageCreateInfo, 2>{vert_shader_stage_info, frag_shader_stage_info};

	auto binding_description = vk::VertexInputBindingDescription{};
	binding_description.binding = 0;
	binding_description.stride = sizeof(vertex);
	binding_description.inputRate = vk::VertexInputRate::eVertex;

	auto attribute_descriptions = std::array<vk::VertexInputAttributeDescription, 1>{};
	attribute_descriptions[0].binding = 0;
	attribute_descriptions[0].location = 0;
	attribute_descriptions[0].format = vk::Format::eR32G32B32Sfloat;
	attribute_descriptions[0].offset = 0;

	auto vertex_input_info = vk::PipelineVertexInputStateCreateInfo{};
	vertex_input_info.vertexBindingDescriptionCount = 1;
	vertex_input_info.pVertexBindingDescriptions = &binding_description;
	vertex_input_info.vertexAttributeDescriptionCount = static_cast<uint32_t>(attribute_descriptions.size());
	vertex_input_info.pVertexAttributeDescriptions = attribute_descriptions.data();

	auto input_assembly = vk::PipelineInputAssemblyStateCreateInfo{};
	input_assembly.topology = vk::PrimitiveTopology::eTriangleList;
	input_assembly.primitiveRestartEnable = false;

	auto draw_resolution = renderer.current_resolution();

	auto viewport = vk::Viewport{};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = static_cast<float>(draw_resolution.width);
	viewport.height = static_cast<float>(draw_resolution.height);
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	auto scissor = vk::Rect2D{};
	scissor.offset = vk::Offset2D{0, 0};
	scissor.extent = draw_resolution;

	auto viewport_state = vk::PipelineViewportStateCreateInfo{};
	viewport_state.viewportCount = 1;
	viewport_state.pViewports = &viewport;
	viewport_state.scissorCount = 1;
	viewport_state.pScissors = &scissor;

	auto rasterizer = vk::PipelineRasterizationStateCreateInfo{};
	rasterizer.depthClampEnable = false;
	rasterizer.rasterizerDiscardEnable = false;
	rasterizer.polygonMode = vk::PolygonMode::eFill;
	rasterizer.lineWidth = 1.0f;
	rasterizer.cullMode = vk::CullModeFlagBits::eBack;
	rasterizer.frontFace = vk::FrontFace::eClockwise;
	rasterizer.depthBiasEnable = false;
	rasterizer.depthBiasConstantFactor = 0.0f;
	rasterizer.depthBiasClamp = 0.0f;
	rasterizer.depthBiasSlopeFactor = 0.0f;

	auto multisampling = vk::PipelineMultisampleStateCreateInfo{};
	multisampling.sampleShadingEnable = false;
	multisampling.rasterizationSamples = renderer.msaa_samples();
	multisampling.minSampleShading = 1.0f;
	multisampling.pSampleMask = nullptr;
	multisampling.alphaToCoverageEnable = false;
	multisampling.alphaToOneEnable = false;

	auto color_blend_attachment = vk::PipelineColorBlendAttachmentState{};
	color_blend_attachment.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG |
		vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
	color_blend_attachment.blendEnable = false;
	color_blend_attachment.srcColorBlendFactor = vk::BlendFactor::eSrcAlpha;
	color_blend_attachment.dstColorBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha;
	color_blend_attachment.colorBlendOp = vk::BlendOp::eAdd;
	color_blend_attachment.srcAlphaBlendFactor = vk::BlendFactor::eOne;
	color_blend_attachment.dstAlphaBlendFactor = vk::BlendFactor::eZero;
	color_blend_attachment.alphaBlendOp = vk::BlendOp::eAdd;

	auto color_blending = vk::PipelineColorBlendStateCreateInfo{};
	color_blending.logicOpEnable = false;
	color_blending.logicOp = vk::LogicOp::eCopy;
	color_blending.attachmentCount = 1;
	color_blending.pAttachments = &color_blend_attachment;
	color_blending.blendConstants[0] = 0.0f;
	color_blending.blendConstants[1] = 0.0f;
	color_blending.blendConstants[2] = 0.0f;
	color_blending.blendConstants[3] = 0.0f;

	auto depth_stencil = vk::PipelineDepthStencilStateCreateInfo{};
	depth_stencil.depthTestEnable = false;
	depth_stencil.depthWriteEnable = false;
	depth_stencil.depthCompareOp = vk::CompareOp::eLess;
	depth_stencil.depthBoundsTestEnable = false;
	depth_stencil.minDepthBounds = 0.0f;
	depth_stencil.maxDepthBounds = 1.0f;
	depth_stencil.stencilTestEnable = false;
	depth_stencil.front = vk::StencilOpState{};
	depth_stencil.back = vk::StencilOpState{};

	auto pipeline_info = vk::GraphicsPipelineCreateInfo{};
	pipeline_info.stageCount = shader_stages.size();
	pipeline_info.pStages = shader_stages.data();
	pipeline_info.pVertexInputState = &vertex_input_info;
	pipeline_info.pInputAssemblyState = &input_assembly;
	pipeline_info.pViewportState = &viewport_state;
	pipeline_info.pRasterizationState = &rasterizer;
	pipeline_info.pMultisampleState = &multisampling;
	pipeline_info.pDepthStencilState = &depth_stencil;
	pipeline_info.pColorBlendState = &color_blending;
	pipeline_info.pDynamicState = nullptr;
	pipeline_info.layout = shader().pipeline_layout();
	pipeline_info.renderPass = render_pass;
	pipeline_info.subpass = 0;
	pipeline_info.basePipelineHandle = nullptr;
	pipeline_info.basePipelineIndex = -1;

	return renderer.device().createGraphicsPipelineUnique(nullptr, pipeline_info);
}

vkpg::material_post_process::material_post_process(
	renderer const& renderer,
	shader_post_process const& shader,
	vk::ImageView const& image_view,
	vk::Sampler const& sampler,
	vk::RenderPass const& render_pass) :
	shader_{&shader}, image_view_{&image_view}, sampler_{&sampler}, pipeline_{create_pipeline(renderer, render_pass)}
{
}

auto vkpg::material_post_process::update_image_view(vk::ImageView const& image_view) -> void
{
	image_view_ = &image_view;
}

auto vkpg::material_post_process::recreate_pipeline(renderer const& renderer, vk::RenderPass const& render_pass) -> void
{
	pipeline_ = create_pipeline(renderer, render_pass);
}

auto vkpg::material_post_process::shader() const -> shader_post_process const&
{
	return *shader_;
}

auto vkpg::material_post_process::image_view() const -> vk::ImageView const&
{
	return *image_view_;
}

auto vkpg::material_post_process::sampler() const -> vk::Sampler const&
{
	return *sampler_;
}

auto vkpg::material_post_process::pipeline() const -> vk::Pipeline const&
{
	return *pipeline_;
}

auto vkpg::material_post_process::create_pipeline(renderer const& renderer, vk::RenderPass const& render_pass) const
	-> vk::UniquePipeline
{
	auto vert_shader_stage_info = vk::PipelineShaderStageCreateInfo{};
	vert_shader_stage_info.stage = vk::ShaderStageFlagBits::eVertex;
	vert_shader_stage_info.module = shader().vertex_shader();
	vert_shader_stage_info.pName = "main";

	auto frag_shader_stage_info = vk::PipelineShaderStageCreateInfo{};
	frag_shader_stage_info.stage = vk::ShaderStageFlagBits::eFragment;
	frag_shader_stage_info.module = shader().fragment_shader();
	frag_shader_stage_info.pName = "main";

	auto shader_stages =
		std::array<vk::PipelineShaderStageCreateInfo, 2>{vert_shader_stage_info, frag_shader_stage_info};

	auto rasterizer = vk::PipelineRasterizationStateCreateInfo{};
	rasterizer.depthClampEnable = false;
	rasterizer.rasterizerDiscardEnable = false;
	rasterizer.polygonMode = vk::PolygonMode::eFill;
	rasterizer.lineWidth = 1.0f;
	rasterizer.cullMode = vk::CullModeFlagBits::eNone;
	rasterizer.frontFace = vk::FrontFace::eCounterClockwise;
	rasterizer.depthBiasEnable = false;
	rasterizer.depthBiasConstantFactor = 0.0f;
	rasterizer.depthBiasClamp = 0.0f;
	rasterizer.depthBiasSlopeFactor = 0.0f;

	auto resolution = renderer.current_resolution();

	auto viewport = vk::Viewport{};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = static_cast<float>(resolution.width);
	viewport.height = static_cast<float>(resolution.height);
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	auto scissor = vk::Rect2D{};
	scissor.offset = vk::Offset2D{0, 0};
	scissor.extent = resolution;

	auto viewport_state = vk::PipelineViewportStateCreateInfo{};
	viewport_state.viewportCount = 1;
	viewport_state.pViewports = &viewport;
	viewport_state.scissorCount = 1;
	viewport_state.pScissors = &scissor;

	auto multisampling = vk::PipelineMultisampleStateCreateInfo{};
	multisampling.sampleShadingEnable = false;
	multisampling.rasterizationSamples = vk::SampleCountFlagBits::e1;
	multisampling.minSampleShading = 1.0f;
	multisampling.pSampleMask = nullptr;
	multisampling.alphaToCoverageEnable = false;
	multisampling.alphaToOneEnable = false;

	auto input_assembly = vk::PipelineInputAssemblyStateCreateInfo{};
	input_assembly.topology = vk::PrimitiveTopology::eTriangleList;
	input_assembly.primitiveRestartEnable = false;

	auto vertex_input_info = vk::PipelineVertexInputStateCreateInfo{};
	vertex_input_info.vertexBindingDescriptionCount = 0;
	vertex_input_info.pVertexBindingDescriptions = nullptr;
	vertex_input_info.vertexAttributeDescriptionCount = 0;
	vertex_input_info.pVertexAttributeDescriptions = nullptr;

	auto color_blend_attachment = vk::PipelineColorBlendAttachmentState{};
	color_blend_attachment.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG |
		vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
	color_blend_attachment.blendEnable = false;

	auto color_blending = vk::PipelineColorBlendStateCreateInfo{};
	color_blending.logicOpEnable = false;
	color_blending.logicOp = vk::LogicOp::eCopy;
	color_blending.attachmentCount = 1;
	color_blending.pAttachments = &color_blend_attachment;
	color_blending.blendConstants[0] = 0.0f;
	color_blending.blendConstants[1] = 0.0f;
	color_blending.blendConstants[2] = 0.0f;
	color_blending.blendConstants[3] = 0.0f;

	auto depth_stencil = vk::PipelineDepthStencilStateCreateInfo{};
	depth_stencil.depthTestEnable = false;
	depth_stencil.depthWriteEnable = false;
	depth_stencil.depthCompareOp = vk::CompareOp::eLess;
	depth_stencil.depthBoundsTestEnable = false;
	depth_stencil.minDepthBounds = 0.0f;
	depth_stencil.maxDepthBounds = 1.0f;
	depth_stencil.stencilTestEnable = false;
	depth_stencil.front = vk::StencilOpState{};
	depth_stencil.back = vk::StencilOpState{};

	auto pipeline_info = vk::GraphicsPipelineCreateInfo{};
	pipeline_info.stageCount = shader_stages.size();
	pipeline_info.pStages = shader_stages.data();
	pipeline_info.pVertexInputState = &vertex_input_info;
	pipeline_info.pInputAssemblyState = &input_assembly;
	pipeline_info.pViewportState = &viewport_state;
	pipeline_info.pRasterizationState = &rasterizer;
	pipeline_info.pMultisampleState = &multisampling;
	pipeline_info.pDepthStencilState = &depth_stencil;
	pipeline_info.pColorBlendState = &color_blending;
	pipeline_info.pDynamicState = nullptr;
	pipeline_info.layout = shader().pipeline_layout();
	pipeline_info.renderPass = render_pass;
	pipeline_info.subpass = 0;
	pipeline_info.basePipelineHandle = nullptr;
	pipeline_info.basePipelineIndex = -1;

	return renderer.device().createGraphicsPipelineUnique(nullptr, pipeline_info);
}
