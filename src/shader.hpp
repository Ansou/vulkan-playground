#pragma once

#include <filesystem>

#include <vulkan/vulkan.hpp>

namespace vkpg
{
	class renderer;

	class shader_standard
	{
	public:
		shader_standard(
			renderer const& renderer,
			std::filesystem::path const& vert_shader_path,
			std::filesystem::path const& frag_shader_path);

		[[nodiscard]] auto vertex_shader() const -> vk::ShaderModule const&;
		[[nodiscard]] auto fragment_shader() const -> vk::ShaderModule const&;
		[[nodiscard]] auto descriptor_set_layout() const -> vk::DescriptorSetLayout const&;
		[[nodiscard]] auto pipeline_layout() const -> vk::PipelineLayout const&;

	private:
		static auto create_descriptor_set_layout(renderer const& renderer) -> vk::UniqueDescriptorSetLayout;
		auto create_pipeline_layout(renderer const& renderer) -> vk::UniquePipelineLayout;

		vk::UniqueShaderModule vertex_shader_;
		vk::UniqueShaderModule fragment_shader_;
		vk::UniqueDescriptorSetLayout descriptor_set_layout_;
		vk::UniquePipelineLayout pipeline_layout_;
	};

	class shader_skybox
	{
	public:
		shader_skybox(
			renderer const& renderer,
			std::filesystem::path const& vert_shader_path,
			std::filesystem::path const& frag_shader_path);

		[[nodiscard]] auto vertex_shader() const -> vk::ShaderModule const&;
		[[nodiscard]] auto fragment_shader() const -> vk::ShaderModule const&;
		[[nodiscard]] auto descriptor_set_layout() const -> vk::DescriptorSetLayout const&;
		[[nodiscard]] auto pipeline_layout() const -> vk::PipelineLayout const&;

	private:
		static auto create_descriptor_set_layout(renderer const& renderer) -> vk::UniqueDescriptorSetLayout;
		auto create_pipeline_layout(renderer const& renderer) -> vk::UniquePipelineLayout;

		vk::UniqueShaderModule vertex_shader_;
		vk::UniqueShaderModule fragment_shader_;
		vk::UniqueDescriptorSetLayout descriptor_set_layout_;
		vk::UniquePipelineLayout pipeline_layout_;
	};

	class shader_post_process
	{
	public:
		shader_post_process(
			renderer const& renderer,
			std::filesystem::path const& vert_shader_path,
			std::filesystem::path const& frag_shader_path);

		[[nodiscard]] auto vertex_shader() const -> vk::ShaderModule const&;
		[[nodiscard]] auto fragment_shader() const -> vk::ShaderModule const&;
		[[nodiscard]] auto descriptor_set_layout() const -> vk::DescriptorSetLayout const&;
		[[nodiscard]] auto pipeline_layout() const -> vk::PipelineLayout const&;

	private:
		static auto create_descriptor_set_layout(renderer const& renderer) -> vk::UniqueDescriptorSetLayout;
		auto create_pipeline_layout(renderer const& renderer) -> vk::UniquePipelineLayout;

		vk::UniqueShaderModule vertex_shader_;
		vk::UniqueShaderModule fragment_shader_;
		vk::UniqueDescriptorSetLayout descriptor_set_layout_;
		vk::UniquePipelineLayout pipeline_layout_;
	};
}
