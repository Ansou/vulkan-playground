#pragma once

#include <descriptors.hpp>
#include <shader.hpp>
#include <texture.hpp>

namespace vkpg
{
	class renderer;

	enum class blend_mode
	{
		opaque,
		mask,
		blend
	};

	struct ubo_material_standard
	{
		float shininess;
		bool masking;
		float padding_1[2];
	};

	class material_standard
	{
	public:
		material_standard(
			renderer const& renderer,
			shader_standard const& shader,
			texture const& tex,
			texture const& specular_map,
			float shininess,
			blend_mode blend_mode,
			vk::RenderPass const& render_pass);

		auto recreate_pipeline(renderer const& renderer, vk::RenderPass const& render_pass) -> void;

		[[nodiscard]] auto shader() const -> shader_standard const&;
		[[nodiscard]] auto albedo() const -> texture const&;
		[[nodiscard]] auto specular() const -> texture const&;
		[[nodiscard]] auto get_blend_mode() const -> blend_mode;
		[[nodiscard]] auto pipeline() const -> vk::Pipeline const&;
		[[nodiscard]] auto uniform_buffer() const -> vk::Buffer const&;
		[[nodiscard]] auto descriptor(uint32_t current_frame) const -> vk::DescriptorSet const&;

	private:
		[[nodiscard]] auto create_pipeline(renderer const& renderer, vk::RenderPass const& render_pass) const
			-> vk::UniquePipeline;
		auto update_uniform_buffer(renderer const& renderer) -> void;

		shader_standard const* shader_;
		texture const* texture_;
		texture const* specular_map_;
		float shininess_;
		blend_mode blend_mode_;

		vk::UniqueBuffer uniform_buffer_;
		vk::UniqueDeviceMemory uniform_buffer_memory_;
		vk::UniquePipeline pipeline_;

		descriptor_standard descriptor_;
	};

	class material_skybox
	{
	public:
		material_skybox(
			renderer const& renderer,
			shader_skybox const& shader,
			cubemap const& tex,
			vk::RenderPass const& render_pass);

		auto recreate_pipeline(renderer const& renderer, vk::RenderPass const& render_pass) -> void;

		[[nodiscard]] auto shader() const -> shader_skybox const&;
		[[nodiscard]] auto texture() const -> cubemap const&;
		[[nodiscard]] auto pipeline() const -> vk::Pipeline const&;

	private:
		[[nodiscard]] auto create_pipeline(renderer const& renderer, vk::RenderPass const& render_pass) const
			-> vk::UniquePipeline;

		shader_skybox const* shader_;
		cubemap const* texture_;

		vk::UniquePipeline pipeline_;
	};

	class material_post_process
	{
	public:
		material_post_process(
			renderer const& renderer,
			shader_post_process const& shader,
			vk::ImageView const& image_view,
			vk::Sampler const& sampler,
			vk::RenderPass const& render_pass);

		auto update_image_view(vk::ImageView const& image_view) -> void;
		auto recreate_pipeline(renderer const& renderer, vk::RenderPass const& render_pass) -> void;

		[[nodiscard]] auto shader() const -> shader_post_process const&;
		[[nodiscard]] auto image_view() const -> vk::ImageView const&;
		[[nodiscard]] auto sampler() const -> vk::Sampler const&;
		[[nodiscard]] auto pipeline() const -> vk::Pipeline const&;

	private:
		[[nodiscard]] auto create_pipeline(renderer const& renderer, vk::RenderPass const& render_pass) const
			-> vk::UniquePipeline;

		shader_post_process const* shader_;
		vk::ImageView const* image_view_;
		vk::Sampler const* sampler_;

		vk::UniquePipeline pipeline_;
	};
}
