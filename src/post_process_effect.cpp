#include <post_process_effect.hpp>
#include <material.hpp>

vkpg::post_process_effect::post_process_effect(renderer const& renderer, material_post_process const& material) :
	material_{&material},
	descriptor_{renderer, material.shader().descriptor_set_layout(), material.image_view(), material.sampler()}
{
}

auto vkpg::post_process_effect::update_image_view(renderer const& renderer, vk::ImageView const& image_view) -> void
{
	descriptor_.update_descriptor_set(renderer, image_view, material_->sampler());
}

auto vkpg::post_process_effect::record_draw_commands(vk::CommandBuffer const& command_buffer) const -> void
{
	command_buffer.bindPipeline(vk::PipelineBindPoint::eGraphics, material_->pipeline());
	command_buffer.bindDescriptorSets(
		vk::PipelineBindPoint::eGraphics,
		material_->shader().pipeline_layout(),
		0,
		descriptor_.descriptor_set(),
		nullptr);
	command_buffer.draw(3, 1, 0, 0);
}
