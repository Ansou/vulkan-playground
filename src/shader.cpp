#include <game.hpp>
#include <shader.hpp>

//
// shader_standard
//

vkpg::shader_standard::shader_standard(
	renderer const& renderer,
	std::filesystem::path const& vert_shader_path,
	std::filesystem::path const& frag_shader_path) :
	vertex_shader_{renderer.load_shader(vert_shader_path)},
	fragment_shader_{renderer.load_shader(frag_shader_path)},
	descriptor_set_layout_{create_descriptor_set_layout(renderer)},
	pipeline_layout_{create_pipeline_layout(renderer)}
{
}

auto vkpg::shader_standard::vertex_shader() const -> vk::ShaderModule const&
{
	return *vertex_shader_;
}

auto vkpg::shader_standard::fragment_shader() const -> vk::ShaderModule const&
{
	return *fragment_shader_;
}

auto vkpg::shader_standard::descriptor_set_layout() const -> vk::DescriptorSetLayout const&
{
	return *descriptor_set_layout_;
}

auto vkpg::shader_standard::pipeline_layout() const -> vk::PipelineLayout const&
{
	return *pipeline_layout_;
}

auto vkpg::shader_standard::create_descriptor_set_layout(renderer const& renderer) -> vk::UniqueDescriptorSetLayout
{
	auto material_binding = vk::DescriptorSetLayoutBinding{};
	material_binding.binding = 0;
	material_binding.descriptorCount = 1;
	material_binding.descriptorType = vk::DescriptorType::eUniformBuffer;
	material_binding.stageFlags = vk::ShaderStageFlagBits::eFragment;

	auto albedo_binding = vk::DescriptorSetLayoutBinding{};
	albedo_binding.binding = 1;
	albedo_binding.descriptorCount = 1;
	albedo_binding.descriptorType = vk::DescriptorType::eCombinedImageSampler;
	albedo_binding.pImmutableSamplers = nullptr;
	albedo_binding.stageFlags = vk::ShaderStageFlagBits::eFragment;

	auto specular_binding = vk::DescriptorSetLayoutBinding{};
	specular_binding.binding = 2;
	specular_binding.descriptorCount = 1;
	specular_binding.descriptorType = vk::DescriptorType::eCombinedImageSampler;
	specular_binding.pImmutableSamplers = nullptr;
	specular_binding.stageFlags = vk::ShaderStageFlagBits::eFragment;

	auto bindings = std::array<vk::DescriptorSetLayoutBinding, 3>{material_binding, albedo_binding, specular_binding};
	auto layout_info = vk::DescriptorSetLayoutCreateInfo{};
	layout_info.bindingCount = static_cast<uint32_t>(bindings.size());
	layout_info.pBindings = bindings.data();

	return renderer.device().createDescriptorSetLayoutUnique(layout_info);
}

auto vkpg::shader_standard::create_pipeline_layout(renderer const& renderer) -> vk::UniquePipelineLayout
{
	auto layouts = std::array<vk::DescriptorSetLayout, 4>{
		renderer.descriptor_set_layout_transform(),
		renderer.descriptor_set_layout_camera(),
		*descriptor_set_layout_,
		renderer.descriptor_set_layout_lights()};

	auto pipeline_layout_info = vk::PipelineLayoutCreateInfo{};
	pipeline_layout_info.setLayoutCount = layouts.size();
	pipeline_layout_info.pSetLayouts = layouts.data();
	pipeline_layout_info.pushConstantRangeCount = 0;
	pipeline_layout_info.pPushConstantRanges = nullptr;

	return renderer.device().createPipelineLayoutUnique(pipeline_layout_info);
}

//
// shader_skybox
//

vkpg::shader_skybox::shader_skybox(
	renderer const& renderer,
	std::filesystem::path const& vert_shader_path,
	std::filesystem::path const& frag_shader_path) :
	vertex_shader_{renderer.load_shader(vert_shader_path)},
	fragment_shader_{renderer.load_shader(frag_shader_path)},
	descriptor_set_layout_{create_descriptor_set_layout(renderer)},
	pipeline_layout_{create_pipeline_layout(renderer)}
{
}

auto vkpg::shader_skybox::vertex_shader() const -> vk::ShaderModule const&
{
	return *vertex_shader_;
}

auto vkpg::shader_skybox::fragment_shader() const -> vk::ShaderModule const&
{
	return *fragment_shader_;
}

auto vkpg::shader_skybox::descriptor_set_layout() const -> vk::DescriptorSetLayout const&
{
	return *descriptor_set_layout_;
}

auto vkpg::shader_skybox::pipeline_layout() const -> vk::PipelineLayout const&
{
	return *pipeline_layout_;
}

auto vkpg::shader_skybox::create_descriptor_set_layout(renderer const& renderer) -> vk::UniqueDescriptorSetLayout
{
	auto ubo_layout_binding = vk::DescriptorSetLayoutBinding{};
	ubo_layout_binding.binding = 0;
	ubo_layout_binding.descriptorType = vk::DescriptorType::eUniformBuffer;
	ubo_layout_binding.descriptorCount = 1;
	ubo_layout_binding.stageFlags = vk::ShaderStageFlagBits::eVertex;

	auto sampler_layout_binding = vk::DescriptorSetLayoutBinding{};
	sampler_layout_binding.binding = 1;
	sampler_layout_binding.descriptorCount = 1;
	sampler_layout_binding.descriptorType = vk::DescriptorType::eCombinedImageSampler;
	sampler_layout_binding.stageFlags = vk::ShaderStageFlagBits::eFragment;

	auto bindings = std::array<vk::DescriptorSetLayoutBinding, 2>{ubo_layout_binding, sampler_layout_binding};
	auto layout_info = vk::DescriptorSetLayoutCreateInfo{};
	layout_info.bindingCount = static_cast<uint32_t>(bindings.size());
	layout_info.pBindings = bindings.data();

	return renderer.device().createDescriptorSetLayoutUnique(layout_info);
}

auto vkpg::shader_skybox::create_pipeline_layout(renderer const& renderer) -> vk::UniquePipelineLayout
{
	auto pipeline_layout_info = vk::PipelineLayoutCreateInfo{};
	pipeline_layout_info.setLayoutCount = 1;
	pipeline_layout_info.pSetLayouts = &*descriptor_set_layout_;
	pipeline_layout_info.pushConstantRangeCount = 0;
	pipeline_layout_info.pPushConstantRanges = nullptr;

	return renderer.device().createPipelineLayoutUnique(pipeline_layout_info);
}

//
// shader_post_process
//

vkpg::shader_post_process::shader_post_process(
	renderer const& renderer,
	std::filesystem::path const& vert_shader_path,
	std::filesystem::path const& frag_shader_path) :
	vertex_shader_{renderer.load_shader(vert_shader_path)},
	fragment_shader_{renderer.load_shader(frag_shader_path)},
	descriptor_set_layout_{create_descriptor_set_layout(renderer)},
	pipeline_layout_{create_pipeline_layout(renderer)}
{
}

auto vkpg::shader_post_process::vertex_shader() const -> vk::ShaderModule const&
{
	return *vertex_shader_;
}

auto vkpg::shader_post_process::fragment_shader() const -> vk::ShaderModule const&
{
	return *fragment_shader_;
}

auto vkpg::shader_post_process::descriptor_set_layout() const -> vk::DescriptorSetLayout const&
{
	return *descriptor_set_layout_;
}

auto vkpg::shader_post_process::pipeline_layout() const -> vk::PipelineLayout const&
{
	return *pipeline_layout_;
}

auto vkpg::shader_post_process::create_descriptor_set_layout(renderer const& renderer) -> vk::UniqueDescriptorSetLayout
{
	auto sampler_layout_binding = vk::DescriptorSetLayoutBinding{};
	sampler_layout_binding.binding = 0;
	sampler_layout_binding.descriptorCount = 1;
	sampler_layout_binding.descriptorType = vk::DescriptorType::eCombinedImageSampler;
	sampler_layout_binding.pImmutableSamplers = nullptr;
	sampler_layout_binding.stageFlags = vk::ShaderStageFlagBits::eFragment;

	auto bindings = std::array<vk::DescriptorSetLayoutBinding, 1>{sampler_layout_binding};
	auto layout_info = vk::DescriptorSetLayoutCreateInfo{};
	layout_info.bindingCount = static_cast<uint32_t>(bindings.size());
	layout_info.pBindings = bindings.data();

	return renderer.device().createDescriptorSetLayoutUnique(layout_info);
}

auto vkpg::shader_post_process::create_pipeline_layout(renderer const& renderer) -> vk::UniquePipelineLayout
{
	auto pipeline_layout_info = vk::PipelineLayoutCreateInfo{};
	pipeline_layout_info.setLayoutCount = 1;
	pipeline_layout_info.pSetLayouts = &*descriptor_set_layout_;
	pipeline_layout_info.pushConstantRangeCount = 0;
	pipeline_layout_info.pPushConstantRanges = nullptr;

	return renderer.device().createPipelineLayoutUnique(pipeline_layout_info);
}
