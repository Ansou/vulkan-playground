#pragma once

#include <chrono>
#include <descriptors.hpp>
#include <material.hpp>
#include <transform.hpp>
#include <vector>
#include <vulkan/vulkan.hpp>

namespace vkpg
{
	class model;
	class renderer;

	struct mvp_ubo
	{
		glm::mat4 model{};
		glm::mat4 view{};
		glm::mat4 proj{};
	};

	class render_component
	{
	public:
		render_component(
			renderer const& renderer,
			model const& model,
			material_standard const& material,
			transform const& transform);

		auto record_draw_commands(
			vk::CommandBuffer const& command_buffer,
			camera const& camera,
			descriptor_lights const& lights_descriptor,
			uint32_t current_frame) const -> void;
		auto update_transform_buffer(renderer const& renderer, uint32_t current_image) -> void;

		[[nodiscard]] auto get_transform() const -> transform const&;
		[[nodiscard]] auto get_model() const -> model const&;
		[[nodiscard]] auto get_material() const -> material_standard const&;

	private:
		transform const* transform_;
		model const* model_;
		material_standard const* material_;

		std::vector<vk::UniqueBuffer> transform_buffers_;
		std::vector<vk::UniqueDeviceMemory> transform_buffers_memory_;

		descriptor_transform descriptor_;
	};
}
