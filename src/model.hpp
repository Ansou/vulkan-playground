#pragma once

#include <filesystem>

#include <vulkan/vulkan.hpp>

#include <vertex.hpp>

namespace vkpg
{
	class renderer;

	class model
	{
	public:
		model(std::filesystem::path const& file_path, renderer const& renderer);
		model(std::vector<vertex> vertices, std::vector<uint32_t> indices, renderer const& renderer);

		[[nodiscard]] auto indices() const -> std::vector<uint32_t> const&;
		[[nodiscard]] auto vertex_buffer() const -> vk::Buffer const&;
		[[nodiscard]] auto index_buffer() const -> vk::Buffer const&;

	private:
		std::vector<vertex> vertices_{};
		std::vector<uint32_t> indices_{};
		vk::UniqueBuffer vertex_buffer_;
		vk::UniqueDeviceMemory vertex_buffer_memory_;
		vk::UniqueBuffer index_buffer_;
		vk::UniqueDeviceMemory index_buffer_memory_;

		friend class renderer;
	};

	auto load_model(const std::filesystem::path& file_path) -> std::pair<std::vector<vertex>, std::vector<uint32_t>>;
	auto create_vertex_buffer(
		std::vector<vertex> const& vertices,
		renderer const& ren) -> std::pair<vk::UniqueBuffer, vk::UniqueDeviceMemory>;
	auto create_index_buffer(
		std::vector<uint32_t> const& indices,
		renderer const& ren) -> std::pair<vk::UniqueBuffer, vk::UniqueDeviceMemory>;
}
