#pragma once

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/hash.hpp>
#include <vulkan/vulkan.hpp>

namespace vkpg
{
	struct vertex
	{
	public:
		glm::vec3 pos{};
		glm::vec3 normal{};
		glm::vec2 tex_coord{};

		auto operator==(const vertex& other) const -> bool;

		static auto get_binding_description() -> vk::VertexInputBindingDescription;
		static auto get_attribute_descriptions() -> std::array<vk::VertexInputAttributeDescription, 3>;
	};
}

namespace std
{
	template<>
	struct hash<vkpg::vertex>
	{
		auto operator()(vkpg::vertex const& vertex) const noexcept -> size_t;
	};
}
