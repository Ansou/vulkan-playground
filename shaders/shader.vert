#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inTexCoord;

layout(location = 0) out vec3 fragNormal;
layout(location = 1) out vec2 fragTexCoord;
layout(location = 2) out vec3 fragPosition;

layout(set = 0, binding = 0) uniform ModelObject {
    mat4 model;
} modelUbo;

layout(set = 1, binding = 0) uniform CameraObject {
    mat4 view;
    mat4 proj;
} cameraUbo;

void main() {
    gl_Position = cameraUbo.proj * cameraUbo.view * modelUbo.model * vec4(inPosition, 1.0);
    fragPosition = vec3(cameraUbo.view * modelUbo.model * vec4(inPosition, 1.0));
    fragNormal = mat3(transpose(inverse(cameraUbo.view * modelUbo.model))) * inNormal;
    fragTexCoord = inTexCoord;
}
