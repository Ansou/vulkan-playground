#include <vertex.hpp>

auto vkpg::vertex::operator==(const vertex& other) const -> bool
{
	return pos == other.pos && normal == other.normal && tex_coord == other.tex_coord;
}

auto vkpg::vertex::get_binding_description() -> vk::VertexInputBindingDescription
{
	auto binding_description = vk::VertexInputBindingDescription{};
	binding_description.binding = 0;
	binding_description.stride = sizeof(vertex);
	binding_description.inputRate = vk::VertexInputRate::eVertex;

	return binding_description;
}

auto vkpg::vertex::get_attribute_descriptions() -> std::array<vk::VertexInputAttributeDescription, 3>
{
	auto attribute_descriptions = std::array<vk::VertexInputAttributeDescription, 3>{};

	attribute_descriptions[0].binding = 0;
	attribute_descriptions[0].location = 0;
	attribute_descriptions[0].format = vk::Format::eR32G32B32Sfloat;
	attribute_descriptions[0].offset = offsetof(vertex, pos);

	attribute_descriptions[1].binding = 0;
	attribute_descriptions[1].location = 1;
	attribute_descriptions[1].format = vk::Format::eR32G32B32Sfloat;
	attribute_descriptions[1].offset = offsetof(vertex, normal);

	attribute_descriptions[2].binding = 0;
	attribute_descriptions[2].location = 2;
	attribute_descriptions[2].format = vk::Format::eR32G32B32Sfloat;
	attribute_descriptions[2].offset = offsetof(vertex, tex_coord);

	return attribute_descriptions;
}

auto std::hash<vkpg::vertex>::operator()(vkpg::vertex const& vertex) const noexcept -> size_t
{
	return ((hash<glm::vec3>{}(vertex.pos) ^
			(hash<glm::vec3>{}(vertex.normal) << 1)) >> 1) ^
		(hash<glm::vec2>{}(vertex.tex_coord) << 1);
}
