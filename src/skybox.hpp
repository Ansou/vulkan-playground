#pragma once

#include <filesystem>

#include <vulkan/vulkan.hpp>

#include <model.hpp>
#include <descriptors.hpp>

namespace vkpg
{
	class material_skybox;
	class renderer;

	struct skybox_mvp_ubo
	{
		glm::mat4 model{};
		glm::mat4 view{};
		glm::mat4 proj{};
	};

	class skybox
	{
	public:
		skybox(renderer const& renderer, model const& model, material_skybox const& material);

		auto record_draw_commands(vk::CommandBuffer& command_buffer, uint32_t current_frame) const -> void;
		auto update_transform_buffer(renderer const& renderer, camera const& cam, uint32_t current_frame) -> void;

	private:
		model const* model_;
		material_skybox const* material_;

		std::vector<vk::UniqueBuffer> transform_buffers_;
		std::vector<vk::UniqueDeviceMemory> transform_buffers_memory_;

		descriptor_skybox descriptor_;
	};
}
