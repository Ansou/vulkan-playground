#pragma once

#include <memory>
#include <chrono>

#include <GLFW/glfw3.h>

#include <renderer.hpp>
#include <scene.hpp>
#include <shader.hpp>
#include <texture.hpp>
#include <material.hpp>
#include <model.hpp>
#include <input.hpp>
#include <post_process_effect.hpp>

namespace vkpg
{
	// TODO: Consider move and copy constructors.
	class game
	{
	public:
		game();
		~game();

		auto run() -> void;

		auto add_model(std::unique_ptr<model> m) -> void;
		auto add_texture(std::unique_ptr<texture> t) -> void;
		auto add_material(std::unique_ptr<material_standard> m) -> void;

		[[nodiscard]] auto get_model(size_t index) const -> model const&;
		[[nodiscard]] auto get_texture(size_t index) const -> texture const&;
		[[nodiscard]] auto get_material(size_t index) const -> material_standard const&;
		[[nodiscard]] auto get_shader_standard() const -> shader_standard const&;

		[[nodiscard]] auto get_input_handler() -> input_handler&;

	private:
		static auto create_window(int width, int height) -> GLFWwindow*;

		auto main_loop() -> void;
		auto update(std::chrono::steady_clock::duration delta_time) -> void;
		auto render() -> void;
		auto update_fps_counter(int fps) -> void;

		auto load_resources() -> void;

		static auto framebuffer_resize_callback(GLFWwindow* window, int width, int height) -> void;

		GLFWwindow* window_{create_window(1280, 720)};
		std::unique_ptr<renderer> renderer_{std::make_unique<renderer>(window_)};
		input_handler input_{window_};

		shader_post_process post_process_shader_{
			*renderer_,
			{"shaders/post_process.vert.spv"},
			{"shaders/pp_none.frag.spv"}
		};
		vk::UniqueSampler post_process_sampler_{create_post_proc_sampler(renderer_->device())};
		material_post_process post_process_material_{
			*renderer_,
			post_process_shader_,
			*renderer_->resolve_image_view_,
			*post_process_sampler_,
			*renderer_->post_proc_render_pass_
		};
		post_process_effect post_process_{*renderer_, post_process_material_};

		std::unique_ptr<shader_standard> shader_standard_{};
		std::unique_ptr<shader_skybox> shader_skybox_{};
		std::vector<std::unique_ptr<model>> models_{};
		std::unique_ptr<model> model_skybox_{};
		std::vector<std::unique_ptr<texture>> textures_{};
		std::unique_ptr<cubemap> texture_skybox_{};
		std::vector<std::unique_ptr<material_standard>> materials_standard_{};
		std::unique_ptr<material_skybox> material_skybox_{};

		scene scene_{*renderer_};
	};
}
