#include <array>
#include <filesystem>

#include <stb_image.h>

#include <texture.hpp>
#include <renderer.hpp>

vkpg::texture::texture(renderer const& renderer, std::filesystem::path const& file_path)
{
	std::tie(image_, image_memory_, mip_levels_) = load_image(file_path, renderer);
	image_view_ = create_image_view(
		*image_,
		vk::Format::eR8G8B8A8Srgb,
		vk::ImageAspectFlagBits::eColor,
		mip_levels_,
		renderer.device());
	sampler_ = create_sampler(mip_levels_, renderer.device());
}

auto vkpg::texture::image_view() const -> vk::ImageView const&
{
	return *image_view_;
}

auto vkpg::texture::sampler() const -> vk::Sampler const&
{
	return *sampler_;
}

auto vkpg::texture::load_image(
	const std::filesystem::path& file_path,
	renderer const& renderer) -> std::tuple<
	vk::UniqueImage, vk::UniqueDeviceMemory, uint32_t>
{
	auto tex_width = 0;
	auto tex_height = 0;
	auto tex_channels = 0;
	const auto pixels = stbi_load(
		file_path.string().c_str(),
		&tex_width,
		&tex_height,
		&tex_channels,
		STBI_rgb_alpha);
	const auto image_size = tex_width * tex_height * 4;

	if(!pixels)
	{
		throw std::runtime_error{"failed to load texture image!"};
	}

	auto mip_levels = static_cast<uint32_t>(std::floor(std::log2(std::max(tex_width, tex_height)))) + 1;

	auto staging_buffer = create_buffer(image_size, vk::BufferUsageFlagBits::eTransferSrc, renderer.device());
	auto staging_buffer_memory = renderer.bind_buffer_memory(
		*staging_buffer,
		vk::MemoryPropertyFlagBits::eHostVisible | vk::
		MemoryPropertyFlagBits::eHostCoherent);

	auto const data = renderer.device().mapMemory(
		*staging_buffer_memory,
		vk::DeviceSize{},
		image_size,
		vk::MemoryMapFlags{});
	memcpy(data, pixels, static_cast<size_t>(image_size));
	renderer.device().unmapMemory(*staging_buffer_memory);

	stbi_image_free(pixels);

	auto texture_image = create_image(
		renderer.device(),
		tex_width,
		tex_height,
		mip_levels,
		vk::SampleCountFlagBits::e1,
		vk::Format::eR8G8B8A8Srgb,
		vk::ImageTiling::eOptimal,
		vk::ImageUsageFlagBits::eTransferSrc | vk::ImageUsageFlagBits::eTransferDst |
		vk::ImageUsageFlagBits::eSampled);
	auto texture_image_memory = renderer.bind_image_memory(*texture_image, vk::MemoryPropertyFlagBits::eDeviceLocal);

	renderer.transition_image_layout(
		*texture_image,
		vk::Format::eR8G8B8A8Srgb,
		vk::ImageLayout::eUndefined,
		vk::ImageLayout::eTransferDstOptimal,
		mip_levels);
	renderer.copy_buffer_to_image(
		*staging_buffer,
		*texture_image,
		static_cast<uint32_t>(tex_width),
		static_cast<uint32_t>(tex_height));

	renderer.generate_mipmaps(*texture_image, vk::Format::eR8G8B8A8Srgb, tex_width, tex_height, mip_levels);

	return std::make_tuple(std::move(texture_image), std::move(texture_image_memory), mip_levels);
}

auto vkpg::texture::create_sampler(uint32_t const mip_levels, vk::Device const& device) -> vk::UniqueSampler
{
	auto sampler_info = vk::SamplerCreateInfo{};
	sampler_info.magFilter = vk::Filter::eLinear;
	sampler_info.minFilter = vk::Filter::eLinear;
	sampler_info.addressModeU = vk::SamplerAddressMode::eRepeat;
	sampler_info.addressModeV = vk::SamplerAddressMode::eRepeat;
	sampler_info.addressModeW = vk::SamplerAddressMode::eRepeat;
	sampler_info.anisotropyEnable = true;
	sampler_info.maxAnisotropy = 16;
	sampler_info.borderColor = vk::BorderColor::eIntOpaqueBlack;
	sampler_info.unnormalizedCoordinates = false;
	sampler_info.compareEnable = false;
	sampler_info.compareOp = vk::CompareOp::eAlways;
	sampler_info.mipmapMode = vk::SamplerMipmapMode::eLinear;
	sampler_info.mipLodBias = 0.0f;
	sampler_info.minLod = 0.0f;
	sampler_info.maxLod = static_cast<float>(mip_levels);

	return device.createSamplerUnique(sampler_info);
}

vkpg::cubemap::cubemap(renderer const& renderer, std::array<std::filesystem::path, 6> const& texture_paths) :
	sampler_{
		create_sampler(renderer.device())
	}
{
	std::tie(image_, image_memory_) = load_image(texture_paths, renderer);
	image_view_ = create_image_view(renderer, *image_, vk::Format::eR8G8B8A8Srgb, vk::ImageAspectFlagBits::eColor);
}

auto vkpg::cubemap::image_view() const -> vk::ImageView const&
{
	return *image_view_;
}

auto vkpg::cubemap::sampler() const -> vk::Sampler const&
{
	return *sampler_;
}

auto vkpg::cubemap::load_image(
	std::array<std::filesystem::path, 6> const& texture_paths,
	renderer const& renderer) -> std::pair<vk::UniqueImage, vk::UniqueDeviceMemory>
{
	constexpr auto paths_size = 6;
	auto tex_width = 0;
	auto tex_height = 0;
	auto tex_channels = 0;

	auto buffer_size = 0;

	auto pixels = std::array<stbi_uc*, paths_size>{};
	auto image_sizes = std::array<size_t, paths_size>{};

	for(auto i = 0; i < texture_paths.size(); ++i)
	{
		pixels[i] = stbi_load(
			texture_paths[i].string().c_str(),
			&tex_width,
			&tex_height,
			&tex_channels,
			STBI_rgb_alpha);
		image_sizes[i] = tex_width * tex_height * 4;
		buffer_size += image_sizes[i];

		if(!pixels[i])
		{
			throw std::runtime_error{"failed to load texture image!"};
		}
	}

	auto staging_buffer = create_buffer(buffer_size, vk::BufferUsageFlagBits::eTransferSrc, renderer.device());
	auto staging_buffer_memory = renderer.bind_buffer_memory(
		*staging_buffer,
		vk::MemoryPropertyFlagBits::eHostVisible | vk::
		MemoryPropertyFlagBits::eHostCoherent);

	const auto data = renderer.device().mapMemory(
		*staging_buffer_memory,
		vk::DeviceSize{},
		buffer_size,
		vk::MemoryMapFlags{});
	auto offset = ptrdiff_t{0};
	for(auto i = 0; i < texture_paths.size(); ++i)
	{
		memcpy(static_cast<void*>(static_cast<char*>(data) + offset), pixels[i], image_sizes[i]);
		offset += image_sizes[i];
	}
	renderer.device().unmapMemory(*staging_buffer_memory);

	std::for_each(
		pixels.begin(),
		pixels.end(),
		[](stbi_uc* pix)
		{
			stbi_image_free(pix);
		});

	auto image_info = vk::ImageCreateInfo{};
	image_info.imageType = vk::ImageType::e2D;
	image_info.extent.width = static_cast<uint32_t>(tex_width);
	image_info.extent.height = static_cast<uint32_t>(tex_height);
	image_info.extent.depth = 1;
	image_info.mipLevels = 1;
	image_info.arrayLayers = texture_paths.size();
	image_info.format = vk::Format::eR8G8B8A8Srgb;
	image_info.tiling = vk::ImageTiling::eOptimal;
	image_info.initialLayout = vk::ImageLayout::eUndefined;
	image_info.usage = vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled;
	image_info.sharingMode = vk::SharingMode::eExclusive;
	image_info.samples = vk::SampleCountFlagBits::e1;
	image_info.flags = vk::ImageCreateFlagBits::eCubeCompatible;
	auto texture_image = renderer.device().createImageUnique(image_info);

	auto texture_image_memory = renderer.bind_image_memory(*texture_image, vk::MemoryPropertyFlagBits::eDeviceLocal);

	auto copy_regions = std::vector<vk::BufferImageCopy>{};
	auto copy_offset = vk::DeviceSize{0};

	for(auto i = size_t{0}; i < texture_paths.size(); ++i)
	{
		auto copy_region = vk::BufferImageCopy{};
		copy_region.imageSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
		copy_region.imageSubresource.mipLevel = 0;
		copy_region.imageSubresource.baseArrayLayer = i;
		copy_region.imageSubresource.layerCount = 1;
		copy_region.imageExtent.width = tex_width;
		copy_region.imageExtent.height = tex_height;
		copy_region.imageExtent.depth = 1;
		copy_region.bufferOffset = copy_offset;

		copy_regions.push_back(copy_region);

		copy_offset += image_sizes[i];
	}

	auto subresoure_range = vk::ImageSubresourceRange{};
	subresoure_range.aspectMask = vk::ImageAspectFlagBits::eColor;
	subresoure_range.baseMipLevel = 0;
	subresoure_range.levelCount = 1;
	subresoure_range.layerCount = texture_paths.size();

	renderer.transition_image_layout(
		*texture_image,
		vk::Format::eR8G8B8A8Srgb,
		vk::ImageLayout::eUndefined,
		vk::ImageLayout::eTransferDstOptimal,
		subresoure_range);
	renderer.copy_buffer_to_image(*staging_buffer, *texture_image, copy_regions);
	renderer.transition_image_layout(
		*texture_image,
		vk::Format::eR8G8B8A8Srgb,
		vk::ImageLayout::eTransferDstOptimal,
		vk::ImageLayout::eShaderReadOnlyOptimal,
		subresoure_range);

	return std::make_pair(std::move(texture_image), std::move(texture_image_memory));
}

auto vkpg::cubemap::create_image_view(
	renderer const& renderer,
	vk::Image& image,
	vk::Format format,
	const vk::ImageAspectFlags&
	aspect_flags) -> vk::UniqueImageView
{
	auto view_info = vk::ImageViewCreateInfo{};
	view_info.image = image;
	view_info.viewType = vk::ImageViewType::eCube;
	view_info.format = format;
	view_info.subresourceRange.aspectMask = aspect_flags;
	view_info.subresourceRange.baseMipLevel = 0;
	view_info.subresourceRange.levelCount = 1;
	view_info.subresourceRange.baseArrayLayer = 0;
	view_info.subresourceRange.layerCount = 6;

	return renderer.device().createImageViewUnique(view_info);
}

auto vkpg::cubemap::create_sampler(vk::Device const& device) -> vk::UniqueSampler
{
	auto sampler_info = vk::SamplerCreateInfo{};
	sampler_info.magFilter = vk::Filter::eLinear;
	sampler_info.minFilter = vk::Filter::eLinear;
	sampler_info.addressModeU = vk::SamplerAddressMode::eClampToEdge;
	sampler_info.addressModeV = vk::SamplerAddressMode::eClampToEdge;
	sampler_info.addressModeW = vk::SamplerAddressMode::eClampToEdge;
	sampler_info.anisotropyEnable = true;
	sampler_info.maxAnisotropy = 16;
	sampler_info.borderColor = vk::BorderColor::eFloatOpaqueWhite;
	sampler_info.unnormalizedCoordinates = false;
	sampler_info.compareEnable = false;
	sampler_info.compareOp = vk::CompareOp::eNever;
	sampler_info.mipmapMode = vk::SamplerMipmapMode::eLinear;
	sampler_info.mipLodBias = 0.0f;
	sampler_info.minLod = 0.0f;
	sampler_info.maxLod = 1.0f;

	return device.createSamplerUnique(sampler_info);
}
