#include <camera.hpp>
#include <glm/gtc/quaternion.hpp>
#include <model.hpp>
#include <render_component.hpp>
#include <renderer.hpp>

vkpg::render_component::render_component(
	renderer const& renderer, model const& model, material_standard const& material, transform const& transform) :
	transform_{&transform},
	model_{&model},
	material_{&material},
	transform_buffers_{create_uniform_buffers<transform_ubo>(renderer::max_frames_in_flight(), renderer.device())},
	transform_buffers_memory_{bind_uniform_buffers_memory(transform_buffers_, renderer)},
	descriptor_{renderer, renderer.descriptor_set_layout_transform(), get_frame_arr(transform_buffers_)}
{
}

auto vkpg::render_component::record_draw_commands(
	vk::CommandBuffer const& command_buffer,
	camera const& camera,
	descriptor_lights const& lights_descriptor,
	uint32_t const current_frame) const -> void
{
	command_buffer.bindPipeline(vk::PipelineBindPoint::eGraphics, get_material().pipeline());
	command_buffer.bindVertexBuffers(0, get_model().vertex_buffer(), vk::DeviceSize{0});
	command_buffer.bindIndexBuffer(get_model().index_buffer(), 0, vk::IndexType::eUint32);
	auto const& pipeline_layout = get_material().shader().pipeline_layout();
	command_buffer.bindDescriptorSets(
		vk::PipelineBindPoint::eGraphics, pipeline_layout, 0, descriptor_.descriptor_set(current_frame), nullptr);
	command_buffer.bindDescriptorSets(
		vk::PipelineBindPoint::eGraphics, pipeline_layout, 1, camera.descriptor(current_frame), nullptr);
	command_buffer.bindDescriptorSets(
		vk::PipelineBindPoint::eGraphics, pipeline_layout, 2, material_->descriptor(current_frame), nullptr);
	command_buffer.bindDescriptorSets(
		vk::PipelineBindPoint::eGraphics, pipeline_layout, 3, lights_descriptor.descriptor_set(current_frame), nullptr);
	command_buffer.drawIndexed(static_cast<uint32_t>(get_model().indices().size()), 1, 0, 0, 0);
}

auto vkpg::render_component::get_transform() const -> transform const&
{
	return *transform_;
}

auto vkpg::render_component::get_model() const -> model const&
{
	return *model_;
}

auto vkpg::render_component::get_material() const -> material_standard const&
{
	return *material_;
}

auto vkpg::render_component::update_transform_buffer(renderer const& renderer, const uint32_t current_image) -> void
{
	auto ubo = transform_ubo{};
	ubo.model = transform_->world_matrix();

	renderer.update_ubo(*transform_buffers_memory_[current_image], ubo);
}
